package com.finpy.newctf.Mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.finpy.newctf.pojo.Do.Crypto;

public interface CryptoMapper extends BaseMapper<Crypto> {

}

