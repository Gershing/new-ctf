package com.finpy.newctf.Mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.finpy.newctf.pojo.Do.WebContainer;
import org.apache.ibatis.annotations.Mapper;

/**
 * docker容器(com.finpy.newctf.pojo.Do.WebContainer)表数据库访问层
 *
 * @author 生姜の鱼丸
 * @since 2023-01-25 12:35:10
 */
@Mapper
public interface WebContainerMapper extends BaseMapper<WebContainer> {

}

