package com.finpy.newctf.Mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.finpy.newctf.pojo.Do.Misc;
import org.apache.ibatis.annotations.Mapper;

/**
 * &#064;Author 生姜の鱼丸
 * &#064;Time 2023/1/24 12:57
 * &#064;Info
 */
@Mapper
public interface MiscMapper extends BaseMapper<Misc> {
}
