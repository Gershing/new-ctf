package com.finpy.newctf.Mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.finpy.newctf.pojo.Do.Category;
import org.apache.ibatis.annotations.Mapper;

/**
 * &#064;Author 生姜の鱼丸
 * &#064;Time 2023/1/19 12:28
 * &#064;Info
 */
@Mapper
public interface CategoryMapper extends BaseMapper<Category> {
}
