package com.finpy.newctf;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.finpy.newctf.Mapper")
public class NewCtfApplication {

    public static void main(String[] args) {
        SpringApplication.run(NewCtfApplication.class, args);
    }

}
