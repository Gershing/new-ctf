package com.finpy.newctf.Handler;


import com.alibaba.fastjson.JSONObject;
import com.finpy.newctf.Utils.JwtUtil;
import com.finpy.newctf.Utils.ResCode;
import com.finpy.newctf.Utils.ResUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @Author 生姜の鱼丸
 * @Date 2023/1/11 20:44
 * @Info token验证拦截器
 */
@Component
@Slf4j
public class MyInterceptor implements HandlerInterceptor {
    // 在handler方法执行之前会被调用
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws IOException {
        // 对预检请求放行
        if (request.getMethod().equalsIgnoreCase("OPTIONS")) {
            return true;
        }
        // 获取请求头中的token
        String token = request.getHeader("Authorization");
        if (token == null) {
            res(response, ResCode.IS_NOT_USER);
            return false;
        }
        // 验证token
        if (!JwtUtil.verify(token)) {
            res(response, ResCode.SIGN_ERROR);
            return false;
        }
        return true;
    }

    public void res(HttpServletResponse response, ResCode resCode) throws IOException {
        response.setContentType("application/json;charset=UTF-8");
        response.setStatus(200);
        response.getWriter().print(JSONObject.toJSONString(ResUtils.fail(resCode)));
    }
}
