package com.finpy.newctf.Utils;

import org.springframework.beans.BeanUtils;

import java.util.List;
import java.util.stream.Collectors;

public class BeanUtil {
    private BeanUtil() {
    }

    public static <T> T copyBean(Object source, Class<T> clazz) {
        T result = null;
        try {
            result = clazz.newInstance();
            BeanUtils.copyProperties(source, result);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static <S, T> List<T> copyBeanList(List<S> list, Class<T> clazz) {
        return list.stream()
                .map(s -> copyBean(s, clazz)).collect(Collectors.toList());
    }
}
