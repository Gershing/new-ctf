package com.finpy.newctf.Utils;

import cn.hutool.core.io.FileTypeUtil;
import com.finpy.newctf.Config.Constant;
import com.finpy.newctf.Config.QiNiuYunConfig;
import com.finpy.newctf.MyException.MyException;
import com.finpy.newctf.pojo.Do.FileInfo;
import com.google.gson.Gson;
import com.qiniu.http.Response;
import com.qiniu.storage.Configuration;
import com.qiniu.storage.Region;
import com.qiniu.storage.UploadManager;
import com.qiniu.storage.model.DefaultPutRet;
import com.qiniu.util.Auth;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.system.ApplicationHome;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.util.Objects;
import java.util.UUID;

/**
 * &#064;Author 生姜の鱼丸
 * &#064;Time 2023/1/23 17:40
 * &#064;Info 文件上传
 */
@Slf4j
@Component
public class FileUtil {
    // 将文件上传到服务器
    public FileInfo uploadFile(MultipartFile file) {
        // 给文件重命名
        String fileName = UUID.randomUUID() + "." + Objects.requireNonNull(file.getContentType())
                .substring(file.getContentType().lastIndexOf("/") + 1);
        try {
            // 获取保存路径
            String path = getSavePath(Constant.FILE_FILE);
            File files = new File(path, fileName);
            System.out.println(path + fileName);
            if (!files.exists()) {
                if (!files.mkdirs()) {
                    log.info("文件目录创建失败！");
                    // 创建失败
                    return null;
                }
            }
            file.transferTo(files);
            FileInfo fileInfo = new FileInfo();
            fileInfo.setFileName(fileName);
            fileInfo.setFilePath(Constant.FILE_FILE + fileName);
            fileInfo.setSize(file.getSize());
            log.info("文件上传成功！");
            return fileInfo; // 返回重命名后的文件名
        } catch (IOException e) {
            log.error("{}   上传失败！", file.getContentType());
            throw new MyException(ResCode.UPLOAD_ERROR);
        }
    }

    public String getSavePath(String filePath) {
        // 这里需要注意的是ApplicationHome是属于SpringBoot的类
        // 获取项目下resources/static/img路径
        ApplicationHome applicationHome = new ApplicationHome(this.getClass());

        // 保存目录位置根据项目需求可随意更改
        // linux
        return System.getProperty("user.dir") + "/static/" + filePath;
//        return applicationHome.getDir().getParentFile()
//                .getParentFile().getAbsolutePath() + "\\src\\main\\resources\\static\\" + filePath;
    }

    public ResUtils uploadAvatar(MultipartFile file) throws IOException {
        //后缀名文件类型
        String filename = file.getOriginalFilename();
        //String suffixType = FileUtil.extName(filename);
        String suffixType = StringUtils.substringAfterLast(filename, ".");
        //文件头文件类型
        String headerType = FileTypeUtil.getType(file.getInputStream());
        log.info("头像文件类型  ：{}", headerType);
        if (!StringUtils.equalsIgnoreCase(suffixType, headerType)) {
            log.error("文件头与文件名后缀不匹配。文件名：{}，文件头：{}", filename, headerType);
            return ResUtils.fail(ResCode.FILE_TYPE_ERROR);
        }

        // 给文件重命名
        String fileName = UUID.randomUUID().toString().replace("-", "") + "." + headerType;
        // 获取保存路径
        String path = getSavePath(Constant.FILE_IMG);
        File files = new File(path, fileName);
        if (!files.exists()) {
            if (!files.mkdirs()) {
                // 创建失败
                log.info("头像目录创建失败！");
                return ResUtils.fail(ResCode.UPLOAD_ERROR);
            }
        }
        file.transferTo(files);
        FileInfo fileInfo = new FileInfo();
        fileInfo.setFileName(fileName);
        fileInfo.setFilePath(Constant.FILE_IMG + fileName);
        fileInfo.setSize(file.getSize());
        log.info("头像上传成功！");
        return ResUtils.success(ResCode.UPLOAD_SUCCESS, fileInfo);
    }

    public ResUtils downFile(FileInfo file, HttpServletResponse response) {
        FileInputStream fileInputStream = null;
        OutputStream outputStream = null;
        try {
            fileInputStream = new FileInputStream(new File(getSavePath(""), file.getFilePath()));
            response.setHeader("Content-Disposition", "attachment;filename=" + file.getFileName());
            BufferedInputStream bufferedInputStream = new BufferedInputStream(fileInputStream);
            int i;
            byte[] buf = new byte[1024 * 8];
            outputStream = response.getOutputStream();
            while ((i = bufferedInputStream.read(buf, 0, buf.length)) != -1) {
                outputStream.write(buf, 0, i);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            throw new MyException(ResCode.FILE_NOT_EXISTENT);
        } catch (IOException e) {
            e.printStackTrace();
            throw new MyException(ResCode.PROGRAM_INSIDE_EXCEPTION);
        } finally {
            try {
                if (fileInputStream != null) fileInputStream.close();
                if (outputStream != null) {
                    outputStream.flush();
                    outputStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return ResUtils.success(ResCode.GET_SUCCESS);
    }

    private QiNiuYunConfig qiNiuYunConfig;

    public FileUtil(QiNiuYunConfig qiNiuYunConfig) {
        this.qiNiuYunConfig = qiNiuYunConfig;
    }

//    private void init() {
//        // 华东：zone0    华北：zone1   华南：zone2   北美：na0   东南亚：as0
//        // 我是华南地区的所以是zone2，如果是其他地区的需要修改
//        uploadManager = new UploadManager(new Configuration(Zone.zone2()));
//        auth = Auth.create(qiNiuYunConfig.getAccessKey(), qiNiuYunConfig.getSecretKey());
//        // 根据命名空间生成的上传token
//        bucketManager = new BucketManager(auth, new Configuration(Zone.zone2()));
//        token = auth.uploadToken(qiNiuYunConfig.getBucketName());
//    }

    public ResUtils uploadQiniu(MultipartFile file) {
        //构造一个带指定 Region 对象的配置类,需要设置区域 region2->华南
        Configuration cfg = new Configuration(Region.region2());
        UploadManager uploadManager = new UploadManager(cfg);

        try {
            Auth auth = Auth.create(qiNiuYunConfig.getAccessKey(), qiNiuYunConfig.getSecretKey());
            // 根据命名空间生成的上传token
            String token = auth.uploadToken(qiNiuYunConfig.getBucketName());
            // 获取文件名
            String filename = file.getOriginalFilename();
            // 获取文件后缀
            String suffixType = StringUtils.substringAfterLast(filename, ".");
            //文件头文件类型
            String headerType = FileTypeUtil.getType(file.getInputStream());
            log.info("头像文件类型  ：{}", headerType);
            if (!StringUtils.equalsIgnoreCase(suffixType, headerType)) {
                log.error("文件头与文件名后缀不匹配。文件名：{}，文件头：{}", filename, headerType);
                return ResUtils.fail(ResCode.FILE_TYPE_ERROR);
            }

            // 给文件重命名
            String fileName = Constant.FILE_IMG + UUID.randomUUID().toString().replace("-", "") + "." + headerType;

            // 上传图片文件
            Response res = uploadManager.put(file.getInputStream(), fileName, token, null, null);
            if (!res.isOK()) {
                throw new RuntimeException("上传七牛出错：" + res);
            }
            // 解析上传成功的结果
            DefaultPutRet putRet = new Gson().fromJson(res.bodyString(), DefaultPutRet.class);
            // 解析下载地址
            String encodedFileName = URLEncoder.encode(fileName, "utf-8").replace("+", "%20");
            String publicUrl = String.format("%s/%s", this.qiNiuYunConfig.getUrl(), encodedFileName);
//            System.out.println("七牛上传返回结果：" + s + publicUrl);
            log.info("访问地址：{}", publicUrl);
            // 直接返回外链地址
            return ResUtils.success(ResCode.UPLOAD_SUCCESS, publicUrl);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ResUtils.fail(ResCode.UPLOAD_ERROR);
    }
}