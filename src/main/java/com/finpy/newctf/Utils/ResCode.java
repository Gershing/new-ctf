package com.finpy.newctf.Utils;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * &#064;Author 生姜の鱼丸
 * &#064;Time 2023/1/14 15:56
 * &#064;Info 返回状态码
 */
@Getter
@AllArgsConstructor
public enum ResCode {
    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/16 14:39
     * 业务处理成功 -> 800 ;
     * 业务处理失败 -> 801 ;
     * 系统异常    -> 803 ;
     * 参数异常    -> 804 ;
     * 用户登录超时 -> 805 ;
     * 用户权限不够 -> 806 ;
     */

    // 用户验证模块
    IS_NOT_USER(806, "您的权限不够!"),
    IS_ADMIN(800, "欢迎管理员！"),
    IS_NOT_ADMIN(806, "您不是管理员！"),

    // 用户登录模块
    SIGN_ERROR(805, "用户身份已过期！"),
    CAPTCHA_ERROR(801, "验证码错误！"),
    CAPTCHA_CORRECT(800, "验证码正确！"),
    CAPTCHA_GET_SUCCESS(800, "验证码获取成功！"),
    CAPTCHA_GET_ERROR(801, "验证码获取失败！"),
    USER_IS_NOT_EXIST(801, "账号不存在！"),
    USER_IS_NOT_TO_USE(801, "该账号不可用！"),
    PASSWORD_ERROR(801, "密码错误！"),
    LOGIN_SUCCESS(800, "登录成功！"),
    REGISTER_SUCCESS(800, "注册成功！"),
    REGISTER_ERROR(801, "注册失败！"),

    // 信息发布模块
    POST_SUCCESS(800, "发布成功!"),
    POST_ERROR(801, "发布失败!"),

    // 数据获取模块
    GET_SUCCESS(800, "获取成功！"),
    GET_ERROR(801, "获取失败！"),

    // 题目模块
    TOPIC_POST_SUCCESS(800, "题目发布成功！"),
    TOPIC_POST_ERROR(801, "题目发布失败！"),
    TOPIC_POST_REPEAT(801, "题目已存在请勿重复发布！"),

    // flag模块
    FLAG_CORRECT(800, "FLAG正确！"),
    FLAG_ERROR(801, "FLAG错误！"),
    SUBMIT_ERROR(801, "FLAG提交失败！"),

    // docker模块
    DOCKER_REPEAT(801, "环境重复开启！"),
    DOCKER_OPEN_SUCCESS(800, "环境开启成功！"),
    DOCKER_OPEN_ERROR(801, "环境开启失败！"),
    DOCKER_CLOSE_SUCCESS(800, "环境关闭成功！"),
    DOCKER_CLOSE_ERROR(801, "环境关闭失败！"),

    // 数据修改模块
    MODIFY_SUCCESS(800, "修改成功！"),
    MODIFY_ERROR(801, "修改失败！"),
    DELETE_SUCCESS(800, "删除成功！"),
    DELETE_ERROR(801, "删除失败！"),

    // 添加模块
    ADD_SUCCESS(800, "添加成功！"),
    ADD_ERROR(801, "添加失败！"),

    // 文件上传模块
    UPLOAD_SUCCESS(800, "上传成功！"),
    UPLOAD_ERROR(801, "上传失败！"),

    // 系统异常模块
    DATA_ERROR(801, "数据异常！"),

    // 未开通模块
    NOT_OPEN(2000, "该功能模块尚未开通！"),

    PROGRAM_INSIDE_EXCEPTION(803, "程序内部异常!"),
    REQUEST_PARAM_ERROR(804, "请求参数错误!"),
    REQUEST_METHOD_ERROR(801, "请求方式错误！"),
    OPERATE_ERROR(801, "操作失败！"),
    OPERATE_SUCCESS(800, "操作成功！"),
    FILE_TYPE_ERROR(801, "文件类型错误！"),
    FILE_NOT_EXISTENT(801, "文件不存在!"),
    USER_IS_EXIST(801, "用户名已存在！"),
    SUBMIT_REPEAT(801, "请勿重复提交！"),
    CHALLENGE_NOT_EXISTENT(801, "该题目不存在！"),
    FLAG_IS_NOT_USE(801, "FLAG已失效！"),
    IS_FINISHED(801, "您已完成该题目！");

    private final Integer code;
    private final String msg;
}