package com.finpy.newctf.Utils;

import java.util.Random;

public class SaltUtils {
    /**
     * 生成salt的静态方法
     */
    public static String getSalt(int n) {
        char[] chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz~!@#$%^&*()_+".toCharArray();
        int length = chars.length;
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < n; i++) {
            char achar = chars[new Random().nextInt(length)];
            sb.append(achar);
        }
        return sb.toString();
    }
}