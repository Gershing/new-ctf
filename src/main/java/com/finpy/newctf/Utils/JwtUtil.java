package com.finpy.newctf.Utils;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.finpy.newctf.Config.Constant;
import com.finpy.newctf.MyException.MyException;
import com.finpy.newctf.pojo.Do.User;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class JwtUtil {
    /**
     * 生成签名，30分钟过期
     *
     * @param **userInfo** 用户信息 用户姓名
     * @param **other**    用户其他信息 用户id
     * @return
     */
    public static String sign(User user) {
        try {
            // 设置过期时间
            Date date = new Date(System.currentTimeMillis() + Constant.TOKEN_OVER_TIME);
            //私钥和加密算法
            Algorithm algorithm = Algorithm.HMAC256(Constant.TOKEN_SECRET);
            // 设置Header
            Map<String, Object> header = new HashMap<>();
            header.put("id", user.getId());
            header.put("key", "754465599");
            // 返回token字符串
            return JWT.create()
                    .withHeader(header)
                    .withClaim("id", user.getId())
                    .withClaim("username", user.getUserName())
                    .withExpiresAt(date)
                    .sign(algorithm);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 检验token是否正确
     *
     * @param **token**
     * @return
     */
    public static boolean verify(String token) {
        try {
            Algorithm algorithm = Algorithm.HMAC256(Constant.TOKEN_SECRET);
            JWTVerifier verifier = JWT.require(algorithm).build();
            verifier.verify(token);//未验证通过会抛出异常
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/15 12:59
     * @Info 获取token中的用户id
     */
    public static Integer getUserId(String token) {
        try {
            DecodedJWT jwt = JWT.decode(token);
            return jwt.getClaim("id").asInt();
        } catch (JWTDecodeException e) {
            throw new MyException(ResCode.IS_NOT_USER);
        }
    }

    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/15 12:59
     * @Info 获取token中的username
     */
    public static String getUserName(String token) {
        try {
            DecodedJWT jwt = JWT.decode(token);
            return jwt.getClaim("username").asString();
        } catch (JWTDecodeException e) {
            throw new MyException(ResCode.IS_NOT_USER);
        }
    }
}
