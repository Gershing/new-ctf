package com.finpy.newctf.Utils;

import lombok.Data;

@Data
public class ResUtils {
    private int code;
    private Object data;
    private String msg;

    public void setCode(int code) {
        this.code = code;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public static ResUtils success(ResCode resCode, Object data) {
        ResUtils resUtil = new ResUtils();
        resUtil.setCode(resCode.getCode());
        resUtil.setMsg(resCode.getMsg());
        resUtil.setData(data);
        return resUtil;
    }

    public static ResUtils success(ResCode resCode) {
        ResUtils resUtil = new ResUtils();
        resUtil.setCode(resCode.getCode());
        resUtil.setMsg(resCode.getMsg());
        return resUtil;
    }

    public static ResUtils fail(ResCode resCode) {
        ResUtils resUtil = new ResUtils();
        resUtil.setCode(resCode.getCode());
        resUtil.setMsg(resCode.getMsg());
        return resUtil;
    }
}
