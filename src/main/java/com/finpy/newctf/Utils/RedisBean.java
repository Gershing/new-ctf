package com.finpy.newctf.Utils;

import com.finpy.newctf.Config.Constant;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Component
public class RedisBean {

    @Resource
    private RedisTemplate<String, Object> redisTemplate;

    /**
     * 存放Object到指定的key
     */
    public boolean set(String key, Object value) {
        try {
            redisTemplate.opsForValue().set(key, value);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }


    /**
     * 实现命令：SET key value，设置一个key-value（将字符串值 value关联到 key）
     *
     * @param key
     * @param value
     */
    public void hSet(String key, String field, Object value) {
        redisTemplate.opsForHash().put(key, field, value);
    }

    /**
     * 实现命令：HDEL key field [field ...]，删除哈希表 key 中的一个或多个指定域，不存在的域将被忽略。
     *
     * @param key
     * @param fields
     */
    public void hDel(String key, Object... fields) {
        redisTemplate.opsForHash().delete(key, fields);
    }

    /**
     * 实现命令：HGETALL key，返回哈希表 key中，所有的域和值。
     *
     * @param key
     * @return
     */
    public Map<Object, Object> hGetAll(String key) {
        return redisTemplate.opsForHash().entries(key);
    }

    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/20 20:07
     * @Info 删除set集合
     */
    public void remove(String key, String value) {
        redisTemplate.opsForSet().remove(key, value);
    }

    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/20 20:09
     * @Info 判断set集合是否存在
     */
    public boolean contains(String key, String value) {
        return Boolean.TRUE.equals(redisTemplate.opsForSet().isMember(key, value));
    }

    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/20 20:11
     * @Info 获取set集合大小
     */
    public Long getSetSize(String key) {
        return redisTemplate.opsForSet().size(key);
    }


    /**
     * 存放Object到指定的key，并指定时效时间
     *
     * @param time 单位为分钟
     */
    public boolean set(String key, Object value, int time) {
        try {
            if (time > 0) {
                redisTemplate.opsForValue().set(Constant.CAPTCHA_KEY + key, value, time, TimeUnit.MINUTES);
            } else {
                set(key, value);
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 获取指定key的值
     */
    public Object get(String key) {
        return key == null ? null : redisTemplate.opsForValue().get(Constant.CAPTCHA_KEY + key);
    }

    /**
     * 删除key
     */
    public void del(String key) {
        redisTemplate.delete(Constant.CAPTCHA_KEY + key);
    }

    /**
     * 指定key的过期时间
     *
     * @param time 单位为秒
     */
    public boolean expire(String key, long time) {
        try {
            if (time > 0) {
                redisTemplate.expire(key, time, TimeUnit.MINUTES);
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }


    // 获取key的过期时间，单位为秒
    // @return 返回-1表示没有指定时效时间
    public long getExpire(String key) {
        Long expire = redisTemplate.getExpire(Constant.CAPTCHA_KEY + key, TimeUnit.MINUTES);
        if (expire == null) {
            return 0;
        }
        return expire;
    }

    /**
     * 是否存在key
     *
     * @return true-存在，false-不存在
     */
    public boolean hasKey(String key) {
        try {
            return Boolean.TRUE.equals(redisTemplate.hasKey(Constant.CAPTCHA_KEY + key));
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

}