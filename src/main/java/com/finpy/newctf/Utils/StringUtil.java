package com.finpy.newctf.Utils;

import com.finpy.newctf.Config.Constant;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Component
public class StringUtil implements ApplicationContextAware {

    private static ApplicationContext applicationContext = null;

    public static String getImg() {
        Random random = new Random();
        int i = random.nextInt(20) + 1;
        return Constant.IMG_URL + i + ".jpg";
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        if (StringUtil.applicationContext == null) {
            StringUtil.applicationContext = applicationContext;
        }
    }

    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/29 14:27
     * @Info 计算两个时间之间的分钟差
     */
    public static long getTime(LocalDateTime oldTime, LocalDateTime newTime) {
        Duration between = Duration.between(newTime, oldTime);
        return between.toMinutes();
    }

    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/29 14:55
     * @Info 计算两个时间之前的秒数差
     */
    public static long getSecond(LocalDateTime oldTime, LocalDateTime newTime) {
        Duration between = Duration.between(newTime, oldTime);
        return between.toMillis() / 1000;
    }

    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/25 13:56
     * @Info 根据分类id获取分类名称
     */
    public static String getCategoryNameById(Integer id) {
        String str = null;
        switch (id) {
            case 1:
                str = "Web";
                break;
            case 2:
                str = "Misc";
                break;
            case 3:
                str = "Pwn";
                break;
            case 4:
                str = "Reverse";
                break;
            case 5:
                str = "Crypto";
                break;
            case 6:
                str = "Real";
                break;
            case 7:
                str = "Other";
                break;
        }
        return str;
    }

    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/20 19:44
     * @Info 生成容器名
     */
    public static String getDockerName() {
        return String.format("%s-%s", 5, UUID.randomUUID());
    }

    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/20 19:45
     * @Info 生成flag
     */
    public static String getFlag() {
        return String.format("%s{%s}", Constant.FLAG, UUID.randomUUID());
    }

    // 判断字符串是否为空
    public static boolean isNull(String str) {
        return str == null || str.equals("");
    }

    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/16 19:52
     * @Info 判断整数是否为空（0）
     */
    public static boolean intIsNull(Integer integer) {
        return integer == null || integer == 0;
    }

    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/19 15:05
     * @Info 获取用户Role
     */
    public static String getUserRole(Integer role) {
        Map<Integer, String> roles = new HashMap<>();
        roles.put(0, "管理员");
        roles.put(1, "小组成员");
        roles.put(2, "普通用户");
        return roles.get(role);
    }

    // 时间转换，将时间戳转换为 XXXX-XX-XX XX:XX:XX 的格式
    public static String toTime(long time) {
        Instant instant = Instant.ofEpochSecond(time / 1000);
        LocalDateTime dateTime = LocalDateTime.ofInstant(instant, ZoneId.systemDefault());
        // return：2022-12-23 12:34:56
        return dateTime.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
    }

    // 获取用户身份表示
    public static String getSign(int point) {
        if (point >= 0 && point <= 100) {
            return "小白-Ⅴ";
        }
        if (point > 100 && point <= 200) {
            return "小白-Ⅳ";
        }
        if (point > 200 && point <= 400) {
            return "小白-Ⅲ";
        }
        if (point > 400 && point <= 500) {
            return "小白-Ⅱ";
        }
        if (point > 500 && point <= 800) {
            return "小白-Ⅰ";
        }
        if (point > 800 && point <= 1100) {
            return "脚本小子-Ⅲ";
        }
        if (point > 1100 && point <= 1400) {
            return "脚本小子-Ⅱ";
        }
        if (point > 1400 && point <= 2000) {
            return "脚本小子-Ⅰ";
        }
        if (point > 2000 && point <= 2400) {
            return "脚本选手-Ⅲ";
        }
        if (point > 2400 && point <= 2800) {
            return "脚本选手-Ⅱ";
        }
        if (point > 2800 && point <= 3600) {
            return "脚本选手-Ⅰ";
        }
        if (point > 3600 && point <= 4100) {
            return "脚本高手-Ⅲ";
        }
        if (point > 4100 && point <= 4600) {
            return "脚本高手-Ⅱ";
        }
        if (point > 4600 && point <= 5600) {
            return "脚本高手-Ⅰ";
        }
        if (point > 5600 && point <= 6600) {
            return "工程师-Ⅲ";
        }
        if (point > 6600 && point <= 8100) {
            return "工程师-Ⅱ";
        }
        if (point > 8100 && point <= 10000) {
            return "工程师-Ⅰ";
        }
        if (point > 10000 && point <= 12000) {
            return "研究员-Ⅲ";
        }
        if (point > 12000 && point <= 15000) {
            return "研究员-Ⅱ";
        }
        if (point > 15000 && point <= 20000) {
            return "研究员-Ⅰ";
        }
        if (point > 20000 && point <= 25000) {
            return "黑客-Ⅲ";
        }
        if (point > 25000 && point <= 35000) {
            return "黑客-Ⅱ";
        }
        if (point > 35000 && point <= 50000) {
            return "黑客-Ⅰ";
        }
        if (point > 50000 && point <= 60000) {
            return "极客-Ⅲ";
        }
        if (point > 70000 && point <= 80000) {
            return "极客-Ⅱ";
        }
        if (point > 80000 && point <= 100000) {
            return "极客-Ⅰ";
        }
        if (point > 100000) {
            return "飞升者";
        }

        return "无知者";
    }

    // 获取当前时间戳
    public static String getTime() {
        return System.currentTimeMillis() + "";
    }

    public static String getMd5(String str, String salt) {
        String newPwd = salt + str + salt;
        return DigestUtils.md5DigestAsHex(newPwd.getBytes());
    }

    // 分类
    public static int getClass(String str) {
        Map<String, Integer> res = new HashMap<>();
        res.put("Web", 1);
        res.put("Misc", 2);
        res.put("Pwn", 3);
        res.put("Reverse", 4);
        res.put("Crypto", 5);
        res.put("Other", 6);
        return res.get(str);
    }

    public static String getRandomImgName(String fileName) {

        // 获取文件后缀
        int index = fileName.lastIndexOf(".");
        String suffix = fileName.substring(index);

        //检验文件
        String uuid = UUID.randomUUID().toString().replaceAll("-", "");
        return uuid + suffix;
    }

    /**
     * &#064;auther 生姜の鱼丸
     * &#064;Info 判断字符串不为空
     */
    public static boolean isNotNull(String str) {
        return StringUtils.hasText(str);
    }

    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/16 17:15
     * @Info 判断 list集合是否为空
     */
    public static boolean listIsNull(List<?> list) {
        return list == null || list.size() == 0;
    }
}
