package com.finpy.newctf.Utils;

import com.finpy.newctf.Config.DockerConfig;
import com.finpy.newctf.pojo.DockerContainer;
import com.github.dockerjava.api.command.CreateContainerResponse;
import com.github.dockerjava.api.exception.ConflictException;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DockerUtil {
    public static String ConnectDocker(Integer port, String image, String flag, String name) {
        log.info("开始连接Docker......");
        log.info("开始创建容器......");


        // 配置容器
        DockerContainer dockerContainer = new DockerContainer();
        dockerContainer.setName(name);
        dockerContainer.setSrcPort(80);     //端口
        dockerContainer.setDstPort(port);   //映射端口
        dockerContainer.setCpuLimit(0.5);   // cpu限制
        dockerContainer.setMemLimit(120);   // 网速限制
        dockerContainer.setImageName(image); // 镜像名称
        dockerContainer.setAddCount(0);
        dockerContainer.setAddTime(0);
        dockerContainer.setFlag(flag);

        String containerId = null;
        try {

            CreateContainerResponse createContainerResponse = DockerConfig.createContainers(dockerContainer);

            DockerConfig.startContainer(createContainerResponse.getId());
            containerId = createContainerResponse.getId();

        } catch (ConflictException e) {
            log.error("容器创建失败！");
        }
        return containerId;
    }

    // 关闭容器
    public static void removeContainer(String containerId) {
        DockerConfig config = new DockerConfig();
        config.removeContainer(containerId);
    }

}
