package com.finpy.newctf.MyException;


import com.finpy.newctf.Utils.ResCode;
import lombok.Getter;

/**
 * @Author 生姜の鱼丸
 * @Date 2023/1/11 20:44
 * @Info 自定义全局异常处理类
 */
@Getter
public class MyException extends RuntimeException{
    private final int code;
    private final String msg;

    public MyException(ResCode resCode){
        this.code = resCode.getCode();
        this.msg = resCode.getMsg();
    }
}
