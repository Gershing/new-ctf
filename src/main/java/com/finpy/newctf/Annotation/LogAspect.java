package com.finpy.newctf.Annotation;

import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * &#064;Author 生姜の鱼丸
 * &#064;Time 2023/1/15 19:32
 * &#064;Info
 */
@Component
@Aspect
@Slf4j
public class LogAspect {
    @Pointcut("@annotation(com.finpy.newctf.Annotation.SystemLog)")
    public void pt() {

    }

    @Around("pt()")
    public Object printLog(ProceedingJoinPoint pjp) throws Throwable {
        Object ret;
        try {
            handleBefore(pjp);
            ret = pjp.proceed();
            handleAfter(ret);
        } finally {
            log.info("============End============" + System.lineSeparator());
        }
        return ret;
    }

    private void handleAfter(Object ret) {
        log.info("Response       : {}", JSONObject.toJSONString(ret));
    }

    private void handleBefore(ProceedingJoinPoint pjp) {
        ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        assert requestAttributes != null;
        HttpServletRequest request = requestAttributes.getRequest();

        SystemLog systemLog = getSystemLog(pjp);

        // 获取请求时间

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Calendar calendar = Calendar.getInstance();
        String time = formatter.format(calendar.getTime());

        log.info("===========Start===========");
        log.info("Time           : {}", time);
        log.info("URL            : {}", request.getRequestURI());
        log.info("FunName        : {}", systemLog.funName());
        log.info("HTTP Method    : {}", request.getMethod());
        log.info("Class Method   : {}, {}", pjp.getSignature().getDeclaringTypeName(), ((MethodSignature) pjp.getSignature()).getName());
        log.info("IP             : {}", request.getRemoteHost());
        log.info("Request Args   : {}", JSONObject.toJSONString(pjp.getArgs()));
    }

    private SystemLog getSystemLog(ProceedingJoinPoint pjp) {
        MethodSignature signature = (MethodSignature) pjp.getSignature();
        return signature.getMethod().getAnnotation(SystemLog.class);
    }
}
