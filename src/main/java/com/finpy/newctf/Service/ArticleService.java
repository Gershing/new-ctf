package com.finpy.newctf.Service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.finpy.newctf.Utils.ResUtils;
import com.finpy.newctf.pojo.Do.Article;
import com.finpy.newctf.pojo.dto.ArticleInfo;
import com.finpy.newctf.pojo.dto.ArticlePage;

/**
 * &#064;Author 生姜の鱼丸
 * &#064;Time 2023/1/19 17:49
 * &#064;Info
 */
public interface ArticleService extends IService<Article> {
    ResUtils GetArticleList(ArticlePage page);

    ResUtils GetArticleInfo(Integer id);

    ResUtils GetArticleUserInfo(Integer id);

    ResUtils GetArticleUserInfos(String userName);

    ResUtils SetArticle(ArticleInfo article, Integer userId);

    ResUtils AdminGetArticleList(ArticlePage page);

    ResUtils DeleteArticle(Integer id);

    ResUtils getUserArticleList(Integer id);
}
