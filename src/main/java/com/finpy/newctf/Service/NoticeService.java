package com.finpy.newctf.Service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.finpy.newctf.Utils.ResUtils;
import com.finpy.newctf.pojo.Do.Notice;

/**
 * 公告(ZbNotice)表服务接口
 *
 * @author makejava
 * @since 2023-01-16 17:03:59
 */
public interface NoticeService extends IService<Notice> {
    ResUtils SetNotice(Notice notice);
}
