package com.finpy.newctf.Service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.finpy.newctf.Utils.ResUtils;
import com.finpy.newctf.pojo.Do.Settings;

/**
 * &#064;Author 生姜の鱼丸
 * &#064;Time 2023/1/16 16:12
 * &#064;Info
 */
public interface SettingsService extends IService<Settings> {
    ResUtils GetSettings();

    ResUtils UpdateSettings(Settings settings);
}
