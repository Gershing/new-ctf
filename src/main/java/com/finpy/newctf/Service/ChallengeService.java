package com.finpy.newctf.Service;

import com.finpy.newctf.Utils.ResUtils;
import com.finpy.newctf.pojo.dto.ChallengeInfo;
import com.finpy.newctf.pojo.dto.DeleteChallenge;
import com.finpy.newctf.pojo.dto.UpdateChallenge;

public interface ChallengeService {

    ResUtils UpdateChallenge(UpdateChallenge challenge);

    ResUtils getChallengeInfo(ChallengeInfo challengeInfo, Integer id);

    ResUtils getAdminChallengeInfo(ChallengeInfo challengeInfo);

    ResUtils deleteChallenge(DeleteChallenge delChallenge);
}
