package com.finpy.newctf.Service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.finpy.newctf.pojo.Do.Ok;

/**
 * &#064;Author 生姜の鱼丸
 * &#064;Time 2023/1/19 16:11
 * &#064;Info
 */
public interface OkService extends IService<Ok> {
    void addOk(Integer id, Integer userId, String category);
}
