package com.finpy.newctf.Service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.finpy.newctf.Utils.ResUtils;
import com.finpy.newctf.pojo.Do.Web;
import com.finpy.newctf.pojo.dto.AddWeb;
import com.finpy.newctf.pojo.dto.PageDto;
import com.finpy.newctf.pojo.dto.SubmitFlag;

/**
 * &#064;Author 生姜の鱼丸
 * &#064;Time 2023/1/19 14:12
 * &#064;Info
 */
public interface WebService extends IService<Web> {
    ResUtils OpenEnv(Integer id, Integer userId);

    ResUtils getWebInfo(Integer id, Integer userId);

    ResUtils getWebList(PageDto page);

    ResUtils addWeb(AddWeb web, Integer id);

    ResUtils submitFlag(SubmitFlag flag, Integer userId);

    ResUtils GetSortAndWp(Integer id);

    ResUtils EndEnv(Integer id, Integer userId);

    ResUtils getAdminWebList(PageDto page);

    ResUtils getAdminWebInfo(Integer id);

    ResUtils deleteWeb(Integer id);
}
