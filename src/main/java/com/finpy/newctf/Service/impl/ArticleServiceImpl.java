package com.finpy.newctf.Service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.finpy.newctf.Config.Constant;
import com.finpy.newctf.Mapper.ArticleMapper;
import com.finpy.newctf.Mapper.CategoryMapper;
import com.finpy.newctf.Mapper.UserMapper;
import com.finpy.newctf.Service.ArticleService;
import com.finpy.newctf.Utils.BeanUtil;
import com.finpy.newctf.Utils.ResCode;
import com.finpy.newctf.Utils.ResUtils;
import com.finpy.newctf.pojo.Do.Article;
import com.finpy.newctf.pojo.Do.Category;
import com.finpy.newctf.pojo.Do.User;
import com.finpy.newctf.pojo.dto.ArticleInfo;
import com.finpy.newctf.pojo.dto.ArticlePage;
import com.finpy.newctf.pojo.vo.*;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

/**
 * &#064;Author 生姜の鱼丸
 * &#064;Time 2023/1/19 17:50
 * &#064;Info
 */
@Service
public class ArticleServiceImpl extends ServiceImpl<ArticleMapper, Article> implements ArticleService {
    final CategoryMapper categoryMapper;
    final UserMapper userMapper;

    public ArticleServiceImpl(CategoryMapper categoryMapper, UserMapper userMapper) {
        this.categoryMapper = categoryMapper;
        this.userMapper = userMapper;
    }

    @Override
    public ResUtils GetArticleList(ArticlePage page) {
        Page<Article> page1 = new Page<>(page.getPage(), page.getPageSize());
        QueryWrapper<Article> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().eq(page.getCategoryId() != 0, Article::getCategoryId, page.getCategoryId())
                .orderByDesc(Article::getUpdateTime); // 按照更新时间倒排列
        Page<Article> page2 = this.page(page1, queryWrapper);
        List<Article> list = page2.getRecords();
        if (list == null) {
            return ResUtils.fail(ResCode.DATA_ERROR);
        }
        // 整理数据
        List<ArticleListVo> collect = list.stream()
                .map(article -> {
                    ArticleListVo articleListVo = BeanUtil.copyBean(article, ArticleListVo.class);
                    Integer categoryId = article.getCategoryId();
                    Category category = categoryMapper.selectById(categoryId);
                    articleListVo.setCategoryName(category.getName());
                    return articleListVo;
                }).collect(Collectors.toList());
        PageVo pageVo = new PageVo(collect, page2.getTotal());
        return ResUtils.success(ResCode.GET_SUCCESS, pageVo);
    }

    @Override
    public ResUtils GetArticleInfo(Integer id) {
        Article article = getById(id);
        if (article == null) {
            return ResUtils.fail(ResCode.DATA_ERROR);
        }
        ArticleInfoVo articleInfoVo = BeanUtil.copyBean(article, ArticleInfoVo.class);

        Integer categoryId = article.getCategoryId();
        Category category = categoryMapper.selectById(categoryId);

        articleInfoVo.setCategoryName(category.getName());
        return ResUtils.success(ResCode.GET_SUCCESS, articleInfoVo);
    }

    @Override
    public ResUtils GetArticleUserInfo(Integer id) {
        User user = userMapper.selectById(id);
        if (user == null) {
            return ResUtils.fail(ResCode.DATA_ERROR);
        }
        UserArticleVo userArticleVo = BeanUtil.copyBean(user, UserArticleVo.class);
        return ResUtils.success(ResCode.GET_SUCCESS, userArticleVo);
    }

    @Override
    public ResUtils GetArticleUserInfos(String userName) {
        LambdaQueryWrapper<User> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(User::getUserName, userName);
        User user = userMapper.selectOne(queryWrapper);
        if (user == null) {
            return ResUtils.fail(ResCode.DATA_ERROR);
        }
        UserArticleVo userArticleVo = BeanUtil.copyBean(user, UserArticleVo.class);
        return ResUtils.success(ResCode.GET_SUCCESS, userArticleVo);
    }

    @Override
    public ResUtils SetArticle(ArticleInfo article, Integer userId) {
        Article article1 = BeanUtil.copyBean(article, Article.class);
        article1.setUserId(userId);

        LambdaQueryWrapper<Category> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(Category::getName, article.getCategoryName());
        Category category = categoryMapper.selectOne(queryWrapper);
        if (category == null) {
            return ResUtils.fail(ResCode.DATA_ERROR);
        }
        article1.setCategoryId(category.getId());
        // 设置随机图片
        // http://file.finpyx.cn/public/imgs/1.jpg
        Random random = new Random();
        int i = random.nextInt(20) + 1;
        String img = Constant.IMG + i + ".jpg";
        article1.setImg(img);

        boolean save = save(article1);
        if (!save) {
            return ResUtils.fail(ResCode.DATA_ERROR);
        }
        return ResUtils.success(ResCode.POST_SUCCESS);
    }

    @Override
    public ResUtils AdminGetArticleList(ArticlePage page) {
        Page<Article> page1 = new Page<>(page.getPage(), page.getPageSize());
        QueryWrapper<Article> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().eq(page.getCategoryId() != 0, Article::getCategoryId, page.getCategoryId());
        Page<Article> page2 = this.page(page1, queryWrapper);
        List<Article> list = page2.getRecords();
        if (list == null) {
            return ResUtils.fail(ResCode.DATA_ERROR);
        }
        // 整理数据
        List<AdminArticleListVo> collect = list.stream()
                .map(article -> {
                    AdminArticleListVo adminArticleListVo = BeanUtil.copyBean(article, AdminArticleListVo.class);
                    // 发布者昵称
                    User user = userMapper.selectById(article.getUserId());
                    adminArticleListVo.setAuthor(user.getNickName());
                    // 分类名
                    Category category = categoryMapper.selectById(article.getCategoryId());
                    adminArticleListVo.setCategoryName(category.getName());
                    return adminArticleListVo;
                }).collect(Collectors.toList());
        PageVo pageVo = new PageVo(collect, page2.getTotal());
        return ResUtils.success(ResCode.GET_SUCCESS, pageVo);
    }

    @Override
    public ResUtils DeleteArticle(Integer id) {
        boolean b = removeById(id);
        if (!b) {
            return ResUtils.fail(ResCode.OPERATE_ERROR);
        }
        return ResUtils.success(ResCode.OPERATE_SUCCESS);
    }

    @Override
    public ResUtils getUserArticleList(Integer id) {
        QueryWrapper<Article> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().eq(Article::getUserId, id).last("limit " + Constant.ARTICLE_LIST_NUM);
        List<Article> list = this.list(queryWrapper);
        if (list == null) {
            return ResUtils.fail(ResCode.DATA_ERROR);
        }
        List<UserArticleList> userArticleLists = BeanUtil.copyBeanList(list, UserArticleList.class);
        return ResUtils.success(ResCode.GET_SUCCESS, userArticleLists);
    }
}
