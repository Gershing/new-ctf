package com.finpy.newctf.Service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.finpy.newctf.Mapper.FileMapper;
import com.finpy.newctf.Service.FileService;
import com.finpy.newctf.Service.UserService;
import com.finpy.newctf.Utils.FileUtil;
import com.finpy.newctf.Utils.ResCode;
import com.finpy.newctf.Utils.ResUtils;
import com.finpy.newctf.pojo.Do.FileInfo;
import com.finpy.newctf.pojo.Do.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.File;

/**
 * &#064;Author 生姜の鱼丸
 * &#064;Time 2023/1/23 19:44
 * &#064;Info
 */
@Service
@Slf4j
public class FileServiceImpl extends ServiceImpl<FileMapper, FileInfo> implements FileService {
    final FileUtil fileUtil;
    final UserService userService;

    public FileServiceImpl(FileUtil fileUtil, UserService userService) {
        this.fileUtil = fileUtil;
        this.userService = userService;
    }

    @Override
    public ResUtils uploadFile(MultipartFile file) {
        FileInfo fileInfo = fileUtil.uploadFile(file);// 返回文件路径
        if (fileInfo == null) {
            log.info("文件上传失败！");
            return ResUtils.fail(ResCode.UPLOAD_ERROR);
        }
        boolean save = this.save(fileInfo);
        if (!save) {
            removeFileByName(fileInfo.getFileName());
            log.info("文件上传失败！文件已删除");
            return ResUtils.fail(ResCode.UPLOAD_ERROR);
        }
        return ResUtils.success(ResCode.UPLOAD_SUCCESS, fileInfo.getId());
    }

    @Override
    public ResUtils uploadAvatar(MultipartFile file, Integer id) {
        ResUtils resUtils = fileUtil.uploadQiniu(file);
        if (resUtils.getCode() == 801) {
            log.info("头像上传失败！");
            return resUtils;
        }
        // 上传成功之后返回文件连接
        String fileName = (String) resUtils.getData();
        // 将信息添加到数据库
        User user = new User();
        user.setId(id);
        user.setAvatar(fileName);
        boolean b = userService.updateById(user);
        if (!b) {
            return ResUtils.fail(ResCode.UPLOAD_ERROR);
        }
        return ResUtils.success(ResCode.UPLOAD_SUCCESS, fileName);
    }

    @Override
    public ResUtils downFile(Integer fileId, HttpServletResponse response) {
        FileInfo byId = this.getById(fileId);
        if (byId == null) {
            return ResUtils.fail(ResCode.FILE_NOT_EXISTENT);
        }
        return fileUtil.downFile(byId, response);
    }


    public void removeFileByName(String fileName) {
        File file = new File(fileName);
        if (file.exists()) {
            file.delete();
        }
    }
}
