package com.finpy.newctf.Service.impl;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.finpy.newctf.Config.Constant;
import com.finpy.newctf.Mapper.WebMapper;
import com.finpy.newctf.Service.*;
import com.finpy.newctf.Utils.*;
import com.finpy.newctf.pojo.Do.Error;
import com.finpy.newctf.pojo.Do.*;
import com.finpy.newctf.pojo.dto.AddWeb;
import com.finpy.newctf.pojo.dto.PageDto;
import com.finpy.newctf.pojo.dto.SubmitFlag;
import com.finpy.newctf.pojo.vo.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;

/**
 * &#064;Author 生姜の鱼丸
 * &#064;Time 2023/1/19 14:12
 * &#064;Info
 */
@Service
public class WebServiceImpl extends ServiceImpl<WebMapper, Web> implements WebService {
    final RedisBean redisBean;
    final OkService okService;
    final WebContainerService containerService;
    final ErrorService errorService;
    final UserService userService;
    final WriteupService writeupService;

    public WebServiceImpl(RedisBean redisBean, OkService okService, WebContainerService containerService, ErrorService errorService, UserService userService, WriteupService writeupService) {
        this.redisBean = redisBean;
        this.okService = okService;
        this.containerService = containerService;
        this.errorService = errorService;
        this.userService = userService;
        this.writeupService = writeupService;
    }

    @Override
    @Transactional
    public ResUtils OpenEnv(Integer id, Integer userId) {
        String redisId = id.toString() + userId.toString() + "";
        // 判断是否已经开启容器
        Object o = redisBean.get(redisId);
        if (o != null) {
            return ResUtils.fail(ResCode.DOCKER_REPEAT);
        }
        // 判断该用户是否已完成该题目
        QueryWrapper<Ok> okQueryWrapper = new QueryWrapper<>();
        okQueryWrapper.lambda().eq(Ok::getUid, userId).eq(Ok::getCId, id).eq(Ok::getCategory, "Web");
        long count = okService.count(okQueryWrapper);
        if (count > 0) {
            return ResUtils.fail(ResCode.IS_FINISHED);
        }
        // 1. 根据题目对应的image创建并开启容器
        Web web = getById(id);
        LambdaQueryWrapper<User> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(User::getId, userId);
        User user = userService.getOne(queryWrapper);
        if (web == null || user == null) {
            return ResUtils.fail(ResCode.DATA_ERROR);
        }
        String webImage = web.getWebImage();
        String dockerName = StringUtil.getDockerName();
        String flag = StringUtil.getFlag();


        // 2. 将题目id、用户id和端口号作为key，创建好的容器id作为value存入redis，设置时间为30分钟
        int testPort = new Random().nextInt(10000 - 9000);
        while (redisBean.get(redisId) != null) {
            testPort = new Random().nextInt(10000 - 9000);
        }
        Integer dstPort = 9000 + testPort;
        // 3. 开启容器
        String containerId = DockerUtil.ConnectDocker(dstPort, webImage, flag, dockerName);
        WebContainer container = new WebContainer();
        container.setContainerId(containerId);
        container.setContainerName(dockerName);
        container.setHttp(Constant.DOCKER_IP + ":" + dstPort + "/");
        container.setWebId(id);
        container.setWebFlag(flag);
        container.setWebPort(dstPort);
        container.setUserId(userId);

        LocalDateTime afterDate = LocalDateTime.now();
        container.setContainerDate(afterDate.plusMinutes(Constant.DOCKER_OVER_TIME));
        redisBean.set(redisId, JSONObject.toJSONString(container), Constant.DOCKER_OVER_TIME);
        // 4. 向数据库中添加数据
        containerService.save(container);
        // 4. 返回容器地址和容器结束时间
        Map<String, Object> res = new HashMap<>();
        res.put("http", Constant.DOCKER_IP + ":" + dstPort + "/");
        res.put("overTime", 30);
        return ResUtils.success(ResCode.DOCKER_OPEN_SUCCESS, res);
    }

    @Override
    public ResUtils getWebInfo(Integer id, Integer userId) {
        QueryWrapper<Web> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().eq(Web::getId, id);
        Web web = this.getOne(queryWrapper);
        if (web == null) {
            return ResUtils.fail(ResCode.DATA_ERROR);
        }
        // 根据用户id和题目id判断该用户是否已开启题目环境
        WebInfoVo webInfoVo = BeanUtil.copyBean(web, WebInfoVo.class);
        String key = id + "" + userId;
        if (redisBean.hasKey(key)) {
            WebContainer container = JSONObject.parseObject((String) redisBean.get(key), WebContainer.class);
            // 获取分钟差
            long time = StringUtil.getSecond(container.getContainerDate(), LocalDateTime.now());
            System.out.println(container.getContainerDate());
            webInfoVo.setHttp(container.getHttp());
            webInfoVo.setOverTime(time);
            webInfoVo.setStartTime(Constant.DOCKER_OVER_TIME * 60);
        } else {
            webInfoVo.setHttp("");
            webInfoVo.setOverTime(0);
        }
        User user = userService.getById(web.getWebAuthor());
        webInfoVo.setWebAuthor(user.getNickName());
        webInfoVo.setWebKnowledge(JSONObject.parseArray(web.getWebKnowledge(), String.class));
        return ResUtils.success(ResCode.GET_SUCCESS, webInfoVo);
    }

    @Override
    public ResUtils getWebList(PageDto page) {
        Page<Web> pages = new Page<>(page.getPage(), page.getPageSize());
        List<Web> records = this.page(pages).getRecords();
        if (records == null) {
            return ResUtils.fail(ResCode.DATA_ERROR);
        }
        List<WebVo> webVos = BeanUtil.copyBeanList(records, WebVo.class);
        return ResUtils.success(ResCode.GET_SUCCESS, webVos);
    }

    @Override
    public ResUtils addWeb(AddWeb web, Integer id) {
        // 判断是否存在相同题目
        QueryWrapper<Web> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().eq(Web::getWebName, web.getWebName());
        long count = this.count(queryWrapper);
        if (count > 0) {
            return ResUtils.fail(ResCode.TOPIC_POST_REPEAT);
        }

        Web web1 = BeanUtil.copyBean(web, Web.class);
        web1.setWebKnowledge(JSONObject.toJSONString(web.getWebKnowledge()));
        web1.setWebAuthor(id);
        boolean save = this.save(web1);
        if (!save) {
            return ResUtils.fail(ResCode.DATA_ERROR);
        }
        return ResUtils.success(ResCode.POST_SUCCESS);
    }

    @Override
    public ResUtils submitFlag(SubmitFlag flag, Integer userId) {
        String redisId = flag.getId().toString() + userId + "";
        // 通过题目id和用户id向redis中查询容器数据
        String o = (String) redisBean.get(redisId);
        // 判断容器是否存在
        if (StringUtil.isNull(o)) {
            return ResUtils.fail(ResCode.FLAG_IS_NOT_USE);
        }
        // 判断当前用户是否已经完成该题目
        QueryWrapper<Ok> okQueryWrapper = new QueryWrapper<>();
        okQueryWrapper.lambda().eq(Ok::getUid, userId).eq(Ok::getCId, flag.getId()).eq(Ok::getCategory, "Web");
        long count = okService.count(okQueryWrapper);
        if (count > 0) {
            return ResUtils.fail(ResCode.SUBMIT_REPEAT);
        }
        WebContainer container = JSON.parseObject(o, WebContainer.class);
        // 判断容器是否到期
        // 获取当前时间
        LocalDateTime now = LocalDateTime.now();
        boolean before = container.getContainerDate().isBefore(now);
        if (before) {
            return ResUtils.fail(ResCode.FLAG_IS_NOT_USE);
        }
        // 判断flag是否相同
        if (!container.getWebFlag().equals(flag.getFlag())) {
            errorService.addError(flag.getId(), userId, "Web");
            return ResUtils.fail(ResCode.FLAG_ERROR);
        }
        // flag提交正确
        // 添加记录
        okService.addOk(flag.getId(), userId, "Web");
        // 关闭容器
        EndEnv(flag.getId(), userId);
        // 删除redis中的数据
        redisBean.del(redisId);
        // 返回结果
        return ResUtils.success(ResCode.FLAG_CORRECT);
    }

    @Override
    public ResUtils GetSortAndWp(Integer id) {
        SortAndWpVo sortAndWpVo = new SortAndWpVo();
        // 获取排行榜信息
        QueryWrapper<Ok> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().orderByDesc(Ok::getCreateTime).eq(Ok::getCategory, "Web").last("limit " + Constant.CHALLENGE_SORT);
        List<Ok> list = okService.list(queryWrapper);
        if (list == null || list.size() == 0) {
            sortAndWpVo.setOk(null);
        } else {
            List<SortVo> collect = list.stream()
                    .map(ok -> {
                        SortVo sortVo = new SortVo();
                        sortVo.setTime(ok.getCreateTime());
                        sortVo.setNickName(userService.getById(ok.getUid()).getNickName());
                        return sortVo;
                    }).collect(Collectors.toList());
            sortAndWpVo.setOk(collect);
        }

        // 获取wp排行榜
        QueryWrapper<Writeup> queryWrapper1 = new QueryWrapper<>();
        queryWrapper1.lambda().orderByDesc(Writeup::getCreateTime).eq(Writeup::getCategoryId, 1).last("limit " + Constant.WRITEUP_SORT);
        List<Writeup> writeupList = writeupService.list(queryWrapper1);
        if (writeupList == null || writeupList.size() == 0) {
            sortAndWpVo.setWriteup(null);
        } else {
            List<WpSortVo> collect = writeupList.stream()
                    .map(writeup -> {
                        WpSortVo wpSortVo = new WpSortVo();
                        wpSortVo.setTitle(writeup.getTitle());
                        wpSortVo.setAuthor(userService.getById(writeup.getUserId()).getNickName());
                        wpSortVo.setTime(writeup.getCreateTime());
                        wpSortVo.setId(writeup.getId());
                        return wpSortVo;
                    }).collect(Collectors.toList());
            sortAndWpVo.setWriteup(collect);
        }
        return ResUtils.success(ResCode.GET_SUCCESS, sortAndWpVo);
    }

    @Override
    public ResUtils EndEnv(Integer id, Integer userId) {
        String redisId = id.toString() + userId.toString() + "";
        // 1. 根据题目id和用户idc从redis中获取容器信息
        String o = (String) redisBean.get(redisId);
        if (StringUtil.isNull(o)) {
            return ResUtils.fail(ResCode.DOCKER_CLOSE_ERROR);
        }
        WebContainer container = JSON.parseObject(o, WebContainer.class);
        // 2. 通过容器id删除容器
        DockerUtil.removeContainer(container.getContainerId());
        // 3. 删除redis中的数据
        redisBean.del(redisId);
        // 4. 不删除数据库中的数据
        // 5. 返回提示消息
        return ResUtils.success(ResCode.DOCKER_CLOSE_SUCCESS);
    }

    @Override
    public ResUtils getAdminWebList(PageDto page) {
        Page<Web> page1 = new Page<>(page.getPage(), page.getPageSize());
        Page<Web> page2 = this.page(page1);
        List<Web> list = page2.getRecords();
        if (list == null) {
            return ResUtils.fail(ResCode.DATA_ERROR);
        }
        List<AdminWebList> webLists = list.stream()
                .map(web -> {
                    AdminWebList webList = BeanUtil.copyBean(web, AdminWebList.class);
                    webList.setWebKnowledge(JSONObject.parseArray(web.getWebKnowledge(), String.class));

                    webList.setAuthor(userService.getById(web.getWebAuthor()).getNickName());

                    Integer id = web.getId();
                    QueryWrapper<WebContainer> query = new QueryWrapper<>();
                    query.lambda().eq(WebContainer::getWebId, id);
                    webList.setContainerNum(containerService.count(query));

                    QueryWrapper<Ok> okQuery = new QueryWrapper<>();
                    okQuery.lambda().eq(Ok::getCId, id).eq(Ok::getCategory, "Web");
                    webList.setWebOk(okService.count(okQuery));

                    return webList;
                }).collect(Collectors.toList());
        PageVo pageVo = new PageVo(webLists, page2.getTotal());
        return ResUtils.success(ResCode.GET_SUCCESS, pageVo);
    }

    @Override
    public ResUtils getAdminWebInfo(Integer id) {
        Web web = this.getById(id);
        if (web == null) {
            return ResUtils.fail(ResCode.DATA_ERROR);
        }
        AdminWebInfo adminWebInfo = BeanUtil.copyBean(web, AdminWebInfo.class);

        QueryWrapper<Ok> okQueryWrapper = new QueryWrapper<>();
        long okNum = okService.count(okQueryWrapper.lambda().eq(Ok::getCId, id));
        adminWebInfo.setOkNum(okNum);

        QueryWrapper<Error> errorQuery = new QueryWrapper<>();
        long errorNum = errorService.count(errorQuery.lambda().eq(Error::getCId, id));
        adminWebInfo.setErrorNum(errorNum);

        adminWebInfo.setAuthor(userService.getById(web.getWebAuthor()).getNickName());
        adminWebInfo.setWebKnowledge(JSONObject.parseArray(web.getWebKnowledge(), String.class));
        return ResUtils.success(ResCode.GET_SUCCESS, adminWebInfo);
    }

    @Override
    public ResUtils deleteWeb(Integer id) {
        boolean b = this.removeById(id);
        if (!b) {
            return ResUtils.fail(ResCode.DATA_ERROR);
        }
        return ResUtils.success(ResCode.DELETE_SUCCESS);
    }
}
