package com.finpy.newctf.Service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.finpy.newctf.Mapper.OkMapper;
import com.finpy.newctf.Service.OkService;
import com.finpy.newctf.pojo.Do.Ok;
import org.springframework.stereotype.Service;

/**
 * &#064;Author 生姜の鱼丸
 * &#064;Time 2023/1/19 16:12
 * &#064;Info
 */
@Service
public class OkServiceImpl extends ServiceImpl<OkMapper, Ok> implements OkService {
    @Override
    public void addOk(Integer id, Integer userId, String categoryName) {
        Ok ok = new Ok();
        ok.setCategory(categoryName);
        ok.setCId(id);
        ok.setUid(userId);
        this.save(ok);
    }
}
