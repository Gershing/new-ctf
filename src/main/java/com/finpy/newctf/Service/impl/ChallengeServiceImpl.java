package com.finpy.newctf.Service.impl;

import com.alibaba.fastjson.JSONObject;
import com.finpy.newctf.Mapper.CategoryMapper;
import com.finpy.newctf.Mapper.FileMapper;
import com.finpy.newctf.Mapper.UserMapper;
import com.finpy.newctf.Service.*;
import com.finpy.newctf.Utils.BeanUtil;
import com.finpy.newctf.Utils.ResCode;
import com.finpy.newctf.Utils.ResUtils;
import com.finpy.newctf.Utils.StringUtil;
import com.finpy.newctf.pojo.Do.*;
import com.finpy.newctf.pojo.dto.ChallengeInfo;
import com.finpy.newctf.pojo.dto.DeleteChallenge;
import com.finpy.newctf.pojo.dto.UpdateChallenge;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class ChallengeServiceImpl implements ChallengeService {
    final UserMapper userMapper;
    final MiscService miscService;
    final WebService webService;
    final ReverseService reverseService;
    final CryptoService cryptoService;
    final PwnService pwnService;
    final CategoryMapper categoryMapper;
    final FileMapper fileMapper;

    @Autowired
    public ChallengeServiceImpl(UserMapper userMapper, MiscService miscService, WebService webService, ReverseService reverseService, CryptoService cryptoService, PwnService pwnService, CategoryMapper categoryMapper, FileMapper fileMapper) {
        this.userMapper = userMapper;
        this.miscService = miscService;
        this.webService = webService;
        this.reverseService = reverseService;
        this.cryptoService = cryptoService;
        this.pwnService = pwnService;
        this.categoryMapper = categoryMapper;
        this.fileMapper = fileMapper;
    }

    @Override
    public ResUtils UpdateChallenge(UpdateChallenge challenge) {
        if (StringUtil.intIsNull(challenge.getId())) {
            return ResUtils.fail(ResCode.REQUEST_PARAM_ERROR);
        }
        // 校验数据
        String categoryName = StringUtil.getCategoryNameById(challenge.getCategory());
        boolean b = false;
        // 判断数据完成性
        // 除id以外，其他数据为空表示不更改
        // 向数据库添加数据
        if (categoryName.equals("Web")) {
            Web web = BeanUtil.copyBean(challenge, Web.class);
            web.setWebKnowledge(JSONObject.toJSONString(challenge.getKnowledge()));
            web.setWebDescribe(challenge.getDescribe());
            web.setWebImage(challenge.getImage());
            web.setWebName(challenge.getName());
            web.setWebScores(challenge.getScores());
            b = webService.updateById(web);
        }

        if (categoryName.equals("Misc")) {
            Misc misc = BeanUtil.copyBean(challenge, Misc.class);
            misc.setMiscKnowledge(JSONObject.toJSONString(challenge.getKnowledge()));
            misc.setMiscDescribe(challenge.getDescribe());
            misc.setMiscFile(challenge.getFileId());
            misc.setMiscName(challenge.getName());
            misc.setMiscScores(challenge.getScores());
            b = miscService.updateById(misc);
        }

        if (categoryName.equals("Pwn")) {
            Pwn pwn = new Pwn();
            pwn.setId(challenge.getId());
            pwn.setPwnKnowledge(JSONObject.toJSONString(challenge.getKnowledge()));
            pwn.setPwnDescribe(challenge.getDescribe());
            pwn.setPwnFile(challenge.getFileId());
            pwn.setPwnFlag(challenge.getFlag());
            pwn.setPwnName(challenge.getName());
            pwn.setPwnScores(challenge.getScores());
            b = pwnService.updateById(pwn);
        }

        if (categoryName.equals("Reverse")) {
            Reverse reverse = new Reverse();
            reverse.setId(challenge.getId());
            reverse.setResKnowledge(JSONObject.toJSONString(challenge.getKnowledge()));
            reverse.setResDescribe(challenge.getDescribe());
            reverse.setResFile(challenge.getFileId());
            reverse.setResFlag(challenge.getFlag());
            reverse.setResName(challenge.getName());
            reverse.setResScores(challenge.getScores());
            b = reverseService.updateById(reverse);
        }

        if (categoryName.equals("Crypto")) {
            Crypto crypto = new Crypto();
            crypto.setId(challenge.getId());
            crypto.setCryKnowledge(JSONObject.toJSONString(challenge.getKnowledge()));
            crypto.setCryFile(challenge.getFileId());
            crypto.setCryFlag(challenge.getFlag());
            crypto.setCryName(challenge.getName());
            crypto.setCryScores(challenge.getScores());
            crypto.setCryDescribe(challenge.getDescribe());
            b = cryptoService.updateById(crypto);
        }

        if (!b) {
            return ResUtils.fail(ResCode.DATA_ERROR);
        }
        return ResUtils.success(ResCode.MODIFY_SUCCESS);
    }


    @Override
    public ResUtils getChallengeInfo(ChallengeInfo info, Integer id) {
        Category category = categoryMapper.selectById(info.getCategoryId());
        if (category == null) {
            return ResUtils.fail(ResCode.DATA_ERROR);
        }

        if (category.getName().equals("Web")) {
            return webService.getWebInfo(info.getId(), id);
        }

        if (category.getName().equals("Misc")) {
            return miscService.getMiscInfo(info.getId());
        }

        if (category.getName().equals("Pwn")) {
            return pwnService.getPwnInfo(info.getId());
        }

        if (category.getName().equals("Reverse")) {
            return reverseService.getReverseInfo(info.getId());
        }

        if (category.getName().equals("Crypto")) {
            return cryptoService.getCryptoInfo(info.getId());
        }
        return ResUtils.fail(ResCode.NOT_OPEN);
    }

    @Override
    public ResUtils getAdminChallengeInfo(ChallengeInfo challengeInfo) {
        Category category = categoryMapper.selectById(challengeInfo.getCategoryId());
        if (category == null) {
            return ResUtils.fail(ResCode.DATA_ERROR);
        }

        if (category.getName().equals("Web")) {
            return webService.getAdminWebInfo(challengeInfo.getId());
        }

        if (category.getName().equals("Misc")) {
            return miscService.getAdminMiscInfo(challengeInfo.getId());
        }

        if (category.getName().equals("Pwn")) {
            return pwnService.getAdminPwnInfo(challengeInfo.getId());
        }

        if (category.getName().equals("Reverse")) {
            return reverseService.getAdminReverseInfo(challengeInfo.getId());
        }

        if (category.getName().equals("Crypto")) {
            return cryptoService.getAdminCryptoInfo(challengeInfo.getId());
        }
        return ResUtils.fail(ResCode.NOT_OPEN);
    }

    @Override
    public ResUtils deleteChallenge(DeleteChallenge delChallenge) {
        if (delChallenge.getCategoryId() == 1) {
            return webService.deleteWeb(delChallenge.getId());
        }
        if (delChallenge.getCategoryId() == 2) {
            return miscService.deleteMisc(delChallenge.getId());
        }
        if (delChallenge.getCategoryId() == 3) {
            return pwnService.deletePwn(delChallenge.getId());
        }
        if (delChallenge.getCategoryId() == 4) {
            return reverseService.deleteReverse(delChallenge.getId());
        }
        if (delChallenge.getCategoryId() == 5) {
            return cryptoService.deleteCrypto(delChallenge.getId());
        }
        return ResUtils.fail(ResCode.REQUEST_PARAM_ERROR);
    }
}
