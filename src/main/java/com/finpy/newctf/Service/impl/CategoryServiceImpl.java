package com.finpy.newctf.Service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.finpy.newctf.Mapper.CategoryMapper;
import com.finpy.newctf.Service.CategoryService;
import com.finpy.newctf.Utils.BeanUtil;
import com.finpy.newctf.Utils.ResCode;
import com.finpy.newctf.Utils.ResUtils;
import com.finpy.newctf.pojo.Do.Category;
import com.finpy.newctf.pojo.vo.CategoryVo;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * &#064;Author 生姜の鱼丸
 * &#064;Time 2023/1/19 12:27
 * &#064;Info 题目分类
 */
@Service
public class CategoryServiceImpl extends ServiceImpl<CategoryMapper, Category> implements CategoryService {
    @Override
    public ResUtils getCategory() {
        List<Category> list = this.list();
        if (list == null) {
            return ResUtils.fail(ResCode.DATA_ERROR);
        }
        List<CategoryVo> categoryVos = BeanUtil.copyBeanList(list, CategoryVo.class);
        return ResUtils.success(ResCode.GET_SUCCESS, categoryVos);
    }
}
