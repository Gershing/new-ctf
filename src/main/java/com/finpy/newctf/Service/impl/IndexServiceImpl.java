package com.finpy.newctf.Service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.finpy.newctf.Mapper.IndexMapper;
import com.finpy.newctf.Service.*;
import com.finpy.newctf.Utils.BeanUtil;
import com.finpy.newctf.Utils.ResCode;
import com.finpy.newctf.Utils.ResUtils;
import com.finpy.newctf.Utils.StringUtil;
import com.finpy.newctf.pojo.Do.Notice;
import com.finpy.newctf.pojo.Do.Ok;
import com.finpy.newctf.pojo.Do.Settings;
import com.finpy.newctf.pojo.Do.User;
import com.finpy.newctf.pojo.vo.Index;
import com.finpy.newctf.pojo.vo.IndexChallenge;
import com.finpy.newctf.pojo.vo.NoticeVo;
import com.finpy.newctf.pojo.vo.UserSortVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class IndexServiceImpl implements IndexService {
    final MiscService miscService;
    final WebService webService;
    final UserService userService;
    final ArticleService articleService;
    final NoticeService noticeService;
    final SettingsService settingsService;
    final OkService okService;
    final IndexMapper indexMapper;

    public IndexServiceImpl(MiscService miscService, WebService webService, UserService userService, ArticleService articleService, NoticeService noticeService, SettingsService settingsService, OkService okService, IndexMapper indexMapper) {
        this.miscService = miscService;
        this.webService = webService;
        this.userService = userService;
        this.articleService = articleService;
        this.noticeService = noticeService;
        this.settingsService = settingsService;
        this.okService = okService;
        this.indexMapper = indexMapper;
    }

    @Override
    public ResUtils Index() {
        Index index = new Index();
        IndexChallenge challenge = new IndexChallenge();
        // 获取题目数
        long miscCount = miscService.count();
        long webCount = webService.count();
        challenge.setAll(miscCount + webCount);
        challenge.setWeb(webCount);
        challenge.setMisc(miscCount);
        challenge.setPwn(0);
        challenge.setReal(0);
        challenge.setReverse(0);
        challenge.setCrypto(0);
        challenge.setOther(0);
        // 获取文章数
        long articleCount = articleService.count();

        // 获取用户数
        long userCount = userService.count();

        // 获取最新公告信息
        LambdaQueryWrapper<Notice> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.orderByDesc(Notice::getUpdateTime);
        queryWrapper.last("limit 1");
        Notice notice = noticeService.getOne(queryWrapper);
        NoticeVo noticeVo = BeanUtil.copyBean(notice, NoticeVo.class);

        // 获取用户排名信息
        LambdaQueryWrapper<User> userQueryWrapper = new LambdaQueryWrapper<>();
        userQueryWrapper.orderByDesc(User::getIntegral);
        // 获取前n条数据
        Settings settings = settingsService.getById(1);
        userQueryWrapper.last("limit " + settings.getIndexSortNum());
        List<User> list = userService.list();
        List<UserSortVo> userCollect = list.stream()
                .map(user -> {
                    UserSortVo userSortVo = BeanUtil.copyBean(user, UserSortVo.class);
                    userSortVo.setGrade(StringUtil.getSign(user.getIntegral()));
                    userSortVo.setRole(StringUtil.getUserRole(user.getUserRole()));
                    // 获取解题量
                    LambdaQueryWrapper<Ok> okLambdaQueryWrapper = new LambdaQueryWrapper<>();
                    okLambdaQueryWrapper.eq(Ok::getUid, user.getId());
                    userSortVo.setOkNum(okService.count(okLambdaQueryWrapper));
                    return userSortVo;
                }).collect(Collectors.toList());

        index.setChallenge(challenge);
        index.setArticle(articleCount);
        index.setUser(userCount);
        index.setSort(userCollect);
        index.setNotice(noticeVo);
        return ResUtils.success(ResCode.GET_SUCCESS, index);
    }
}
