package com.finpy.newctf.Service.impl;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.finpy.newctf.Mapper.CategoryMapper;
import com.finpy.newctf.Mapper.UserMapper;
import com.finpy.newctf.Mapper.WebMapper;
import com.finpy.newctf.Mapper.WriteupMapper;
import com.finpy.newctf.Service.WriteupService;
import com.finpy.newctf.Utils.BeanUtil;
import com.finpy.newctf.Utils.ResCode;
import com.finpy.newctf.Utils.ResUtils;
import com.finpy.newctf.Utils.StringUtil;
import com.finpy.newctf.pojo.Do.Category;
import com.finpy.newctf.pojo.Do.User;
import com.finpy.newctf.pojo.Do.Web;
import com.finpy.newctf.pojo.Do.Writeup;
import com.finpy.newctf.pojo.dto.WriteupPage;
import com.finpy.newctf.pojo.vo.AdminWpVo;
import com.finpy.newctf.pojo.vo.PageVo;
import com.finpy.newctf.pojo.vo.WriteupInfoVo;
import com.finpy.newctf.pojo.vo.WriteupVo;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * &#064;Author 生姜の鱼丸
 * &#064;Time 2023/1/16 19:40
 * &#064;Info
 */
@Service
public class WriteupServiceImpl extends ServiceImpl<WriteupMapper, Writeup> implements WriteupService {

    final WriteupMapper writeupMapper;
    final CategoryMapper categoryMapper;
    final UserMapper userMapper;
    final WebMapper webMapper;

    public WriteupServiceImpl(WriteupMapper writeupMapper, CategoryMapper categoryMapper, UserMapper userMapper, WebMapper webMapper) {
        this.writeupMapper = writeupMapper;
        this.categoryMapper = categoryMapper;
        this.userMapper = userMapper;
        this.webMapper = webMapper;
    }

    @Override
    public ResUtils GetWriteupList(WriteupPage writeupPage) {
        Page<Writeup> page = new Page<>(writeupPage.getPage(), writeupPage.getPageSize());
        LambdaQueryWrapper<Writeup> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(writeupPage.getCategoryId() != 0, Writeup::getCategoryId, writeupPage.getCategoryId());
        queryWrapper.orderByDesc(Writeup::getUpdateTime);
        Page<Writeup> writeupPage1 = writeupMapper.selectPage(page, queryWrapper);
        List<Writeup> records = writeupPage1.getRecords();

        if (records == null) {
            return ResUtils.fail(ResCode.GET_ERROR);
        }
        // 数据整理
        List<WriteupVo> collect = records.stream()
                .map(writeup -> {
                    WriteupVo writeupVo = BeanUtil.copyBean(writeup, WriteupVo.class);
                    writeupVo.setCategoryName(categoryMapper.selectById(writeup.getCategoryId()).getName());
                    return writeupVo;
                }).collect(Collectors.toList());
        PageVo pageVo = new PageVo(collect, writeupPage1.getTotal());
        return ResUtils.success(ResCode.GET_SUCCESS, pageVo);
    }

    @Override
    public ResUtils GetWriteupListAndCategory(WriteupPage writeupPage) {
        Page<Writeup> page = new Page<>(writeupPage.getPage(), writeupPage.getPageSize());
        LambdaQueryWrapper<Writeup> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(writeupPage.getCategoryId() != 0, Writeup::getCategoryId, writeupPage.getCategoryId());
        Page<Writeup> writeupPage1 = writeupMapper.selectPage(page, queryWrapper);
        List<Writeup> records = writeupPage1.getRecords();
        if (records == null) {
            return ResUtils.fail(ResCode.GET_ERROR);
        }
        // 数据整理 获取分类名和用户名
        List<AdminWpVo> collect = records.stream()
                .map(writeup -> {
                    AdminWpVo adminWpVo = BeanUtil.copyBean(writeup, AdminWpVo.class);
                    Category category = categoryMapper.selectById(writeup.getCategoryId());
                    User user = userMapper.selectById(writeup.getUserId());
                    adminWpVo.setCategoryName(category.getName());
                    adminWpVo.setAuthor(user.getNickName());
                    return adminWpVo;
                }).collect(Collectors.toList());
        PageVo pageVo = new PageVo(collect, writeupPage1.getTotal());
        // 获取所有分类
        List<Category> categories = categoryMapper.selectList(null);

        Map<String, Object> res = new HashMap<>();
        res.put("wp", pageVo);
        res.put("category", categories);

        return ResUtils.success(ResCode.GET_SUCCESS, res);
    }

    @Override
    public ResUtils GetWriteupInfoById(Integer id) {
        // 获取数据
        Writeup writeup = getById(id);
        // 判断是否为空
        if (writeup == null) {
            return ResUtils.fail(ResCode.GET_ERROR);
        }
        WriteupInfoVo writeupInfoVo = BeanUtil.copyBean(writeup, WriteupInfoVo.class);

        // 获取分类名
        Integer categoryId = writeup.getCategoryId();
        Category category = categoryMapper.selectById(categoryId);
        writeupInfoVo.setCategoryName(category.getName());
        // 获取作者名
        Integer userId = writeup.getUserId();
        User user = userMapper.selectById(userId);
        writeupInfoVo.setNickName(user.getNickName());
        // 获取对应的题目名
        Integer cId = writeup.getCId();
        String name = category.getName();
        if (name.equals("Web")) {
            Web web = webMapper.selectById(cId);
            writeupInfoVo.setChallenge(web.getWebName());
        }

        return ResUtils.success(ResCode.GET_SUCCESS, writeupInfoVo);
    }

    @Override
    public ResUtils DeleteWriteupById(Integer id) {
        boolean b = removeById(id);
        if (!b) {
            return ResUtils.fail(ResCode.OPERATE_ERROR);
        }
        return ResUtils.success(ResCode.DELETE_SUCCESS);
    }

    @Override
    public ResUtils AddWriteUp(Writeup writeup, Integer id) {
        writeup.setUserId(id);
        writeup.setImg(StringUtil.getImg());
        boolean save = this.save(writeup);
        if (!save) {
            return ResUtils.fail(ResCode.DATA_ERROR);
        }
        return ResUtils.success(ResCode.POST_SUCCESS);
    }
}
