package com.finpy.newctf.Service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.finpy.newctf.Config.Constant;
import com.finpy.newctf.Mapper.OkMapper;
import com.finpy.newctf.Mapper.UserMapper;
import com.finpy.newctf.Service.ArticleService;
import com.finpy.newctf.Service.UserService;
import com.finpy.newctf.Service.WriteupService;
import com.finpy.newctf.Utils.*;
import com.finpy.newctf.pojo.Do.Article;
import com.finpy.newctf.pojo.Do.Ok;
import com.finpy.newctf.pojo.Do.User;
import com.finpy.newctf.pojo.Do.Writeup;
import com.finpy.newctf.pojo.dto.ListData;
import com.finpy.newctf.pojo.dto.Register;
import com.finpy.newctf.pojo.dto.UserLogin;
import com.finpy.newctf.pojo.dto.UserPage;
import com.finpy.newctf.pojo.vo.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.DigestUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {
    final UserMapper userMapper;
    final RedisBean redisBean;
    final OkMapper okMapper;
    final ArticleService articleService;
    final WriteupService writeupService;

    @Autowired
    public UserServiceImpl(UserMapper userMapper, RedisBean redisBean, OkMapper okMapper, ArticleService articleService, WriteupService writeupService) {
        this.userMapper = userMapper;
        this.redisBean = redisBean;
        this.okMapper = okMapper;
        this.articleService = articleService;
        this.writeupService = writeupService;
    }

    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/19 14:53
     * @Info 判断是不是管理员，用于后台页面
     */
    @Override
    public ResUtils isAdmin(String userName) {
        LambdaQueryWrapper<User> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(User::getUserName, userName);
        User user = getOne(queryWrapper);
        if (user == null) {
            return ResUtils.fail(ResCode.USER_IS_NOT_EXIST);
        }
        if (user.getUserRole() != 0) {
            return ResUtils.fail(ResCode.IS_NOT_ADMIN);
        }
        return ResUtils.success(ResCode.IS_ADMIN);
    }

    @Override
    public ResUtils GetUserInfo(String userName) {
        LambdaQueryWrapper<User> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(User::getUserName, userName);
        User user = getOne(queryWrapper);

        UserInfoVo userInfoVo = BeanUtil.copyBean(user, UserInfoVo.class);

        userInfoVo.setRole(StringUtil.getUserRole(user.getUserRole()));
        return ResUtils.success(ResCode.GET_SUCCESS, userInfoVo);
    }

    @Override
    public ResUtils GetUserList(UserPage userPage) {
        // 获取用户列表
        List<User> user = list();
        if (user == null) {
            return ResUtils.fail(ResCode.DATA_ERROR);
        }
        // 获取每个用户的 正确率 和 等级
        List<UserListVo> collect = user.stream()
                .map(user1 -> {
                    UserListVo userListVo = BeanUtil.copyBean(user1, UserListVo.class);
                    userListVo.setUserRole(StringUtil.getUserRole(user1.getUserRole()));
                    // 获取正确数量
                    LambdaQueryWrapper<Ok> okWrapper = new LambdaQueryWrapper<>();
                    okWrapper.eq(Ok::getUid, user1.getId());
                    Long ok = okMapper.selectCount(okWrapper);
                    userListVo.setCorrectNum(ok.intValue());
                    // 获取用户等级
                    userListVo.setGrade(StringUtil.getSign(user1.getIntegral()));
                    return userListVo;
                }).collect(Collectors.toList());

        return ResUtils.success(ResCode.GET_SUCCESS, collect);
    }

    @Override
    public ResUtils GetUserOkNum(Integer id) {
        // 获取用户做题数据
        String[] types = new String[]{"Web", "Misc", "Pwn", "Reverse", "Crypto", "Real", "Other"};
        List<Long> okInfos = new ArrayList<>();
        for (String type : types) {
            LambdaQueryWrapper<Ok> queryWrapper = new LambdaQueryWrapper<>();
            queryWrapper.eq(Ok::getCategory, type);
            queryWrapper.eq(Ok::getUid, id);
            Long aLong = okMapper.selectCount(queryWrapper);
            okInfos.add(aLong);
        }
        return ResUtils.success(ResCode.GET_SUCCESS, okInfos);
    }

    @Override
    public ResUtils EndUserByUid(Integer id) {
        LambdaUpdateWrapper<User> lambdaUpdateWrapper = new LambdaUpdateWrapper<>();
        lambdaUpdateWrapper.eq(User::getId, id).set(User::getUserStatus, 1);
        boolean update = update(lambdaUpdateWrapper);
        if (!update) {
            return ResUtils.fail(ResCode.OPERATE_ERROR);
        }
        return ResUtils.success(ResCode.OPERATE_SUCCESS);
    }

    @Override
    public ResUtils StartUserByUid(Integer id) {
        LambdaUpdateWrapper<User> lambdaUpdateWrapper = new LambdaUpdateWrapper<>();
        lambdaUpdateWrapper.eq(User::getId, id).set(User::getUserStatus, 0);
        boolean update = update(lambdaUpdateWrapper);
        if (!update) {
            return ResUtils.fail(ResCode.OPERATE_ERROR);
        }
        return ResUtils.success(ResCode.OPERATE_SUCCESS);
    }

    @Override
    @Transactional
    public ResUtils AddAdminByUid(ListData ids) {
        for (Integer id : ids.getIds()) {
            LambdaUpdateWrapper<User> updateWrapper = new LambdaUpdateWrapper<>();
            updateWrapper.eq(User::getId, id).set(User::getUserRole, 0);
            boolean update = update(updateWrapper);
            if (!update) {
                return ResUtils.fail(ResCode.ADD_ERROR);
            }
        }

        return ResUtils.success(ResCode.ADD_SUCCESS);
    }

    @Override
    @Transactional
    public ResUtils DeleteUserByUid(ListData ids) {
        for (Integer id : ids.getIds()) {
            LambdaQueryWrapper<User> queryWrapper = new LambdaQueryWrapper<>();
            queryWrapper.eq(User::getId, id);
            boolean remove = remove(queryWrapper);
            if (!remove) {
                return ResUtils.fail(ResCode.ADD_ERROR);
            }
        }
        return ResUtils.fail(ResCode.ADD_ERROR);
    }

    @Override
    public ResUtils Login(UserLogin userLogin) {
        // 验证 验证码
        String captchaValue = (String) redisBean.get(userLogin.getCaptchaId());
        if (!captchaValue.equals(userLogin.getCaptcha()) || StringUtil.isNull(captchaValue)) {
            return ResUtils.fail(ResCode.CAPTCHA_ERROR);
        }
        // 用户验证
        LambdaQueryWrapper<User> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(User::getUserName, userLogin.getUserName());
        User user = userMapper.selectOne(queryWrapper);
        // 判断用户数据不存在，用户未注册
        if (user == null) {
            return ResUtils.fail(ResCode.USER_IS_NOT_EXIST);
        }
        // 判断账户是否可用
        if (user.getUserStatus() == 1) {
            return ResUtils.fail(ResCode.USER_IS_NOT_TO_USE);
        }
        // 验证密码
        if (!user.getUserPassword().equals(StringUtil.getMd5(userLogin.getPassWord(), user.getSalt()))) {
            return ResUtils.fail(ResCode.PASSWORD_ERROR);
        }
        // 获取token
        String token = JwtUtil.sign(user);
        // 一切正常，返回数据
        UserVo userVo = new UserVo(token, user.getNickName(), user.getUserRole());
        return ResUtils.success(ResCode.LOGIN_SUCCESS, userVo);
    }

    @Override
    public ResUtils EditUserInfo(User user) {
        LambdaQueryWrapper<User> queryWrapper = new LambdaQueryWrapper<>();
        // 修改密码
        if (StringUtil.isNotNull(user.getUserPassword())) {
            queryWrapper.eq(User::getUserName, user.getUserName());
            User one = getOne(queryWrapper);
            user.setUserPassword(StringUtil.getMd5(user.getUserPassword(), one.getSalt()));
        }
        // 没有修改密码
        queryWrapper.eq(User::getUserName, user.getUserName());
        boolean update = update(user, queryWrapper);
        if (!update) {
            return ResUtils.fail(ResCode.OPERATE_ERROR);
        }
        return ResUtils.success(ResCode.OPERATE_SUCCESS);
    }

    @Override
    public ResUtils Register(Register register) {
        String captchaValue = (String) redisBean.get(register.getCaptchaId());
        log.info("Redis验证码：" + captchaValue);
        if (!captchaValue.equals(register.getCaptcha()) || StringUtil.isNull(captchaValue)) {
            return ResUtils.fail(ResCode.CAPTCHA_ERROR);
        }
        // 验证码正确，判断用户名是否已存在
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().eq(User::getUserName, register.getUserName());
        User user1 = userMapper.selectOne(queryWrapper);
        if (user1 != null) {
            return ResUtils.fail(ResCode.USER_IS_EXIST);
        }
        String salt = SaltUtils.getSalt(Constant.SALT_LENGTH);
        String newPwd = salt + register.getPassWord() + salt;
        String Md5Pwd = DigestUtils.md5DigestAsHex(newPwd.getBytes());
        // 向数据库中添加数据
        User user = BeanUtil.copyBean(register, User.class);
        user.setUserPassword(Md5Pwd);
        user.setSalt(salt);
        boolean save = this.save(user);
        if (!save) {
            return ResUtils.fail(ResCode.REGISTER_ERROR);
        }
        return ResUtils.success(ResCode.REGISTER_SUCCESS);
    }

    @Override
    public ResUtils GetUserSort(UserPage page) {
        Page<User> page1 = new Page<>(page.getPage(), page.getPageSize());
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().orderByDesc(User::getIntegral);
        Page<User> userPage = this.page(page1, queryWrapper);
        List<User> records = userPage.getRecords();
        if (records == null) {
            return ResUtils.fail(ResCode.DATA_ERROR);
        }
        List<UserSortVo> collect = records.stream()
                .map(user -> {
                    UserSortVo userSortVo = BeanUtil.copyBean(user, UserSortVo.class);
                    userSortVo.setGrade(StringUtil.getSign(user.getIntegral()));
                    QueryWrapper<Ok> okQueryWrapper = new QueryWrapper<>();
                    okQueryWrapper.lambda().eq(Ok::getUid, user.getId());
                    userSortVo.setOkNum(okMapper.selectCount(okQueryWrapper));
                    userSortVo.setRole(StringUtil.getUserRole(user.getUserRole()));
                    return userSortVo;
                }).collect(Collectors.toList());
        return ResUtils.success(ResCode.GET_SUCCESS, collect);
    }

    @Override
    public ResUtils GetSortInfo() {
        // 获取所有用户
        List<User> users = userMapper.selectList(null);
        if (users == null) {
            return ResUtils.fail(ResCode.DATA_ERROR);
        }
        List<UserChallengeData> collect = users.stream()
                .map(user -> {
                    UserChallengeData data = new UserChallengeData();
                    QueryWrapper<Ok> queryWrapper = new QueryWrapper<>();
                    queryWrapper.lambda().eq(Ok::getUid, user.getId());
                    data.setNickName(user.getNickName());
                    ResUtils resUtils = this.GetUserOkNum(user.getId());
                    data.setValue(resUtils.getData());
                    return data;
                }).collect(Collectors.toList());
        return ResUtils.success(ResCode.GET_SUCCESS, collect);
    }

    @Override
    public ResUtils GetUserSortInfo(Integer id) {
        /*
        {
            name:"Web",
            value:10
        }
         */
        String[] types = new String[]{"Web", "Misc", "Pwn", "Reverse", "Crypto", "Real", "Other"};
        List<CategoryAndValue> collect = Arrays.stream(types).map(str -> {
            CategoryAndValue data = new CategoryAndValue();
            QueryWrapper<Ok> queryWrapper = new QueryWrapper<>();
            queryWrapper.lambda().eq(Ok::getUid, id).eq(Ok::getCategory, str);
            Long aLong = okMapper.selectCount(queryWrapper);
            data.setValue(aLong);
            data.setName(str);
            return data;
        }).collect(Collectors.toList());
        return ResUtils.success(ResCode.GET_SUCCESS, collect);
    }

    @Override
    public ResUtils getUserInfo(Integer id) {
        User user = this.getById(id);
        if (user == null) {
            return ResUtils.fail(ResCode.DATA_ERROR);
        }
        UserArticleVo userArticleVo = BeanUtil.copyBean(user, UserArticleVo.class);
        // 获取文章数
        QueryWrapper<Article> articleQueryWrapper = new QueryWrapper<>();
        articleQueryWrapper.lambda().eq(Article::getUserId, id);
        long articles = articleService.count(articleQueryWrapper);
        userArticleVo.setArticleNum(articles);
        // 获取解题数
        QueryWrapper<Ok> okQueryWrapper = new QueryWrapper<>();
        okQueryWrapper.lambda().eq(Ok::getUid, id);
        Long oks = okMapper.selectCount(okQueryWrapper);
        userArticleVo.setOkNum(oks);
        // 获取wp数量
        QueryWrapper<Writeup> wrapper = new QueryWrapper<>();
        wrapper.lambda().eq(Writeup::getUserId, id);
        long writes = writeupService.count(wrapper);
        userArticleVo.setWpNum(writes);
        return ResUtils.success(ResCode.GET_SUCCESS, userArticleVo);
    }
}