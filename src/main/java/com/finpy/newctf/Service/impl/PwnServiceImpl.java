package com.finpy.newctf.Service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.finpy.newctf.Config.Constant;
import com.finpy.newctf.Mapper.PwnMapper;
import com.finpy.newctf.Service.*;
import com.finpy.newctf.Utils.BeanUtil;
import com.finpy.newctf.Utils.ResCode;
import com.finpy.newctf.Utils.ResUtils;
import com.finpy.newctf.pojo.Do.Error;
import com.finpy.newctf.pojo.Do.*;
import com.finpy.newctf.pojo.dto.AddPwn;
import com.finpy.newctf.pojo.dto.PageDto;
import com.finpy.newctf.pojo.dto.SubmitFlag;
import com.finpy.newctf.pojo.vo.*;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class PwnServiceImpl extends ServiceImpl<PwnMapper, Pwn> implements PwnService {
    final OkService okService;
    final ErrorService errorService;
    final UserService userService;
    final WriteupService writeupService;
    final FileService fileService;

    public PwnServiceImpl(OkService okService, ErrorService errorService, UserService userService, WriteupService writeupService, FileService fileService) {
        this.okService = okService;
        this.errorService = errorService;
        this.userService = userService;
        this.writeupService = writeupService;
        this.fileService = fileService;
    }

    @Override
    public ResUtils deletePwn(Integer id) {
        boolean b = this.removeById(id);
        if (!b) {
            return ResUtils.fail(ResCode.DATA_ERROR);
        }
        return ResUtils.success(ResCode.DELETE_SUCCESS);
    }

    @Override
    public ResUtils getAdminPwnInfo(Integer id) {
        Pwn pwn = this.getById(id);
        if (pwn == null) {
            return ResUtils.fail(ResCode.DATA_ERROR);
        }
        AdminWebInfo adminWebInfo = BeanUtil.copyBean(pwn, AdminWebInfo.class);

        QueryWrapper<Ok> okQueryWrapper = new QueryWrapper<>();
        long okNum = okService.count(okQueryWrapper.lambda().eq(Ok::getCId, id));
        adminWebInfo.setOkNum(okNum);

        QueryWrapper<Error> errorQuery = new QueryWrapper<>();
        long errorNum = errorService.count(errorQuery.lambda().eq(Error::getCId, id));
        adminWebInfo.setErrorNum(errorNum);

        adminWebInfo.setAuthor(userService.getById(pwn.getPwnAuthor()).getNickName());
        adminWebInfo.setWebKnowledge(JSONObject.parseArray(pwn.getPwnKnowledge(), String.class));
        return ResUtils.success(ResCode.GET_SUCCESS, adminWebInfo);
    }

    @Override
    public ResUtils getPwnInfo(Integer id) {
        QueryWrapper<Pwn> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().eq(Pwn::getId, id);
        Pwn one = this.getOne(queryWrapper);
        if (one == null) {
            return ResUtils.fail(ResCode.DATA_ERROR);
        }
        ReverseInfoVo reverseInfoVo = BeanUtil.copyBean(one, ReverseInfoVo.class);

        List<String> list = JSON.parseArray(one.getPwnKnowledge(), String.class);
        reverseInfoVo.setResKnowledge(list);

        User user = userService.getById(one.getPwnAuthor());
        reverseInfoVo.setResAuthor(user.getNickName());

        FileInfo file = fileService.getById(one.getPwnFile());
        reverseInfoVo.setFileName(file.getFileName());

        return ResUtils.success(ResCode.GET_SUCCESS, reverseInfoVo);
    }

    @Override
    public ResUtils GetSortAndWp(Integer id) {
        SortAndWpVo sortAndWpVo = new SortAndWpVo();
        // 获取排行榜信息
        QueryWrapper<Ok> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().orderByDesc(Ok::getCreateTime).eq(Ok::getCategory, "Pwn").last("limit " + Constant.CHALLENGE_SORT);
        List<Ok> list = okService.list(queryWrapper);
        if (list == null || list.size() == 0) {
            sortAndWpVo.setOk(null);
        } else {
            List<SortVo> collect = list.stream()
                    .map(ok -> {
                        SortVo sortVo = new SortVo();
                        sortVo.setTime(ok.getCreateTime());
                        sortVo.setNickName(userService.getById(ok.getUid()).getNickName());
                        return sortVo;
                    }).collect(Collectors.toList());
            sortAndWpVo.setOk(collect);
        }

        // 获取wp排行榜
        QueryWrapper<Writeup> queryWrapper1 = new QueryWrapper<>();
        queryWrapper1.lambda().orderByDesc(Writeup::getCreateTime).eq(Writeup::getCategoryId, 4).last("limit " + Constant.WRITEUP_SORT);
        List<Writeup> writeupList = writeupService.list(queryWrapper1);
        if (writeupList == null || writeupList.size() == 0) {
            sortAndWpVo.setWriteup(null);
        } else {
            List<WpSortVo> collect = writeupList.stream()
                    .map(writeup -> {
                        WpSortVo wpSortVo = new WpSortVo();
                        wpSortVo.setTitle(writeup.getTitle());
                        wpSortVo.setAuthor(userService.getById(writeup.getUserId()).getNickName());
                        wpSortVo.setTime(writeup.getCreateTime());
                        wpSortVo.setId(writeup.getId());
                        return wpSortVo;
                    }).collect(Collectors.toList());
            sortAndWpVo.setWriteup(collect);
        }
        return ResUtils.success(ResCode.GET_SUCCESS, sortAndWpVo);
    }

    @Override
    public ResUtils submitFlag(SubmitFlag flag, Integer userId) {
        // 判断用户是否重复提交
        QueryWrapper<Ok> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().eq(Ok::getUid, userId).eq(Ok::getCId, flag.getId()).eq(Ok::getCategory, "Pwn");
        long aLong = okService.count(queryWrapper);
        // 重复提交
        if (aLong > 0) {
            return ResUtils.fail(ResCode.SUBMIT_REPEAT);
        }
        Pwn pwn = this.getById(flag.getId());
        // 题目是否存在
        if (pwn == null) {
            return ResUtils.fail(ResCode.CHALLENGE_NOT_EXISTENT);
        }
        if (!pwn.getPwnFlag().equals(flag.getFlag())) {
            errorService.addError(flag.getId(), userId, "pwn");
            return ResUtils.fail(ResCode.FLAG_ERROR);
        }
        okService.addOk(flag.getId(), userId, "pwn");
        return ResUtils.success(ResCode.FLAG_CORRECT);
    }

    @Override
    public ResUtils addPwn(AddPwn pwn, Integer id) {
        // 判断题目名是否已存在
        QueryWrapper<Pwn> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().eq(Pwn::getPwnName, pwn.getPwnName());
        Pwn one = this.getOne(queryWrapper);
        if (one != null) {
            return ResUtils.fail(ResCode.TOPIC_POST_REPEAT);
        }

        Pwn pwn1 = BeanUtil.copyBean(pwn, Pwn.class);
        pwn1.setPwnAuthor(id);
        pwn1.setPwnKnowledge(JSON.toJSONString(pwn.getPwnKnowledge()));

        boolean save = this.save(pwn1);
        if (!save) {
            return ResUtils.fail(ResCode.DATA_ERROR);
        }
        return ResUtils.success(ResCode.POST_SUCCESS);
    }

    @Override
    public ResUtils getPwnList(PageDto pageDto) {
        List<Pwn> list = this.list();
        // 如果数据为空
        if (list == null) {
            return ResUtils.success(ResCode.GET_SUCCESS, null);
        }
        List<PwnListVo> pwnListVos = BeanUtil.copyBeanList(list, PwnListVo.class);
        return ResUtils.success(ResCode.GET_SUCCESS, pwnListVos);
    }

    @Override
    public ResUtils getAdminPwnList(PageDto page) {

        Page<Pwn> page1 = new Page<>(page.getPage(), page.getPageSize());
        Page<Pwn> page2 = this.page(page1);
        List<Pwn> pwns = page2.getRecords();
        if (pwns == null) {
            return ResUtils.fail(ResCode.DATA_ERROR);
        }
        List<AdminPwnList> collect = pwns.stream()
                .map(pwn -> {
                    AdminPwnList adminPwnList = BeanUtil.copyBean(pwn, AdminPwnList.class);
                    adminPwnList.setAuthor(userService.getById(pwn.getPwnAuthor()).getNickName());

                    QueryWrapper<Ok> okQuery = new QueryWrapper<>();
                    okQuery.lambda().eq(Ok::getCId, pwn.getId());
                    adminPwnList.setPwnOk(okService.count(okQuery));

                    adminPwnList.setPwnKnowledge(JSONObject.parseArray(pwn.getPwnKnowledge(), String.class));
                    return adminPwnList;
                }).collect(Collectors.toList());
        return ResUtils.success(ResCode.GET_SUCCESS, collect);
    }
}

