package com.finpy.newctf.Service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.finpy.newctf.Config.Constant;
import com.finpy.newctf.Mapper.CryptoMapper;
import com.finpy.newctf.Service.*;
import com.finpy.newctf.Utils.BeanUtil;
import com.finpy.newctf.Utils.ResCode;
import com.finpy.newctf.Utils.ResUtils;
import com.finpy.newctf.pojo.Do.Crypto;
import com.finpy.newctf.pojo.Do.Error;
import com.finpy.newctf.pojo.Do.Ok;
import com.finpy.newctf.pojo.Do.Writeup;
import com.finpy.newctf.pojo.dto.AddCrypto;
import com.finpy.newctf.pojo.dto.PageDto;
import com.finpy.newctf.pojo.dto.SubmitFlag;
import com.finpy.newctf.pojo.vo.*;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class CryptoServiceImpl extends ServiceImpl<CryptoMapper, Crypto> implements CryptoService {

    final UserService userService;
    final FileService fileService;
    final OkService okService;
    final WriteupService writeupService;
    final ErrorService errorService;

    public CryptoServiceImpl(UserService userService, FileService fileService, OkService okService, WriteupService writeupService, ErrorService errorService) {
        this.userService = userService;
        this.fileService = fileService;
        this.okService = okService;
        this.writeupService = writeupService;
        this.errorService = errorService;
    }

    @Override
    public ResUtils addCrypto(AddCrypto crypto, Integer id) {
        // 判断题目是否重复
        QueryWrapper<Crypto> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().eq(Crypto::getCryName, crypto.getCryName());
        long count = this.count(queryWrapper);
        if (count > 0) {
            return ResUtils.fail(ResCode.TOPIC_POST_REPEAT);
        }
        // 整理数据
        Crypto crypto1 = BeanUtil.copyBean(crypto, Crypto.class);
        crypto1.setCryAuthor(id);
        crypto1.setCryKnowledge(JSON.toJSONString(crypto.getCryKnowledge()));
        // 添加题目
        boolean save = this.save(crypto1);
        if (!save) {
            return ResUtils.fail(ResCode.DATA_ERROR);
        }
        return ResUtils.success(ResCode.POST_SUCCESS);
    }

    @Override
    public ResUtils getCryptoList(PageDto pageDto) {
        List<Crypto> list = this.list();
        // 如果数据为空
        if (list == null) {
            return ResUtils.success(ResCode.GET_SUCCESS, null);
        }
        List<CryptoListVo> cryptoListVos = BeanUtil.copyBeanList(list, CryptoListVo.class);
        return ResUtils.success(ResCode.GET_SUCCESS, cryptoListVos);
    }

    @Override
    public ResUtils getCryptoInfo(Integer id) {
        Crypto crypto = this.getById(id);
        if (Objects.isNull(crypto)) {
            return ResUtils.fail(ResCode.DATA_ERROR);
        }
        CryptoInfoVo cryptoInfoVo = BeanUtil.copyBean(crypto, CryptoInfoVo.class);

        cryptoInfoVo.setCryAuthor(userService.getById(crypto.getCryAuthor()).getNickName());
        cryptoInfoVo.setCryKnowledge(JSON.parseArray(crypto.getCryKnowledge(), String.class));
        cryptoInfoVo.setFileName(fileService.getById(crypto.getCryFile()).getFileName());

        return ResUtils.success(ResCode.GET_SUCCESS, cryptoInfoVo);
    }

    @Override
    public ResUtils submitFlag(SubmitFlag flag, Integer userId) {
        // 判断用户是否已经完成该题目
        QueryWrapper<Ok> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().eq(Ok::getUid, userId).eq(Ok::getCId, flag.getId());
        Ok one = okService.getOne(queryWrapper);
        if (Objects.isNull(one)) {
            return ResUtils.fail(ResCode.SUBMIT_REPEAT);
        }
        Crypto crypto = this.getById(flag.getId());
        if (Objects.isNull(crypto)) {
            return ResUtils.fail(ResCode.CHALLENGE_NOT_EXISTENT);
        }
        if (flag.getFlag().equals(crypto.getCryFlag())) {
            Ok ok = new Ok();
            ok.setCategory("Crypto");
            ok.setUid(userId);
            ok.setCId(flag.getId());
            boolean save = okService.save(ok);
            if (!save) {
                return ResUtils.fail(ResCode.DATA_ERROR);
            }
            return ResUtils.success(ResCode.FLAG_CORRECT);
        } else {
            Error error = new Error();
            error.setCategory("Crypto");
            error.setUid(userId);
            error.setCId(flag.getId());
            boolean save = errorService.save(error);
            if (!save) {
                return ResUtils.fail(ResCode.DATA_ERROR);
            }
            return ResUtils.fail(ResCode.FLAG_ERROR);
        }
    }

    @Override
    public ResUtils GetSortAndWp(Integer id) {

        SortAndWpVo sortAndWpVo = new SortAndWpVo();
        // 获取排行榜信息
        QueryWrapper<Ok> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().orderByDesc(Ok::getCreateTime).eq(Ok::getCategory, "Crypto").last("limit " + Constant.CHALLENGE_SORT);
        List<Ok> list = okService.list(queryWrapper);
        if (list == null || list.size() == 0) {
            sortAndWpVo.setOk(null);
        } else {
            List<SortVo> collect = list.stream()
                    .map(ok -> {
                        SortVo sortVo = new SortVo();
                        sortVo.setTime(ok.getCreateTime());
                        sortVo.setNickName(userService.getById(ok.getUid()).getNickName());
                        return sortVo;
                    }).collect(Collectors.toList());
            sortAndWpVo.setOk(collect);
        }

        // 获取wp排行榜
        QueryWrapper<Writeup> queryWrapper1 = new QueryWrapper<>();
        queryWrapper1.lambda().orderByDesc(Writeup::getCreateTime).eq(Writeup::getCategoryId, 5).last("limit " + Constant.WRITEUP_SORT);
        List<Writeup> writeupList = writeupService.list(queryWrapper1);
        if (writeupList == null || writeupList.size() == 0) {
            sortAndWpVo.setWriteup(null);
        } else {
            List<WpSortVo> collect = writeupList.stream()
                    .map(writeup -> {
                        WpSortVo wpSortVo = new WpSortVo();
                        wpSortVo.setTitle(writeup.getTitle());
                        wpSortVo.setAuthor(userService.getById(writeup.getUserId()).getNickName());
                        wpSortVo.setTime(writeup.getCreateTime());
                        wpSortVo.setId(writeup.getId());
                        return wpSortVo;
                    }).collect(Collectors.toList());
            sortAndWpVo.setWriteup(collect);
        }
        return ResUtils.success(ResCode.GET_SUCCESS, sortAndWpVo);
    }

    @Override
    public ResUtils getAdminCryptoList(PageDto page) {
        Page<Crypto> page1 = new Page<>(page.getPage(), page.getPageSize());
        Page<Crypto> page2 = this.page(page1);
        List<Crypto> cryptoList = page2.getRecords();
        if (cryptoList == null) {
            return ResUtils.fail(ResCode.DATA_ERROR);
        }
        List<AdminCryptoList> collect = cryptoList.stream()
                .map(crypto -> {
                    AdminCryptoList adminCryptoList = BeanUtil.copyBean(crypto, AdminCryptoList.class);
                    adminCryptoList.setAuthor(userService.getById(crypto.getCryAuthor()).getNickName());

                    QueryWrapper<Ok> okQuery = new QueryWrapper<>();
                    okQuery.lambda().eq(Ok::getCId, crypto.getId());
                    adminCryptoList.setCryOk(okService.count(okQuery));

                    adminCryptoList.setCryKnowledge(JSONObject.parseArray(crypto.getCryKnowledge(), String.class));
                    return adminCryptoList;
                }).collect(Collectors.toList());
        return ResUtils.success(ResCode.GET_SUCCESS, collect);
    }

    @Override
    public ResUtils deleteCrypto(Integer id) {
        boolean b = this.removeById(id);
        if (!b) {
            return ResUtils.fail(ResCode.DATA_ERROR);
        }
        return ResUtils.success(ResCode.DELETE_SUCCESS);
    }

    @Override
    public ResUtils getAdminCryptoInfo(Integer id) {

        Crypto crypto = this.getById(id);
        if (crypto == null) {
            return ResUtils.fail(ResCode.DATA_ERROR);
        }
        AdminWebInfo adminWebInfo = BeanUtil.copyBean(crypto, AdminWebInfo.class);

        QueryWrapper<Ok> okQueryWrapper = new QueryWrapper<>();
        long okNum = okService.count(okQueryWrapper.lambda().eq(Ok::getCId, id));
        adminWebInfo.setOkNum(okNum);

        QueryWrapper<Error> errorQuery = new QueryWrapper<>();
        long errorNum = errorService.count(errorQuery.lambda().eq(Error::getCId, id));
        adminWebInfo.setErrorNum(errorNum);

        adminWebInfo.setAuthor(userService.getById(crypto.getCryAuthor()).getNickName());
        adminWebInfo.setWebKnowledge(JSONObject.parseArray(crypto.getCryKnowledge(), String.class));
        return ResUtils.success(ResCode.GET_SUCCESS, adminWebInfo);
    }
}

