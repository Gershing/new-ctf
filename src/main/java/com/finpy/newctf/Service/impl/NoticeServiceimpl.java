package com.finpy.newctf.Service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.finpy.newctf.Mapper.NoticeMapper;
import com.finpy.newctf.Service.NoticeService;
import com.finpy.newctf.Utils.BeanUtil;
import com.finpy.newctf.Utils.ResCode;
import com.finpy.newctf.Utils.ResUtils;
import com.finpy.newctf.Utils.StringUtil;
import com.finpy.newctf.pojo.Do.Notice;
import com.finpy.newctf.pojo.vo.NoticeVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class NoticeServiceimpl extends ServiceImpl<NoticeMapper, Notice> implements NoticeService {

    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/16 18:06
     * @Info 获取公告列表
     */
    public ResUtils GetNoticeList() {
        // 获取公告列表
        List<Notice> list = list();
        // 校验结果
        if (StringUtil.listIsNull(list)) {
            return ResUtils.fail(ResCode.GET_ERROR);
        }
        // 整理数据返回前端
        List<NoticeVo> noticeVos = BeanUtil.copyBeanList(list, NoticeVo.class);
        return ResUtils.success(ResCode.GET_SUCCESS, noticeVos);
    }

    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/16 18:06
     * @Info 发布公告
     */
    @Override
    public ResUtils SetNotice(Notice notice) {
        boolean save = save(notice);
        if (!save) {
            return ResUtils.fail(ResCode.PROGRAM_INSIDE_EXCEPTION);
        }
        return ResUtils.success(ResCode.POST_SUCCESS);
    }
}
