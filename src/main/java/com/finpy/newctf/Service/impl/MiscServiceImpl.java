package com.finpy.newctf.Service.impl;


import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.finpy.newctf.Config.Constant;
import com.finpy.newctf.Mapper.MiscMapper;
import com.finpy.newctf.Service.*;
import com.finpy.newctf.Utils.BeanUtil;
import com.finpy.newctf.Utils.ResCode;
import com.finpy.newctf.Utils.ResUtils;
import com.finpy.newctf.pojo.Do.Error;
import com.finpy.newctf.pojo.Do.*;
import com.finpy.newctf.pojo.dto.AddMisc;
import com.finpy.newctf.pojo.dto.PageDto;
import com.finpy.newctf.pojo.dto.SubmitFlag;
import com.finpy.newctf.pojo.vo.*;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;


/**
 * &#064;Author 生姜の鱼丸
 * &#064;Time 2023/1/24 12:58
 * &#064;Info
 */
@Service
public class MiscServiceImpl extends ServiceImpl<MiscMapper, Misc> implements MiscService {
    final ErrorService errorService;
    final OkService okService;
    final UserService userService;
    final WriteupService writeupService;
    final FileService fileService;

    public MiscServiceImpl(ErrorService errorService, OkService okService, UserService userService, WriteupService writeupService, FileService fileService) {
        this.errorService = errorService;
        this.okService = okService;
        this.userService = userService;
        this.writeupService = writeupService;
        this.fileService = fileService;
    }

    @Override
    public ResUtils getMiscInfo(Integer id) {
        QueryWrapper<Misc> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().eq(Misc::getId, id);
        Misc misc = this.getOne(queryWrapper);
        if (misc == null) {
            return ResUtils.fail(ResCode.DATA_ERROR);
        }
        MiscInfoVo miscInfoVo = BeanUtil.copyBean(misc, MiscInfoVo.class);
        FileInfo fileInfo = fileService.getById(misc.getMiscFile());
        miscInfoVo.setFileName(fileInfo.getFileName());
        miscInfoVo.setMiscKnowledge(JSONObject.parseArray(misc.getMiscKnowledge(), String.class));
        return ResUtils.success(ResCode.GET_SUCCESS, miscInfoVo);
    }

    @Override
    public ResUtils getMiscList(PageDto page) {
        Page<Misc> pages = new Page<>(page.getPage(), page.getPageSize());
        List<Misc> records = this.page(pages).getRecords();
        if (records == null) {
            return ResUtils.fail(ResCode.DATA_ERROR);
        }
        List<MiscVo> miscVos = BeanUtil.copyBeanList(records, MiscVo.class);
        return ResUtils.success(ResCode.GET_SUCCESS, miscVos);
    }

    @Override
    public ResUtils addMisc(AddMisc misc, Integer id) {
        // 判断是否存在相同题目
        QueryWrapper<Misc> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().eq(Misc::getMiscName, misc.getMiscName());
        long count = this.count(queryWrapper);
        if (count > 0) {
            return ResUtils.fail(ResCode.TOPIC_POST_REPEAT);
        }

        Misc misc1 = BeanUtil.copyBean(misc, Misc.class);
        misc1.setMiscKnowledge(JSONArray.toJSONString(misc.getMiscKnowledge()));
        misc1.setMiscAuthor(id);
        boolean save = this.save(misc1);
        if (!save) {
            return ResUtils.fail(ResCode.DATA_ERROR);
        }
        return ResUtils.success(ResCode.POST_SUCCESS);
    }

    @Override
    public ResUtils submitFlag(SubmitFlag flag, Integer userId) {
        // 判断用户是否重复提交
        QueryWrapper<Ok> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().eq(Ok::getUid, userId).eq(Ok::getCId, flag.getId()).eq(Ok::getCategory, "Misc");
        long aLong = okService.count(queryWrapper);
        // 重复提交
        if (aLong > 0) {
            return ResUtils.fail(ResCode.SUBMIT_REPEAT);
        }
        Misc misc = this.getById(flag.getId());
        // 题目是否存在
        if (misc == null) {
            return ResUtils.fail(ResCode.CHALLENGE_NOT_EXISTENT);
        }
        if (!misc.getMiscFlag().equals(flag.getFlag())) {
            errorService.addError(flag.getId(), userId, "Misc");
            return ResUtils.fail(ResCode.FLAG_ERROR);
        }
        okService.addOk(flag.getId(), userId, "Misc");
        return ResUtils.success(ResCode.FLAG_CORRECT);
    }

    @Override
    public ResUtils GetSortAndWp(Integer id) {
        SortAndWpVo sortAndWpVo = new SortAndWpVo();
        // 获取排行榜信息
        QueryWrapper<Ok> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().orderByDesc(Ok::getCreateTime).eq(Ok::getCategory, "Misc").last("limit " + Constant.CHALLENGE_SORT);
        List<Ok> list = okService.list(queryWrapper);
        if (list == null || list.size() == 0) {
            sortAndWpVo.setOk(null);
        } else {
            List<SortVo> collect = list.stream()
                    .map(ok -> {
                        SortVo sortVo = new SortVo();
                        sortVo.setTime(ok.getCreateTime());
                        sortVo.setNickName(userService.getById(ok.getUid()).getNickName());
                        return sortVo;
                    }).collect(Collectors.toList());
            sortAndWpVo.setOk(collect);
        }

        // 获取wp排行榜
        QueryWrapper<Writeup> queryWrapper1 = new QueryWrapper<>();
        queryWrapper1.lambda().orderByDesc(Writeup::getCreateTime).eq(Writeup::getCategoryId, 2).last("limit " + Constant.WRITEUP_SORT);
        List<Writeup> list1 = writeupService.list(queryWrapper1);
        if (list1 == null || list1.size() == 0) {
            sortAndWpVo.setWriteup(null);
        } else {
            List<WpSortVo> collect = list1.stream()
                    .map(writeup -> {
                        WpSortVo wpSortVo = new WpSortVo();
                        wpSortVo.setTitle(writeup.getTitle());
                        wpSortVo.setAuthor(userService.getById(writeup.getUserId()).getNickName());
                        wpSortVo.setTime(writeup.getCreateTime());
                        wpSortVo.setId(writeup.getId());
                        return wpSortVo;
                    }).collect(Collectors.toList());
            sortAndWpVo.setWriteup(collect);
        }

        return ResUtils.success(ResCode.GET_SUCCESS, sortAndWpVo);
    }

    @Override
    public ResUtils getAdminMiscInfo(Integer id) {
        Misc misc = this.getById(id);
        if (misc == null) {
            return ResUtils.fail(ResCode.DATA_ERROR);
        }
        AdminMiscInfo adminMiscInfo = BeanUtil.copyBean(misc, AdminMiscInfo.class);

        QueryWrapper<Ok> okQuery = new QueryWrapper<>();
        okQuery.lambda().eq(Ok::getCId, id);
        adminMiscInfo.setOkNum(okService.count(okQuery));

        QueryWrapper<Error> errorQuery = new QueryWrapper<>();
        errorQuery.lambda().eq(Error::getCId, id);
        adminMiscInfo.setErrorNum(errorService.count(errorQuery));

        adminMiscInfo.setAuthor(userService.getById(misc.getMiscAuthor()).getNickName());
        adminMiscInfo.setMiscFile(fileService.getById(misc.getMiscFile()).getFileName());
        adminMiscInfo.setMiscKnowledge(JSONObject.parseArray(misc.getMiscKnowledge(), String.class));
        return ResUtils.success(ResCode.GET_SUCCESS, adminMiscInfo);
    }

    @Override
    public ResUtils getAdminMiscList(PageDto page) {
        Page<Misc> page1 = new Page<>(page.getPage(), page.getPageSize());
        Page<Misc> page2 = this.page(page1);
        List<Misc> miscList = page2.getRecords();
        if (miscList == null) {
            return ResUtils.fail(ResCode.DATA_ERROR);
        }
        List<AdminMiscList> collect = miscList.stream()
                .map(misc -> {
                    AdminMiscList adminMiscList = BeanUtil.copyBean(misc, AdminMiscList.class);
                    adminMiscList.setAuthor(userService.getById(misc.getMiscAuthor()).getNickName());

                    QueryWrapper<Ok> okQuery = new QueryWrapper<>();
                    okQuery.lambda().eq(Ok::getCId, misc.getId());
                    adminMiscList.setMiscOk(okService.count(okQuery));

                    adminMiscList.setMiscKnowledge(JSONObject.parseArray(misc.getMiscKnowledge(), String.class));
                    return adminMiscList;
                }).collect(Collectors.toList());
        return ResUtils.success(ResCode.GET_SUCCESS, collect);
    }

    @Override
    public ResUtils deleteMisc(Integer id) {
        boolean b = this.removeById(id);
        if (!b) {
            return ResUtils.fail(ResCode.DATA_ERROR);
        }
        return ResUtils.success(ResCode.DELETE_SUCCESS);
    }
}
