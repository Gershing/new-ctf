package com.finpy.newctf.Service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.finpy.newctf.Mapper.WebContainerMapper;
import com.finpy.newctf.Service.WebContainerService;
import com.finpy.newctf.pojo.Do.WebContainer;
import org.springframework.stereotype.Service;

/**
 * docker容器(com.finpy.newctf.pojo.Do.WebContainer)表服务实现类
 *
 * @author 生姜の鱼丸
 * @since 2023-01-25 12:35:19
 */
@Service
public class WebContainerServiceImpl extends ServiceImpl<WebContainerMapper, WebContainer> implements WebContainerService {

}

