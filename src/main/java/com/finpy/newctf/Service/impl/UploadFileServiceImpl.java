package com.finpy.newctf.Service.impl;

import com.finpy.newctf.Config.QiNiuYunConfig;
import com.finpy.newctf.Service.UploadFileService;
import com.finpy.newctf.Utils.ResUtils;
import com.qiniu.common.QiniuException;
import com.qiniu.common.Zone;
import com.qiniu.storage.BucketManager;
import com.qiniu.storage.Configuration;
import com.qiniu.storage.UploadManager;
import com.qiniu.util.Auth;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

@Service
public class UploadFileServiceImpl implements UploadFileService {

    private QiNiuYunConfig qiNiuYunConfig;

    /**
     * 七牛文件上传管理器
     */
    private UploadManager uploadManager;
    /**
     * 上传的token
     */
    private String token;
    /**
     * 七牛认证管理
     */
    private Auth auth;

    private BucketManager bucketManager;

    public UploadFileServiceImpl(QiNiuYunConfig qiNiuYunConfig) {
        this.qiNiuYunConfig = qiNiuYunConfig;
        init();
    }

    private void init() {
//        华东：zone0    华北：zone1   华南：zone2   北美：na0   东南亚：as0
        // 我是华南地区的所以是zone2，如果是其他地区的需要修改
        uploadManager = new UploadManager(new Configuration(Zone.zone2()));
        auth = Auth.create(qiNiuYunConfig.getAccessKey(), qiNiuYunConfig.getSecretKey());
        // 根据命名空间生成的上传token
        bucketManager = new BucketManager(auth, new Configuration(Zone.zone2()));
        token = auth.uploadToken(qiNiuYunConfig.getBucketName());
    }


    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/14 16:53
     * @Info 上传文件
     */
    public ResUtils uploadFile(MultipartFile file) {
//        String s = FileUtil.uploadFile(file);
//        if (s == null) {
//            return ResUtils.fail(ResCode.UPLOAD_ERROR);
//        }
//        return ResUtils.success(ResCode.UPLOAD_SUCCESS);
//        try {
//            // 判断图片后缀，并使用工具类根据上传文件生成唯一图片名称,防止截断字符如“%00”
//            String fileName = file.getOriginalFilename();
//            assert fileName != null;
//            String imgName = StringUtil.getRandomImgName(fileName);
//
//            // 上传图片文件
//            Response res = uploadManager.put(file.getInputStream(), imgName, token, null, null);
//            if (!res.isOK()) {
//                throw new RuntimeException("上传七牛出错：" + res);
//            }
//            // 解析上传成功的结果
//            DefaultPutRet putRet = new Gson().fromJson(res.bodyString(), DefaultPutRet.class);
//
//            // 直接返回外链地址
//            return ResUtils.success(ResCode.UPLOAD_SUCCESS, getPrivateFile(imgName));
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return ResUtils.fail(ResCode.UPLOAD_ERROR);
        return null;
    }

    /**
     * 获取私有空间文件
     *
     * @param fileKey
     * @return
     */

    public String getPrivateFile(String fileKey) {
        String encodedFileName;
        String finalUrl = null;
        try {
            encodedFileName = URLEncoder.encode(fileKey, "utf-8").replace("+", "%20");
            String publicUrl = String.format("%s/%s", this.qiNiuYunConfig.getUrl(), encodedFileName);
            //1小时，可以自定义链接过期时间
            long expireInSeconds = 3600;
            finalUrl = auth.privateDownloadUrl(publicUrl, expireInSeconds);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        assert finalUrl != null;
        return finalUrl.split("\\?e=")[0];
    }


    /**
     * 根据空间名、文件名删除文件
     *
     * @param bucketName
     * @param fileKey
     * @return
     */

    public boolean removeFile(String bucketName, String fileKey) {
        try {
            bucketManager.delete(bucketName, fileKey);
        } catch (QiniuException e) {
            e.printStackTrace();
        }
        return true;
    }
}
