package com.finpy.newctf.Service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.finpy.newctf.Mapper.ErrorMapper;
import com.finpy.newctf.Service.ErrorService;
import com.finpy.newctf.pojo.Do.Error;
import org.springframework.stereotype.Service;

/**
 * &#064;Author 生姜の鱼丸
 * &#064;Time 2023/1/19 16:12
 * &#064;Info
 */
@Service
public class ErrorServiceImpl extends ServiceImpl<ErrorMapper, Error> implements ErrorService {
    @Override
    public void addError(Integer id, Integer userId, String categoryName) {
        Error error = new Error();
        error.setUid(userId);
        error.setCId(id);
        error.setCategory(categoryName);
        this.save(error);
    }
}
