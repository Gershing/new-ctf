package com.finpy.newctf.Service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.finpy.newctf.Mapper.SettingsMapper;
import com.finpy.newctf.Service.SettingsService;
import com.finpy.newctf.Utils.BeanUtil;
import com.finpy.newctf.Utils.ResCode;
import com.finpy.newctf.Utils.ResUtils;
import com.finpy.newctf.pojo.Do.Settings;
import com.finpy.newctf.pojo.vo.SettingsVo;
import org.springframework.stereotype.Service;

/**
 * &#064;Author 生姜の鱼丸
 * &#064;Time 2023/1/16 16:13
 * &#064;Info
 */
@Service
public class SettingsServiceImpl extends ServiceImpl<SettingsMapper, Settings> implements SettingsService {

    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/16 16:24
     * @Info 获取全局配置
     */
    @Override
    public ResUtils GetSettings() {
        // 设置条件
        LambdaQueryWrapper<Settings> queryWrapper = new LambdaQueryWrapper<>();
        // id = 1
        queryWrapper.eq(Settings::getId, 1);
        // 查询一条数据
        Settings one = getOne(queryWrapper);
        // 判断数据库中是否含有数据 或 是否获取成功
        if (one == null) {
            return ResUtils.fail(ResCode.GET_ERROR);
        }
        SettingsVo settingsVo = BeanUtil.copyBean(one, SettingsVo.class);
        return ResUtils.success(ResCode.GET_SUCCESS, settingsVo);
    }

    @Override
    public ResUtils UpdateSettings(Settings settings) {
        settings.setId(1);
        if (!updateById(settings)) {
            return ResUtils.fail(ResCode.MODIFY_ERROR);
        }
        return ResUtils.success(ResCode.MODIFY_SUCCESS);
    }
}
