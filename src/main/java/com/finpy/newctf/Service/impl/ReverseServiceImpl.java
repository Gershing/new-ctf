package com.finpy.newctf.Service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.finpy.newctf.Config.Constant;
import com.finpy.newctf.Mapper.ReverseMapper;
import com.finpy.newctf.Service.*;
import com.finpy.newctf.Utils.BeanUtil;
import com.finpy.newctf.Utils.ResCode;
import com.finpy.newctf.Utils.ResUtils;
import com.finpy.newctf.pojo.Do.Error;
import com.finpy.newctf.pojo.Do.*;
import com.finpy.newctf.pojo.dto.AddReverse;
import com.finpy.newctf.pojo.dto.PageDto;
import com.finpy.newctf.pojo.dto.SubmitFlag;
import com.finpy.newctf.pojo.vo.*;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ReverseServiceImpl extends ServiceImpl<ReverseMapper, Reverse> implements ReverseService {
    final UserService userService;
    final FileService fileService;
    final OkService okService;
    final ErrorService errorService;
    final WriteupService writeupService;

    public ReverseServiceImpl(UserService userService, FileService fileService, OkService okService, ErrorService errorService, WriteupService writeupService) {
        this.userService = userService;
        this.fileService = fileService;
        this.okService = okService;
        this.errorService = errorService;
        this.writeupService = writeupService;
    }

    @Override
    public ResUtils getReverseList(PageDto pageDto) {
        List<Reverse> list = this.list();
        // 如果数据为空
        if (list == null) {
            return ResUtils.success(ResCode.GET_SUCCESS, null);
        }
        List<ReverseListVo> reverseListVos = BeanUtil.copyBeanList(list, ReverseListVo.class);
        return ResUtils.success(ResCode.GET_SUCCESS, reverseListVos);
    }

    @Override
    public ResUtils getReverseInfo(Integer id) {
        QueryWrapper<Reverse> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().eq(Reverse::getId, id);
        Reverse one = this.getOne(queryWrapper);
        if (one == null) {
            return ResUtils.fail(ResCode.DATA_ERROR);
        }
        ReverseInfoVo reverseInfoVo = BeanUtil.copyBean(one, ReverseInfoVo.class);

        List<String> list = JSON.parseArray(one.getResKnowledge(), String.class);
        reverseInfoVo.setResKnowledge(list);

        User user = userService.getById(one.getResAuthor());
        reverseInfoVo.setResAuthor(user.getNickName());

        FileInfo file = fileService.getById(one.getResFile());
        reverseInfoVo.setFileName(file.getFileName());

        return ResUtils.success(ResCode.GET_SUCCESS, reverseInfoVo);
    }

    @Override
    public ResUtils addReverse(AddReverse reverse, Integer id) {
        // 判断题目名是否已存在
        QueryWrapper<Reverse> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().eq(Reverse::getResName, reverse.getResName());
        Reverse one = this.getOne(queryWrapper);
        if (one != null) {
            return ResUtils.fail(ResCode.TOPIC_POST_REPEAT);
        }

        Reverse reverse1 = BeanUtil.copyBean(reverse, Reverse.class);
        reverse1.setResAuthor(id);
        reverse1.setResKnowledge(JSON.toJSONString(reverse.getResKnowledge()));

        boolean save = this.save(reverse1);
        if (!save) {
            return ResUtils.fail(ResCode.DATA_ERROR);
        }
        return ResUtils.success(ResCode.POST_SUCCESS);
    }

    @Override
    public ResUtils submitFlag(SubmitFlag flag, Integer userId) {
        // 判断用户是否重复提交
        QueryWrapper<Ok> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().eq(Ok::getUid, userId).eq(Ok::getCId, flag.getId()).eq(Ok::getCategory, "Reverse");
        long aLong = okService.count(queryWrapper);
        // 重复提交
        if (aLong > 0) {
            return ResUtils.fail(ResCode.SUBMIT_REPEAT);
        }
        Reverse reverse = this.getById(flag.getId());
        // 题目是否存在
        if (reverse == null) {
            return ResUtils.fail(ResCode.CHALLENGE_NOT_EXISTENT);
        }
        if (!reverse.getResFlag().equals(flag.getFlag())) {
            errorService.addError(flag.getId(), userId, "Reverse");
            return ResUtils.fail(ResCode.FLAG_ERROR);
        }
        okService.addOk(flag.getId(), userId, "Reverse");
        return ResUtils.success(ResCode.FLAG_CORRECT);
    }

    @Override
    public ResUtils GetSortAndWp(Integer id) {
        SortAndWpVo sortAndWpVo = new SortAndWpVo();
        // 获取排行榜信息
        QueryWrapper<Ok> queryWrapper = new QueryWrapper<>();
        queryWrapper.lambda().orderByDesc(Ok::getCreateTime).eq(Ok::getCategory, "Reverse").last("limit " + Constant.CHALLENGE_SORT);
        List<Ok> list = okService.list(queryWrapper);
        if (list == null || list.size() == 0) {
            sortAndWpVo.setOk(null);
        } else {
            List<SortVo> collect = list.stream()
                    .map(ok -> {
                        SortVo sortVo = new SortVo();
                        sortVo.setTime(ok.getCreateTime());
                        sortVo.setNickName(userService.getById(ok.getUid()).getNickName());
                        return sortVo;
                    }).collect(Collectors.toList());
            sortAndWpVo.setOk(collect);
        }

        // 获取wp排行榜
        QueryWrapper<Writeup> queryWrapper1 = new QueryWrapper<>();
        queryWrapper1.lambda().orderByDesc(Writeup::getCreateTime).eq(Writeup::getCategoryId, 4).last("limit " + Constant.WRITEUP_SORT);
        List<Writeup> writeupList = writeupService.list(queryWrapper1);
        if (writeupList == null || writeupList.size() == 0) {
            sortAndWpVo.setWriteup(null);
        } else {
            List<WpSortVo> collect = writeupList.stream()
                    .map(writeup -> {
                        WpSortVo wpSortVo = new WpSortVo();
                        wpSortVo.setTitle(writeup.getTitle());
                        wpSortVo.setAuthor(userService.getById(writeup.getUserId()).getNickName());
                        wpSortVo.setTime(writeup.getCreateTime());
                        wpSortVo.setId(writeup.getId());
                        return wpSortVo;
                    }).collect(Collectors.toList());
            sortAndWpVo.setWriteup(collect);
        }
        return ResUtils.success(ResCode.GET_SUCCESS, sortAndWpVo);
    }

    @Override
    public ResUtils getAdminReverseList(PageDto page) {
        Page<Reverse> page1 = new Page<>(page.getPage(), page.getPageSize());
        Page<Reverse> page2 = this.page(page1);
        List<Reverse> reverseList = page2.getRecords();
        if (reverseList == null) {
            return ResUtils.fail(ResCode.DATA_ERROR);
        }
        List<AdminReverseList> collect = reverseList.stream()
                .map(reverse -> {
                    AdminReverseList adminReverseList = BeanUtil.copyBean(reverse, AdminReverseList.class);
                    adminReverseList.setAuthor(userService.getById(reverse.getResAuthor()).getNickName());

                    QueryWrapper<Ok> okQuery = new QueryWrapper<>();
                    okQuery.lambda().eq(Ok::getCId, reverse.getId());
                    adminReverseList.setResOk(okService.count(okQuery));

                    adminReverseList.setResKnowledge(JSONObject.parseArray(reverse.getResKnowledge(), String.class));
                    return adminReverseList;
                }).collect(Collectors.toList());
        return ResUtils.success(ResCode.GET_SUCCESS, collect);
    }

    @Override
    public ResUtils deleteReverse(Integer id) {
        boolean b = this.removeById(id);
        if (!b) {
            return ResUtils.fail(ResCode.DATA_ERROR);
        }
        return ResUtils.success(ResCode.DELETE_SUCCESS);
    }

    @Override
    public ResUtils getAdminReverseInfo(Integer id) {
        Reverse reverse = this.getById(id);
        if (reverse == null) {
            return ResUtils.fail(ResCode.DATA_ERROR);
        }
        AdminWebInfo adminWebInfo = BeanUtil.copyBean(reverse, AdminWebInfo.class);

        QueryWrapper<Ok> okQueryWrapper = new QueryWrapper<>();
        long okNum = okService.count(okQueryWrapper.lambda().eq(Ok::getCId, id));
        adminWebInfo.setOkNum(okNum);

        QueryWrapper<Error> errorQuery = new QueryWrapper<>();
        long errorNum = errorService.count(errorQuery.lambda().eq(Error::getCId, id));
        adminWebInfo.setErrorNum(errorNum);

        adminWebInfo.setAuthor(userService.getById(reverse.getResAuthor()).getNickName());
        adminWebInfo.setWebKnowledge(JSONObject.parseArray(reverse.getResKnowledge(), String.class));
        return ResUtils.success(ResCode.GET_SUCCESS, adminWebInfo);
    }
}

