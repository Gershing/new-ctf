package com.finpy.newctf.Service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.finpy.newctf.pojo.Do.WebContainer;

/**
 * docker容器(com.finpy.newctf.pojo.Do.WebContainer)表服务接口
 *
 * @author 生姜の鱼丸
 * @since 2023-01-25 12:35:12
 */
public interface WebContainerService extends IService<WebContainer> {

}

