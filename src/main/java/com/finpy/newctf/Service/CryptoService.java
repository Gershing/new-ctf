package com.finpy.newctf.Service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.finpy.newctf.Utils.ResUtils;
import com.finpy.newctf.pojo.Do.Crypto;
import com.finpy.newctf.pojo.dto.AddCrypto;
import com.finpy.newctf.pojo.dto.PageDto;
import com.finpy.newctf.pojo.dto.SubmitFlag;

public interface CryptoService extends IService<Crypto> {

    ResUtils addCrypto(AddCrypto crypto, Integer id);

    ResUtils getCryptoList(PageDto pageDto);

    ResUtils getCryptoInfo(Integer id);

    ResUtils submitFlag(SubmitFlag flag, Integer userId);

    ResUtils GetSortAndWp(Integer id);

    ResUtils getAdminCryptoList(PageDto page);

    ResUtils deleteCrypto(Integer id);

    ResUtils getAdminCryptoInfo(Integer id);
}

