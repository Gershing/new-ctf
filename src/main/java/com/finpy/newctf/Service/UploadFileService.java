package com.finpy.newctf.Service;

import com.finpy.newctf.Utils.ResUtils;
import org.springframework.web.multipart.MultipartFile;

public interface UploadFileService {
    ResUtils uploadFile(MultipartFile file);
}
