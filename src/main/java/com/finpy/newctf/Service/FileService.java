package com.finpy.newctf.Service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.finpy.newctf.Utils.ResUtils;
import com.finpy.newctf.pojo.Do.FileInfo;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;

/**
 * &#064;Author 生姜の鱼丸
 * &#064;Time 2023/1/23 19:44
 * &#064;Info
 */
public interface FileService extends IService<FileInfo> {
    ResUtils uploadFile(MultipartFile file);

    ResUtils uploadAvatar(MultipartFile file, Integer id);

    ResUtils downFile(Integer fileId, HttpServletResponse response);
}
