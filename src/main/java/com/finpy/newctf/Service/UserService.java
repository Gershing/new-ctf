package com.finpy.newctf.Service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.finpy.newctf.Utils.ResUtils;
import com.finpy.newctf.pojo.Do.User;
import com.finpy.newctf.pojo.dto.ListData;
import com.finpy.newctf.pojo.dto.Register;
import com.finpy.newctf.pojo.dto.UserLogin;
import com.finpy.newctf.pojo.dto.UserPage;

public interface UserService extends IService<User> {

    ResUtils isAdmin(String userName);

    ResUtils GetUserInfo(String userName);

    ResUtils GetUserList(UserPage userPage);

    ResUtils GetUserOkNum(Integer id);

    ResUtils EndUserByUid(Integer id);

    ResUtils StartUserByUid(Integer id);

    ResUtils AddAdminByUid(ListData ids);

    ResUtils DeleteUserByUid(ListData uid);

    ResUtils Login(UserLogin userLogin);

    ResUtils EditUserInfo(User user);

    ResUtils Register(Register register);

    ResUtils GetUserSort(UserPage page);

    ResUtils GetSortInfo();

    ResUtils GetUserSortInfo(Integer id);

    ResUtils getUserInfo(Integer id);
}
