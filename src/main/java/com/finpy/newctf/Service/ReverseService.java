package com.finpy.newctf.Service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.finpy.newctf.Utils.ResUtils;
import com.finpy.newctf.pojo.Do.Reverse;
import com.finpy.newctf.pojo.dto.AddReverse;
import com.finpy.newctf.pojo.dto.PageDto;
import com.finpy.newctf.pojo.dto.SubmitFlag;

public interface ReverseService extends IService<Reverse> {

    ResUtils getReverseList(PageDto pageDto);

    ResUtils getReverseInfo(Integer id);

    ResUtils addReverse(AddReverse reverse, Integer id);

    ResUtils submitFlag(SubmitFlag flag, Integer userId);

    ResUtils GetSortAndWp(Integer id);

    ResUtils getAdminReverseList(PageDto page);

    ResUtils deleteReverse(Integer id);

    ResUtils getAdminReverseInfo(Integer id);
}

