package com.finpy.newctf.Service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.finpy.newctf.Utils.ResUtils;
import com.finpy.newctf.pojo.Do.Category;

/**
 * &#064;Author 生姜の鱼丸
 * &#064;Time 2023/1/19 12:26
 * &#064;Info
 */
public interface CategoryService extends IService<Category> {
    ResUtils getCategory();
}
