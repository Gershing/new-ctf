package com.finpy.newctf.Service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.finpy.newctf.Utils.ResUtils;
import com.finpy.newctf.pojo.Do.Misc;
import com.finpy.newctf.pojo.dto.AddMisc;
import com.finpy.newctf.pojo.dto.PageDto;
import com.finpy.newctf.pojo.dto.SubmitFlag;

/**
 * &#064;Author 生姜の鱼丸
 * &#064;Time 2023/1/24 12:58
 * &#064;Info
 */
public interface MiscService extends IService<Misc> {
    ResUtils getMiscInfo(Integer id);

    ResUtils getMiscList(PageDto pageDto);

    ResUtils addMisc(AddMisc misc, Integer id);

    ResUtils submitFlag(SubmitFlag flag, Integer userId);

    ResUtils GetSortAndWp(Integer id);

    ResUtils getAdminMiscInfo(Integer id);

    ResUtils getAdminMiscList(PageDto page);

    ResUtils deleteMisc(Integer id);
}
