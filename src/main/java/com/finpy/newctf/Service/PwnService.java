package com.finpy.newctf.Service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.finpy.newctf.Utils.ResUtils;
import com.finpy.newctf.pojo.Do.Pwn;
import com.finpy.newctf.pojo.dto.AddPwn;
import com.finpy.newctf.pojo.dto.PageDto;
import com.finpy.newctf.pojo.dto.SubmitFlag;

public interface PwnService extends IService<Pwn> {

    ResUtils deletePwn(Integer id);

    ResUtils getAdminPwnInfo(Integer id);

    ResUtils getPwnInfo(Integer id);

    ResUtils GetSortAndWp(Integer id);

    ResUtils submitFlag(SubmitFlag flag, Integer userId);

    ResUtils addPwn(AddPwn pwn, Integer id);

    ResUtils getPwnList(PageDto pageDto);

    ResUtils getAdminPwnList(PageDto page);
}

