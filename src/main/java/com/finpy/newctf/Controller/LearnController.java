package com.finpy.newctf.Controller;

import com.finpy.newctf.Annotation.SystemLog;
import com.finpy.newctf.Resolver.UserId;
import com.finpy.newctf.Resolver.UserName;
import com.finpy.newctf.Service.ArticleService;
import com.finpy.newctf.Utils.ResCode;
import com.finpy.newctf.Utils.ResUtils;
import com.finpy.newctf.Utils.StringUtil;
import com.finpy.newctf.pojo.dto.ArticleInfo;
import com.finpy.newctf.pojo.dto.ArticlePage;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class LearnController {
    final ArticleService articleService;

    public LearnController(ArticleService articleService) {
        this.articleService = articleService;
    }


    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/19 17:50
     * @Info 获取文章列表
     */
    @PostMapping("/article")
    @SystemLog(funName = "【前端】 获取文章列表")
    public ResUtils getLearnData(@RequestBody ArticlePage page) {
        if (StringUtil.intIsNull(page.getPage()) || StringUtil.intIsNull(page.getPageSize()) || page.getCategoryId() == null) {
            return ResUtils.fail(ResCode.REQUEST_PARAM_ERROR);
        }
        return articleService.GetArticleList(page);
    }

    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/19 18:00
     * @Info 获取文章详情
     */
    @GetMapping("/article/info")
    @SystemLog(funName = "【前端】 获取文章详情")
    public ResUtils getArticleInfo(Integer id) {
        if (StringUtil.intIsNull(id)) {
            return ResUtils.fail(ResCode.REQUEST_PARAM_ERROR);
        }
        return articleService.GetArticleInfo(id);
    }

    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/19 18:12
     * @Info 获取文章详情页用户信息
     */
    @GetMapping("/article/info/user")
    @SystemLog(funName = "【前端】 获取文章详情页用户信息")
    public ResUtils getUserInfo(Integer id) {
        if (StringUtil.intIsNull(id)) {
            return ResUtils.fail(ResCode.REQUEST_PARAM_ERROR);
        }
        return articleService.GetArticleUserInfo(id);
    }

    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/19 18:12
     * @Info 获取文章列表页用户信息
     */
    @GetMapping("/article/user")
    @SystemLog(funName = "【前端】 获取文章列表页用户信息")
    public ResUtils getUserInfo(@UserName String userName) {
        if (StringUtil.isNull(userName)) {
            return ResUtils.fail(ResCode.REQUEST_PARAM_ERROR);
        }
        return articleService.GetArticleUserInfos(userName);
    }

    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/27 17:19
     * @Info 获取用户文章列表
     */
    @GetMapping("/user/article/list")
    @SystemLog(funName = "【前端】 获取用户文章列表")
    public ResUtils getUserArticleList(@UserId Integer id) {
        if (StringUtil.intIsNull(id)) {
            return ResUtils.fail(ResCode.REQUEST_PARAM_ERROR);
        }
        return articleService.getUserArticleList(id);
    }

    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/19 13:19
     * @Info 发布文章（前端）
     */
    @PostMapping("/write/article")
    @SystemLog(funName = "【前端】 发布文章")
    public ResUtils WriteArticle(@UserId Integer userId, @RequestBody ArticleInfo articleInfo) {
        if (StringUtil.isNull(articleInfo.getTitle()) || StringUtil.isNull(articleInfo.getContent()) ||
                StringUtil.isNull(articleInfo.getCategoryName())) {
            return ResUtils.fail(ResCode.REQUEST_PARAM_ERROR);
        }
        return articleService.SetArticle(articleInfo, userId);
    }

    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/13 17:47
     * @Info 获取文章列表（后台）
     */
    @PostMapping("/admin/article")
    @SystemLog(funName = "【后端】 获取文章列表")
    public ResUtils getArticle(@RequestBody ArticlePage pageDto) {
        if (StringUtil.intIsNull(pageDto.getPage()) || StringUtil.intIsNull(pageDto.getPageSize())) {
            return ResUtils.fail(ResCode.REQUEST_PARAM_ERROR);
        }
        return articleService.AdminGetArticleList(pageDto);
    }

    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/13 18:25
     * @Info 删除文章（后台）
     */
    @GetMapping("/delete/article")
    @SystemLog(funName = "【后端】 删除文章")
    public ResUtils deleteArticle(Integer id) {
        if (StringUtil.intIsNull(id)) {
            return ResUtils.fail(ResCode.REQUEST_PARAM_ERROR);
        }
        return articleService.DeleteArticle(id);
    }
}
