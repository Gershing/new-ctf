package com.finpy.newctf.Controller;

import com.finpy.newctf.Annotation.SystemLog;
import com.finpy.newctf.Service.NoticeService;
import com.finpy.newctf.Service.impl.IndexServiceImpl;
import com.finpy.newctf.Utils.ResUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class IndexController {
    final IndexServiceImpl indexService;
    final NoticeService noticeService;

    public IndexController(IndexServiceImpl indexService, NoticeService noticeService) {
        this.indexService = indexService;
        this.noticeService = noticeService;
    }

//    @GetMapping("/test")
//    public String test() {
//        File file = new File("/templates/key.pem");
//        return file.getName();
//    }

    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/19 14:29
     * @Info 首页获取数据
     */
    @GetMapping("/index")
    @SystemLog(funName = "【前端】 获取首页数据")
    public ResUtils index() {
        // 获取首页数据
        return indexService.Index();
    }

}
