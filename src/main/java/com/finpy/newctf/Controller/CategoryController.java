package com.finpy.newctf.Controller;

import com.finpy.newctf.Service.CategoryService;
import com.finpy.newctf.Utils.ResUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * &#064;Author 生姜の鱼丸
 * &#064;Time 2023/1/27 16:31
 * &#064;Info 分类
 */
@RestController
@RequestMapping("/api")
public class CategoryController {
    final CategoryService categoryService;

    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/27 16:32
     * @Info 获取学习页面文章分类信息
     */
    @GetMapping("/learn/category")
    public ResUtils getCategory() {
        return categoryService.getCategory();
    }
}
