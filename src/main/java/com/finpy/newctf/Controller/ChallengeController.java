package com.finpy.newctf.Controller;

import com.finpy.newctf.Annotation.SystemLog;
import com.finpy.newctf.Resolver.UserId;
import com.finpy.newctf.Service.*;
import com.finpy.newctf.Service.impl.ChallengeServiceImpl;
import com.finpy.newctf.Utils.BeanUtil;
import com.finpy.newctf.Utils.ResCode;
import com.finpy.newctf.Utils.ResUtils;
import com.finpy.newctf.Utils.StringUtil;
import com.finpy.newctf.pojo.Do.Category;
import com.finpy.newctf.pojo.dto.*;
import com.finpy.newctf.pojo.vo.CategoryVo;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class ChallengeController {
    final ChallengeService challengeService;
    final MiscService miscService;
    final WebService webService;
    final PwnService pwnService;
    final ReverseService reverseService;
    final CryptoService cryptoService;
    final CategoryService categoryService;

    public ChallengeController(ChallengeServiceImpl challenge, MiscService miscService, WebService webService, PwnService pwnService, ReverseService reverseService, CryptoService cryptoService, CategoryService categoryService) {
        this.challengeService = challenge;
        this.miscService = miscService;
        this.webService = webService;
        this.pwnService = pwnService;
        this.reverseService = reverseService;
        this.cryptoService = cryptoService;
        this.categoryService = categoryService;
    }

    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/24 15:27
     * @Info 获取misc题目列表
     */
    @PostMapping("/get/misc")
    @SystemLog(funName = "【前端】获取misc题目")
    public ResUtils getMiscList(@RequestBody PageDto pageDto) {
        if (StringUtil.intIsNull(pageDto.getPage()) ||
                StringUtil.intIsNull(pageDto.getPageSize())) {
            return ResUtils.fail(ResCode.REQUEST_PARAM_ERROR);
        }
        return miscService.getMiscList(pageDto);
    }

    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/24 15:27
     * @Info 获取web题目列表
     */
    @PostMapping("/get/web")
    @SystemLog(funName = "【前端】获取web题目")
    public ResUtils getWebList(@RequestBody PageDto pageDto) {
        if (StringUtil.intIsNull(pageDto.getPage()) ||
                StringUtil.intIsNull(pageDto.getPageSize())) {
            return ResUtils.fail(ResCode.REQUEST_PARAM_ERROR);
        }
        return webService.getWebList(pageDto);
    }

    /**
     * @Author 生姜の鱼丸
     * @Date 2023/3/12 14:25
     * @Info 获取Pwn题目
     */
    @PostMapping("/get/pwn")
    @SystemLog(funName = "【前端】获取Pwn题目")
    public ResUtils getPwnList(@RequestBody PageDto pageDto) {
        if (StringUtil.intIsNull(pageDto.getPage()) ||
                StringUtil.intIsNull(pageDto.getPageSize())) {
            return ResUtils.fail(ResCode.REQUEST_PARAM_ERROR);
        }
        return pwnService.getPwnList(pageDto);
    }

    /**
     * @Author 生姜の鱼丸
     * @Date 2023/3/12 14:25
     * @Info 获取Reverse题目
     */
    @PostMapping("/get/reverse")
    @SystemLog(funName = "【前端】获取Reverse题目")
    public ResUtils getReverseList(@RequestBody PageDto pageDto) {
        if (StringUtil.intIsNull(pageDto.getPage()) ||
                StringUtil.intIsNull(pageDto.getPageSize())) {
            return ResUtils.fail(ResCode.REQUEST_PARAM_ERROR);
        }
        return reverseService.getReverseList(pageDto);
    }

    /**
     * @Author 生姜の鱼丸
     * @Date 2023/3/12 14:25
     * @Info 获取Reverse题目
     */
    @PostMapping("/get/crypto")
    @SystemLog(funName = "【前端】获取Crypto题目")
    public ResUtils getCryptoList(@RequestBody PageDto pageDto) {
        if (StringUtil.intIsNull(pageDto.getPage()) ||
                StringUtil.intIsNull(pageDto.getPageSize())) {
            return ResUtils.fail(ResCode.REQUEST_PARAM_ERROR);
        }
        return cryptoService.getCryptoList(pageDto);
    }

    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/24 15:27
     * @Info 获取题目详情
     */
    @PostMapping("/get/challenge/info")
    @SystemLog(funName = "【前端】通过题目id和类型获取")
    public ResUtils getChallengeByCid(@RequestBody ChallengeInfo challengeInfo, @UserId Integer id) {
        if (StringUtil.intIsNull(challengeInfo.getId()) || StringUtil.intIsNull(challengeInfo.getCategoryId())) {
            return ResUtils.fail(ResCode.REQUEST_PARAM_ERROR);
        }
        return challengeService.getChallengeInfo(challengeInfo, id);
    }

    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/24 16:31
     * @Info 添加misc题目
     */
    @PostMapping("/add/misc")
    @SystemLog(funName = "【后端】添加misc题目")
    public ResUtils addMisc(@RequestBody AddMisc misc, @UserId Integer id) {
        if (StringUtil.intIsNull(id) || StringUtil.isNull(misc.getMiscName()) ||
                StringUtil.isNull(misc.getMiscFlag()) || StringUtil.intIsNull(misc.getMiscScores()) ||
                StringUtil.intIsNull(misc.getMiscFile()) || StringUtil.intIsNull(misc.getCategoryId()) || misc.getMiscKnowledge() == null) {
            return ResUtils.fail(ResCode.REQUEST_PARAM_ERROR);
        }
        // 判断知识点是否为空
        if (misc.getMiscKnowledge().size() == 0) {
            misc.getMiscKnowledge().add("无");
        }
        if (StringUtil.isNull(misc.getMiscDescribe())) {
            misc.setMiscDescribe("无");
        }
        return miscService.addMisc(misc, id);
    }

    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/24 16:31
     * @Info 添加Pwn题目
     */
    @PostMapping("/add/pwn")
    @SystemLog(funName = "【后端】添加Pwn题目")
    public ResUtils addPwn(@RequestBody AddPwn pwn, @UserId Integer id) {
        if (StringUtil.intIsNull(id) || StringUtil.isNull(pwn.getPwnName()) ||
                StringUtil.isNull(pwn.getPwnDescribe()) || StringUtil.intIsNull(pwn.getPwnScores()) ||
                StringUtil.intIsNull(pwn.getCategoryId()) || StringUtil.intIsNull(pwn.getPwnFile())) {
            return ResUtils.fail(ResCode.REQUEST_PARAM_ERROR);
        }
        // 判断知识点是否为空
        if (pwn.getPwnKnowledge().size() == 0) {
            pwn.getPwnKnowledge().add("无");
        }
        if (StringUtil.isNull(pwn.getPwnDescribe())) {
            pwn.setPwnDescribe("无");
        }
        return pwnService.addPwn(pwn, id);
    }

    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/24 16:31
     * @Info 添加reverse题目
     */
    @PostMapping("/add/reverse")
    @SystemLog(funName = "【后端】添加reverse题目")
    public ResUtils addReverse(@RequestBody AddReverse reverse, @UserId Integer id) {
        if (StringUtil.intIsNull(id) || StringUtil.isNull(reverse.getResName()) ||
                StringUtil.isNull(reverse.getResDescribe()) || StringUtil.intIsNull(reverse.getResScores()) ||
                StringUtil.intIsNull(reverse.getCategoryId()) || StringUtil.intIsNull(reverse.getResFile())) {
            return ResUtils.fail(ResCode.REQUEST_PARAM_ERROR);
        }
        // 判断知识点是否为空
        if (reverse.getResKnowledge().size() == 0) {
            reverse.getResKnowledge().add("无");
        }
        if (StringUtil.isNull(reverse.getResDescribe())) {
            reverse.setResDescribe("无");
        }
        return reverseService.addReverse(reverse, id);
    }

    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/24 16:31
     * @Info 添加crypto题目
     */
    @PostMapping("/add/crypto")
    @SystemLog(funName = "【后端】添加crypto题目题目")
    public ResUtils addCrypto(@RequestBody AddCrypto crypto, @UserId Integer id) {
        if (StringUtil.intIsNull(id) || StringUtil.isNull(crypto.getCryName()) ||
                StringUtil.isNull(crypto.getCryDescribe()) || StringUtil.intIsNull(crypto.getCryScores()) ||
                StringUtil.intIsNull(crypto.getCategoryId()) || StringUtil.intIsNull(crypto.getCryFile())) {
            return ResUtils.fail(ResCode.REQUEST_PARAM_ERROR);
        }
        // 判断知识点是否为空
        if (crypto.getCryKnowledge().size() == 0) {
            crypto.getCryKnowledge().add("无");
        }
        if (StringUtil.isNull(crypto.getCryDescribe())) {
            crypto.setCryDescribe("无");
        }
        return cryptoService.addCrypto(crypto, id);
    }

    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/24 16:31
     * @Info 添加web题目
     */
    @PostMapping("/add/web")
    @SystemLog(funName = "【后端】添加web题目")
    public ResUtils addWeb(@RequestBody AddWeb web, @UserId Integer id) {
        if (StringUtil.intIsNull(id) || StringUtil.isNull(web.getWebName()) ||
                StringUtil.isNull(web.getWebImage()) || StringUtil.intIsNull(web.getWebScores()) || web.getWebKnowledge() == null) {
            return ResUtils.fail(ResCode.REQUEST_PARAM_ERROR);
        }
        // 判断知识点是否为空
        if (web.getWebKnowledge().size() == 0) {
            web.getWebKnowledge().add("无");
        }
        if (StringUtil.isNull(web.getWebDescribe())) {
            web.setWebDescribe("无");
        }
        return webService.addWeb(web, id);
    }

    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/24 17:02
     * @Info 提交misc题目的flag
     */
    @PostMapping("/submit/misc/flag")
    @SystemLog(funName = "【前端】提交misc flag")
    public ResUtils subMiscFlag(@RequestBody SubmitFlag flag, @UserId Integer userId) {
        if (StringUtil.isNull(flag.getFlag()) || StringUtil.intIsNull(flag.getId())) {
            return ResUtils.fail(ResCode.REQUEST_PARAM_ERROR);
        }
        return miscService.submitFlag(flag, userId);
    }

    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/24 17:02
     * @Info 提交Pwn题目的flag
     */
    @PostMapping("/submit/pwn/flag")
    @SystemLog(funName = "【前端】提交pwn flag")
    public ResUtils subPwnFlag(@RequestBody SubmitFlag flag, @UserId Integer userId) {
        if (StringUtil.isNull(flag.getFlag()) || StringUtil.intIsNull(flag.getId())) {
            return ResUtils.fail(ResCode.REQUEST_PARAM_ERROR);
        }
        return pwnService.submitFlag(flag, userId);
    }

    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/24 17:02
     * @Info 提交Reverse题目的flag
     */
    @PostMapping("/submit/reverse/flag")
    @SystemLog(funName = "【前端】提交reverse flag")
    public ResUtils subReverseFlag(@RequestBody SubmitFlag flag, @UserId Integer userId) {
        if (StringUtil.isNull(flag.getFlag()) || StringUtil.intIsNull(flag.getId())) {
            return ResUtils.fail(ResCode.REQUEST_PARAM_ERROR);
        }
        return reverseService.submitFlag(flag, userId);
    }

    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/24 17:02
     * @Info 提交Crypto题目的flag
     */
    @PostMapping("/submit/crypto/flag")
    @SystemLog(funName = "【前端】提交Crypto flag")
    public ResUtils subCryptoFlag(@RequestBody SubmitFlag flag, @UserId Integer userId) {
        if (StringUtil.isNull(flag.getFlag()) || StringUtil.intIsNull(flag.getId())) {
            return ResUtils.fail(ResCode.REQUEST_PARAM_ERROR);
        }
        return cryptoService.submitFlag(flag, userId);
    }

    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/24 17:02
     * @Info 提交web题目的flag
     */
    @PostMapping("/submit/web/flag")
    @SystemLog(funName = "【前端】提交web flag")
    public ResUtils subWebFlag(@RequestBody SubmitFlag flag, @UserId Integer userId) {
        if (StringUtil.isNull(flag.getFlag()) || StringUtil.intIsNull(flag.getId())) {
            return ResUtils.fail(ResCode.REQUEST_PARAM_ERROR);
        }
        return webService.submitFlag(flag, userId);
    }

    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/25 12:59
     * @Info 获取misc题目的解题排名
     */
    @GetMapping("/get/misc/sort/wp")
    @SystemLog(funName = "【前端】获取misc题目的解题排名")
    public ResUtils getMiscSort(Integer id) {
        if (StringUtil.intIsNull(id)) {
            return ResUtils.fail(ResCode.REQUEST_PARAM_ERROR);
        }
        return miscService.GetSortAndWp(id);
    }

    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/25 13:35
     * @Info 获取web题目的解题排名
     */
    @GetMapping("/get/web/sort/wp")
    @SystemLog(funName = "【前端】获取web题目的解题排名")
    public ResUtils getWebSort(Integer id) {
        if (StringUtil.intIsNull(id)) {
            return ResUtils.fail(ResCode.REQUEST_PARAM_ERROR);
        }
        return webService.GetSortAndWp(id);
    }

    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/25 13:35
     * @Info 获取Pwn题目的解题排名
     */
    @GetMapping("/get/pwn/sort/wp")
    @SystemLog(funName = "【前端】获取pwn题目的解题排名")
    public ResUtils getPwnSort(Integer id) {
        if (StringUtil.intIsNull(id)) {
            return ResUtils.fail(ResCode.REQUEST_PARAM_ERROR);
        }
        return pwnService.GetSortAndWp(id);
    }

    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/25 13:35
     * @Info 获取Reverse题目的解题排名
     */
    @GetMapping("/get/reverse/sort/wp")
    @SystemLog(funName = "【前端】获取reverse题目的解题排名")
    public ResUtils getReverseSort(Integer id) {
        if (StringUtil.intIsNull(id)) {
            return ResUtils.fail(ResCode.REQUEST_PARAM_ERROR);
        }
        return reverseService.GetSortAndWp(id);
    }

    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/25 13:35
     * @Info 获取Crypto题目的解题排名
     */
    @GetMapping("/get/crypto/sort/wp")
    @SystemLog(funName = "【前端】获取crypto题目的解题排名")
    public ResUtils getCryptoSort(Integer id) {
        if (StringUtil.intIsNull(id)) {
            return ResUtils.fail(ResCode.REQUEST_PARAM_ERROR);
        }
        return cryptoService.GetSortAndWp(id);
    }

    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/25 15:34
     * @Info 修改题目信息
     */
    @PostMapping("/update/challenge")
    @SystemLog(funName = "【后台】 修改题目")
    public ResUtils updateChallenge(@RequestBody UpdateChallenge updateChallenge) {
        if (StringUtil.intIsNull(updateChallenge.getCategory())) {
            return ResUtils.fail(ResCode.REQUEST_PARAM_ERROR);
        }
        return challengeService.UpdateChallenge(updateChallenge);
    }

    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/13 18:14
     * @Info 获取分类列表（后台）
     */
    @GetMapping("/admin/class")
    @SystemLog(funName = "【后台】 获取题目分类列表")
    public ResUtils getClassList() {
        List<Category> list = categoryService.list();
        if (list == null) {
            return ResUtils.fail(ResCode.GET_ERROR);
        }
        List<CategoryVo> categoryVos = BeanUtil.copyBeanList(list, CategoryVo.class);
        return ResUtils.success(ResCode.GET_SUCCESS, categoryVos);
    }

    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/30 12:09
     * @Info 获取web题目列表（后台）
     */
    @PostMapping("/admin/get/web/challenge")
    @SystemLog(funName = "【后台】 获取web题目列表")
    public ResUtils getAdminWebList(@RequestBody PageDto page) {
        if (StringUtil.intIsNull(page.getPage()) ||
                StringUtil.intIsNull(page.getPageSize())) {
            return ResUtils.fail(ResCode.REQUEST_PARAM_ERROR);
        }
        return webService.getAdminWebList(page);
    }

    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/30 12:09
     * @Info 获取misc题目列表（后台）
     */
    @PostMapping("/admin/get/misc/challenge")
    @SystemLog(funName = "【后台】 获取misc题目列表")
    public ResUtils getAdminMiscList(@RequestBody PageDto page) {
        if (StringUtil.intIsNull(page.getPage()) ||
                StringUtil.intIsNull(page.getPageSize())) {
            return ResUtils.fail(ResCode.REQUEST_PARAM_ERROR);
        }
        return miscService.getAdminMiscList(page);
    }

    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/30 12:09
     * @Info 获取pwn题目列表（后台）
     */
    @PostMapping("/admin/get/pwn/challenge")
    @SystemLog(funName = "【后台】 获取pwn题目列表")
    public ResUtils getAdminPwnList(@RequestBody PageDto page) {
        if (StringUtil.intIsNull(page.getPage()) ||
                StringUtil.intIsNull(page.getPageSize())) {
            return ResUtils.fail(ResCode.REQUEST_PARAM_ERROR);
        }
        return pwnService.getAdminPwnList(page);
    }

    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/30 12:09
     * @Info 获取crypto题目列表（后台）
     */
    @PostMapping("/admin/get/crypto/challenge")
    @SystemLog(funName = "【后台】 获取Crypto题目列表")
    public ResUtils getAdminCryptoList(@RequestBody PageDto page) {
        if (StringUtil.intIsNull(page.getPage()) ||
                StringUtil.intIsNull(page.getPageSize())) {
            return ResUtils.fail(ResCode.REQUEST_PARAM_ERROR);
        }
        return cryptoService.getAdminCryptoList(page);
    }

    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/30 12:09
     * @Info 获取Reverse题目列表（后台）
     */
    @PostMapping("/admin/get/reverse/challenge")
    @SystemLog(funName = "【后台】 获取Reverse题目列表")
    public ResUtils getAdminReverseList(@RequestBody PageDto page) {
        if (StringUtil.intIsNull(page.getPage()) ||
                StringUtil.intIsNull(page.getPageSize())) {
            return ResUtils.fail(ResCode.REQUEST_PARAM_ERROR);
        }
        return reverseService.getAdminReverseList(page);
    }

    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/30 13:24
     * @Info 获取题目详情（后台）
     */
    @PostMapping("/admin/get/challenge/info")
    @SystemLog(funName = "【后台】 获取题目详情")
    public ResUtils getAdminChallengeInfo(@RequestBody ChallengeInfo challengeInfo) {
        if (StringUtil.intIsNull(challengeInfo.getId()) || StringUtil.intIsNull(challengeInfo.getCategoryId())) {
            return ResUtils.fail(ResCode.REQUEST_PARAM_ERROR);
        }
        return challengeService.getAdminChallengeInfo(challengeInfo);
    }

    /**
     * @Author 生姜の鱼丸
     * @Date 2023/2/3 14:25
     * @Info 删除题目
     */
    @PostMapping("/admin/delete/challenge")
    @SystemLog(funName = "【后台】 删除题目")
    public ResUtils deleteChallenge(@RequestBody DeleteChallenge delChallenge) {
        if (StringUtil.intIsNull(delChallenge.getCategoryId()) || StringUtil.intIsNull(delChallenge.getId())) {
            return ResUtils.fail(ResCode.REQUEST_PARAM_ERROR);
        }
        return challengeService.deleteChallenge(delChallenge);
    }

}
