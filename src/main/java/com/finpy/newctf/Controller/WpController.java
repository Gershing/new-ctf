package com.finpy.newctf.Controller;

import com.finpy.newctf.Annotation.SystemLog;
import com.finpy.newctf.Resolver.UserId;
import com.finpy.newctf.Service.WriteupService;
import com.finpy.newctf.Utils.ResCode;
import com.finpy.newctf.Utils.ResUtils;
import com.finpy.newctf.Utils.StringUtil;
import com.finpy.newctf.pojo.Do.Writeup;
import com.finpy.newctf.pojo.dto.WriteupPage;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/api")
public class WpController {
    final WriteupService writeupService;

    public WpController(WriteupService writeupService) {
        this.writeupService = writeupService;
    }

    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/16 19:47
     * @Info 获取wp列表（分页）
     */
    @PostMapping("/wp/list")
    @SystemLog(funName = "【前端】 获取wp列表")
    public ResUtils GetWpList(@RequestBody WriteupPage writeupPage) {
        if (StringUtil.intIsNull(writeupPage.getPage()) ||
                StringUtil.intIsNull(writeupPage.getPageSize())) {
            return ResUtils.fail(ResCode.REQUEST_PARAM_ERROR);
        }
        // 分类id等于0时表示查询所有
        if (writeupPage.getCategoryId() == null || writeupPage.getCategoryId() < 0) {
            return ResUtils.fail(ResCode.REQUEST_PARAM_ERROR);
        }
        return writeupService.GetWriteupList(writeupPage);
    }

    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/19 12:29
     * @Info 获取wp列表和分类列表（后端）
     */
    @PostMapping("/admin/wp/list")
    @SystemLog(funName = "【后端】 获取wp列表")
    public ResUtils GetWp(@RequestBody WriteupPage writeupPage) {
        if (StringUtil.intIsNull(writeupPage.getPage()) ||
                StringUtil.intIsNull(writeupPage.getPageSize())) {
            return ResUtils.fail(ResCode.REQUEST_PARAM_ERROR);
        }
        // 分类id等于0时表示查询所有
        if (writeupPage.getCategoryId() == null || writeupPage.getCategoryId() < 0) {
            return ResUtils.fail(ResCode.REQUEST_PARAM_ERROR);
        }
        return writeupService.GetWriteupListAndCategory(writeupPage);
    }

    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/19 14:27
     * @Info 通过id获取wp详情
     */
    @GetMapping("/wp/info")
    @SystemLog(funName = "【前端】 获取wp详情")
    public ResUtils GetWpInfo(Integer id) {
        if (StringUtil.intIsNull(id)) {
            return ResUtils.fail(ResCode.DATA_ERROR);
        }
        return writeupService.GetWriteupInfoById(id);
    }

    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/19 14:28
     * @Info 通过id删除wp
     */
    @GetMapping("/delete/wp")
    @SystemLog(funName = "【后端】 删除wp")
    public ResUtils DeleteWp(Integer id) {
        if (StringUtil.intIsNull(id)) {
            return ResUtils.fail(ResCode.GET_ERROR);
        }
        return writeupService.DeleteWriteupById(id);
    }

    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/25 15:34
     * @Info 添加wp
     */
    @PostMapping("/write/wp")
    @SystemLog(funName = "【前端】提交题目wp")
    public ResUtils WriteUp(@RequestBody Writeup writeup, @UserId Integer id) {
        if (StringUtil.isNull(writeup.getContent()) || StringUtil.intIsNull(writeup.getCId()) ||
                StringUtil.intIsNull(writeup.getCategoryId()) || StringUtil.isNull(writeup.getTitle())) {
            return ResUtils.fail(ResCode.REQUEST_PARAM_ERROR);
        }
        return writeupService.AddWriteUp(writeup, id);
    }
}
