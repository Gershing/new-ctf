package com.finpy.newctf.Controller;


import com.finpy.newctf.Utils.GraphicHelper;
import com.finpy.newctf.Utils.RedisBean;
import com.finpy.newctf.Utils.ResCode;
import com.finpy.newctf.Utils.ResUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.FastByteArrayOutputStream;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;


@RestController
@RequestMapping("/api")
public class CaptchaController {

    final RedisBean redisBean;

    @Autowired
    public CaptchaController(RedisBean redisBean) {
        this.redisBean = redisBean;
    }

    @GetMapping("/getCaptchaImage")
    public ResUtils getCaptchaImage() {
        String uuid = UUID.randomUUID().toString();
        FastByteArrayOutputStream fastByteArrayOutputStream = new FastByteArrayOutputStream();
        String captchaValue = GraphicHelper.create(fastByteArrayOutputStream);
        if (redisBean.set(uuid, captchaValue, 2)) {
            Map<String, String> res = new HashMap<>();
            res.put("captchaId", uuid);
            res.put("img", new String(Base64.getEncoder().encode(fastByteArrayOutputStream.toByteArray())));
            return ResUtils.success(ResCode.CAPTCHA_GET_SUCCESS, res);
        }
        return ResUtils.fail(ResCode.CAPTCHA_GET_ERROR);
    }
}
