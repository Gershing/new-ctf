package com.finpy.newctf.Controller;


import com.finpy.newctf.Annotation.SystemLog;
import com.finpy.newctf.Resolver.UserId;
import com.finpy.newctf.Resolver.UserName;
import com.finpy.newctf.Service.UserService;
import com.finpy.newctf.Utils.ResCode;
import com.finpy.newctf.Utils.ResUtils;
import com.finpy.newctf.Utils.StringUtil;
import com.finpy.newctf.pojo.Do.User;
import com.finpy.newctf.pojo.dto.ListData;
import com.finpy.newctf.pojo.dto.Register;
import com.finpy.newctf.pojo.dto.UserLogin;
import com.finpy.newctf.pojo.dto.UserPage;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class UserController {
    final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/19 14:30
     * @Info 用户登录
     */
    @PostMapping("/login")
    @SystemLog(funName = "【前端】 账号登录")
    public ResUtils login(@RequestBody UserLogin userLogin) {
        // 校验参数
        if (StringUtil.isNull(userLogin.getUserName()) || StringUtil.isNull(userLogin.getPassWord()) ||
                StringUtil.isNull(userLogin.getCaptchaId()) || StringUtil.isNull(userLogin.getCaptcha())) {
            return ResUtils.fail(ResCode.REQUEST_PARAM_ERROR);
        }
        return userService.Login(userLogin);
    }

    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/24 13:33
     * @Info 注册
     */
    @PostMapping("/register")
    @SystemLog(funName = "【前端】 注册账号")
    public ResUtils Register(@RequestBody Register register) {
        if (StringUtil.isNull(register.getUserName()) || StringUtil.isNull(register.getNickName()) ||
                StringUtil.isNull(register.getCaptcha()) || StringUtil.isNull(register.getCaptchaId())) {
            return ResUtils.fail(ResCode.REQUEST_PARAM_ERROR);
        }
        return userService.Register(register);
    }

    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/24 14:29
     * @Info 获取全局用户排名信息
     */
    @GetMapping("/sort/info")
    @SystemLog(funName = "【前端】 获取全局用户排名信息")
    public ResUtils GetSortInfo() {
        return userService.GetSortInfo();
    }

    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/27 14:52
     * @Info 获取当前用户排名信息
     */
    @GetMapping("/user/sort/info")
    @SystemLog(funName = "【前端】 获取当前用户排名信息")
    public ResUtils GetUserSortInfo(@UserId Integer id) {
        // 参数校验
        if (StringUtil.intIsNull(id)) {
            return ResUtils.fail(ResCode.REQUEST_PARAM_ERROR);
        }
        return userService.GetUserSortInfo(id);
    }

    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/24 14:03
     * @Info 获取用户排名
     */
    @PostMapping("/user/sort")
    @SystemLog(funName = "【前端】获取用户排名信息")
    public ResUtils getUserSort(@RequestBody UserPage page) {
        if (StringUtil.intIsNull(page.getPage()) || StringUtil.intIsNull(page.getPageSize())) {
            return ResUtils.fail(ResCode.REQUEST_PARAM_ERROR);
        }
        return userService.GetUserSort(page);
    }

    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/27 16:54
     * @Info 获取用户信息
     */
    @GetMapping("/user/info")
    @SystemLog(funName = "【前端】获取用户信息")
    public ResUtils getUserInfo(@UserId Integer id) {
        return userService.getUserInfo(id);
    }

    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/19 15:48
     * @Info 验证用户是否是管理员
     */
    @GetMapping("/admin")
    @SystemLog(funName = "【后端】 管理员验证")
    public ResUtils isAdmin(@UserName String userName) {
        return userService.isAdmin(userName);
    }

    @GetMapping("/user")
    @SystemLog(funName = "【前端】 获取用户个人信息")
    public ResUtils getUserInfo(@UserName String userName) {
        return userService.GetUserInfo(userName);
    }

    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/19 15:45
     * @Info 用户修改个人信息
     */
    @PostMapping("/edit/info")
    @SystemLog(funName = "【前端】 用户修改个人信息")
    public ResUtils setUserInfo(@RequestBody User user, @UserName String userName) {
        if (StringUtil.isNull(user.getNickName()) || StringUtil.isNull(user.getUserSign()) ||
                StringUtil.isNull(user.getAvatar())) {
            return ResUtils.fail(ResCode.OPERATE_ERROR);
        }
        user.setUserName(userName);
        return userService.EditUserInfo(user);
    }

    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/12 17:24
     * @Info 后台获取所有用户数据
     */
    @PostMapping("/admin/user")
    @SystemLog(funName = "【后端】 获取用户列表")
    public ResUtils getUserList(@RequestBody UserPage userPage) {
        if (StringUtil.intIsNull(userPage.getPage()) || StringUtil.intIsNull(userPage.getPageSize())) {
            return ResUtils.fail(ResCode.REQUEST_PARAM_ERROR);
        }
        return userService.GetUserList(userPage);
    }

    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/12 17:52
     * @Info 获取用户每种题目的完成量（后台）
     */
    @GetMapping("/admin/user/ok")
    @SystemLog(funName = "【后端】 获取用户每种题目的完成量")
    public ResUtils getUserOkNum(Integer id) {
        if (StringUtil.intIsNull(id)) {
            return ResUtils.fail(ResCode.REQUEST_PARAM_ERROR);
        }
        return userService.GetUserOkNum(id);
    }

    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/12 18:03
     * @Info 禁用用户（后台）
     */
    @GetMapping("/admin/end/user")
    @SystemLog(funName = "【后端】 禁用用户")
    public ResUtils endUser(Integer id) {
        if (StringUtil.intIsNull(id)) {
            return ResUtils.fail(ResCode.REQUEST_PARAM_ERROR);
        }
        return userService.EndUserByUid(id);
    }

    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/12 18:09
     * @Info 启用用户（后台）
     */
    @GetMapping("/admin/start/user")
    @SystemLog(funName = "【后端】 启用用户")
    public ResUtils startUser(Integer id) {
        if (StringUtil.intIsNull(id)) {
            return ResUtils.fail(ResCode.REQUEST_PARAM_ERROR);
        }
        return userService.StartUserByUid(id);
    }

    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/12 18:27
     * @Info 添加管理员（后台）
     */
    @PostMapping("/admin/add/admin")
    @SystemLog(funName = "【后端】 添加管理员")
    public ResUtils addAdmin(@RequestBody ListData listData) {
        if (StringUtil.listIsNull(listData.getIds())) {
            return ResUtils.fail(ResCode.REQUEST_PARAM_ERROR);
        }
        return userService.AddAdminByUid(listData);
    }

    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/12 18:44
     * @Info 删除用户（后台）
     */
    @PostMapping("/admin/delete/user")
    @SystemLog(funName = "【后端】 删除用户")
    public ResUtils deleteUser(@RequestBody ListData listData) {
        if (StringUtil.listIsNull(listData.getIds())) {
            return ResUtils.fail(ResCode.REQUEST_PARAM_ERROR);
        }
        return userService.DeleteUserByUid(listData);
    }
}
