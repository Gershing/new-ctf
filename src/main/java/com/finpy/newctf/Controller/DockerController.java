package com.finpy.newctf.Controller;

import com.finpy.newctf.Annotation.SystemLog;
import com.finpy.newctf.Resolver.UserId;
import com.finpy.newctf.Service.WebService;
import com.finpy.newctf.Service.impl.ChallengeServiceImpl;
import com.finpy.newctf.Utils.ResCode;
import com.finpy.newctf.Utils.ResUtils;
import com.finpy.newctf.Utils.StringUtil;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class DockerController {
    final ChallengeServiceImpl challenge;
    final WebService webService;

    public DockerController(ChallengeServiceImpl challenge, WebService webService) {
        this.challenge = challenge;
        this.webService = webService;
    }

    @GetMapping("/open/env")
    @SystemLog(funName = "【前端】 开启docker容器")
    public ResUtils openEnv(Integer id, @UserId Integer userId) {
        if (StringUtil.intIsNull(id)) {
            return ResUtils.fail(ResCode.REQUEST_PARAM_ERROR);
        }
        return webService.OpenEnv(id, userId);
    }

    @GetMapping("/end/env")
    @SystemLog(funName = "【前端】 关闭docker容器")
    public ResUtils endEnv(Integer id, @UserId Integer userId) {
        if (StringUtil.intIsNull(id)) {
            return ResUtils.fail(ResCode.REQUEST_PARAM_ERROR);
        }
        return webService.EndEnv(id, userId);
    }
}
