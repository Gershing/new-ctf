package com.finpy.newctf.Controller;

import com.finpy.newctf.Annotation.SystemLog;
import com.finpy.newctf.Service.SettingsService;
import com.finpy.newctf.Utils.ResUtils;
import com.finpy.newctf.pojo.Do.Settings;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/global")
public class SettingsController {
    final SettingsService settingsService;

    public SettingsController(SettingsService settingsService) {
        this.settingsService = settingsService;
    }

    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/16 16:26
     * @Info 获取全局配置
     */
    @GetMapping("/settings")
    @SystemLog(funName = "【前端】 获取全局配置")
    public ResUtils getGlobalSettings() {
        return settingsService.GetSettings();
    }

    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/16 16:26
     * @Info 修改全局配置
     */
    @PostMapping("/update")
    @SystemLog(funName = "【后端】 修改全局配置")
    public ResUtils updateSettings(@RequestBody Settings settings) {
        return settingsService.UpdateSettings(settings);
    }
}
