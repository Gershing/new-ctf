package com.finpy.newctf.Controller;

import com.finpy.newctf.Resolver.UserId;
import com.finpy.newctf.Service.FileService;
import com.finpy.newctf.Service.UploadFileService;
import com.finpy.newctf.Service.impl.UploadFileServiceImpl;
import com.finpy.newctf.Utils.ResCode;
import com.finpy.newctf.Utils.ResUtils;
import com.finpy.newctf.Utils.StringUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;

@Slf4j
@RestController
@RequestMapping("/api")
public class FileController {
    final UploadFileService uploadFileService;
    final FileService fileService;

    public FileController(UploadFileServiceImpl uploadFileService, FileService fileService) {
        this.uploadFileService = uploadFileService;
        this.fileService = fileService;
    }

    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/24 11:36
     * @Info 题目文件上传
     */
    @PostMapping("/upload/file")
    public ResUtils upload(@RequestParam("file") MultipartFile file) {
        if (file == null) {
            return ResUtils.fail(ResCode.REQUEST_PARAM_ERROR);
        }
        log.info("上传文件          ：{}", file.getOriginalFilename());
        return fileService.uploadFile(file);
    }

    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/24 11:37
     * @Info 用户头像上传
     */
    @PostMapping("/upload/avatar")
    public ResUtils uploadAvatar(@RequestParam(name = "file") MultipartFile file, @UserId Integer id) {
        if (file == null || StringUtil.intIsNull(id)) {
            return ResUtils.fail(ResCode.REQUEST_PARAM_ERROR);
        }
        log.info("上传头像          ：{}", file.getOriginalFilename());
        return fileService.uploadAvatar(file, id);
    }

    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/24 11:37
     * @Info 文件下载
     */
    @GetMapping("/download/file")
    public ResUtils downloadFile(@RequestParam("fileId") Integer fileId, HttpServletResponse response) {
        if (StringUtil.intIsNull(fileId)) {
            return ResUtils.fail(ResCode.REQUEST_PARAM_ERROR);
        }
        return fileService.downFile(fileId, response);
    }
}
