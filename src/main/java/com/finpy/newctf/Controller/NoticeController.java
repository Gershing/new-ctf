package com.finpy.newctf.Controller;

import com.finpy.newctf.Annotation.SystemLog;
import com.finpy.newctf.Service.impl.NoticeServiceimpl;
import com.finpy.newctf.Utils.ResCode;
import com.finpy.newctf.Utils.ResUtils;
import com.finpy.newctf.Utils.StringUtil;
import com.finpy.newctf.pojo.Do.Notice;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class NoticeController {
    final NoticeServiceimpl noticeService;

    public NoticeController(NoticeServiceimpl noticeService) {
        this.noticeService = noticeService;
    }

    @GetMapping("/notice/list")
    @SystemLog(funName = "【前端】 获取公告列表")
    public ResUtils GetNoticeList() {
        return noticeService.GetNoticeList();
    }

    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/19 14:29
     * @Info 发布公告（后端）
     */
    @PostMapping("/notice")
    @SystemLog(funName = "【后端】 发布公告")
    public ResUtils notice(@RequestBody Notice notice) {
        // 验证数据
        if (StringUtil.isNull(notice.getTitle()) || StringUtil.isNull(notice.getContent())) {
            return ResUtils.fail(ResCode.DATA_ERROR);
        }
        return noticeService.SetNotice(notice);
    }
}
