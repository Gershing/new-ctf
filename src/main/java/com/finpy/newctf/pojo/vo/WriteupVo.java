package com.finpy.newctf.pojo.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class WriteupVo {
    private Integer id;
    //Wp标题
    private String title;
    //题目类型
    private String categoryName;
    //wp封面
    private String img;
    private LocalDateTime createTime;

}
