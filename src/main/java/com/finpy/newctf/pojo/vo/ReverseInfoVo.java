package com.finpy.newctf.pojo.vo;


import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

/**
 * &#064;Author 生姜の鱼丸
 * &#064;Time 2023/3/12 14:57
 * &#064;Info 用于前端展示Reverse题目信息
 */
@Data
public class ReverseInfoVo {
    private Integer id;

    private String resName;

    private Integer resScores;

    private String resDescribe;

    private List<String> resKnowledge;

    private Integer resFile;

    private String fileName;

    private String resAuthor;

    private LocalDateTime updateTime;

}
