package com.finpy.newctf.pojo.vo;

import lombok.Data;

/**
 * &#064;Author 生姜の鱼丸
 * &#064;Time 2023/3/28 21:31
 * &#064;Info
 */
@Data
public class PwnListVo {
    private Integer id;
    private String pwnName;
    private Integer pwnScores;
}
