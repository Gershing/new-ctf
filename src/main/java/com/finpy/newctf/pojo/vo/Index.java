package com.finpy.newctf.pojo.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * &#064;Author 生姜の鱼丸
 * &#064;Time 2023/1/24 13:01
 * &#064;Info
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Index {
    private IndexChallenge challenge;
    private long article;
    private long user;
    private NoticeVo notice;
    private List<UserSortVo> sort;
}
