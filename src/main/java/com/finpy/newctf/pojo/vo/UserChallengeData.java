package com.finpy.newctf.pojo.vo;

import lombok.Data;

/**
 * &#064;Author 生姜の鱼丸
 * &#064;Time 2023/1/27 14:33
 * &#064;Info
 */
@Data
public class UserChallengeData {
    // 用户名
    private String nickName;
    // 总完成量
    private Object value;
}
