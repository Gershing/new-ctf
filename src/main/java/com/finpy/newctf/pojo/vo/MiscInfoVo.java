package com.finpy.newctf.pojo.vo;


import lombok.Data;

import java.util.List;

/**
 * &#064;Author 生姜の鱼丸
 * &#064;Time 2023/1/24 15:44
 * &#064;Info
 */
@Data
public class MiscInfoVo {
    //题目名称
    private String miscName;
    //题目分数
    private Integer miscScores;
    //出题人
    private Integer miscAuthor;
    //题目描述
    private String miscDescribe;
    //题目文件
    private Integer miscFile;
    private String fileName;
    private List<String> miscKnowledge;
}
