package com.finpy.newctf.pojo.vo;

import lombok.Data;

import java.util.List;

/**
 * &#064;Author 生姜の鱼丸
 * &#064;Time 2023/1/30 13:28
 * &#064;Info
 */
@Data
public class AdminWebInfo {
    //题目名称
    private String webName;
    //题目分数
    private Integer webScores;
    //题目描述
    private String webDescribe;
    //题目镜像
    private String webImage;
    //题目作者
    private String author;
    // 完成数
    private long okNum;
    // 失败数
    private long errorNum;
    // 知识点
    private List<String> webKnowledge;
}
