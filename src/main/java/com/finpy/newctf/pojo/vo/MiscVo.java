package com.finpy.newctf.pojo.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MiscVo {
    private Integer id;
    //题目名称
    private String miscName;
    //题目分数
    private Integer miscScores;

}
