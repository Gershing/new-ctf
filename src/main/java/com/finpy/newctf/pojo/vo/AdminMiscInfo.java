package com.finpy.newctf.pojo.vo;

import lombok.Data;

import java.util.List;

/**
 * &#064;Author 生姜の鱼丸
 * &#064;Time 2023/1/30 13:32
 * &#064;Info
 */
@Data
public class AdminMiscInfo {
    //题目名称
    private String miscName;
    //题目分数
    private Integer miscScores;
    //出题人
    private String author;
    //题目描述
    private String miscDescribe;
    //flag
    private String miscFlag;
    //题目文件
    private String miscFile;
    // 完成数
    private long okNum;
    // 失败数
    private long errorNum;
    // 题目知识点
    private List<String> miscKnowledge;
}
