package com.finpy.newctf.pojo.vo;

import lombok.Data;

import java.util.List;

/**
 * &#064;Author 生姜の鱼丸
 * &#064;Time 2023/3/28 21:34
 * &#064;Info
 */
@Data
public class AdminPwnList {
    private Integer id;

    private String pwnName;

    private Integer pwnScores;

    private String pwnDescribe;

    private List<String> pwnKnowledge;

    private String author;

    private String pwnFlag;

    private long pwnOk;
}
