package com.finpy.newctf.pojo.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * &#064;Author 生姜の鱼丸
 * &#064;Time 2023/1/19 15:56
 * &#064;Info
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserListVo {
    private Integer id;
    private String nickName;
    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/19 16:00
     * @Info 用户签名
     */
    private String userSign;
    private String avatar;
    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/19 16:00
     * @Info 用户角色
     */
    private String userRole;
    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/19 16:00
     * @Info 用户状态
     */
    private Integer userStatus;
    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/19 16:00
     * @Info 用户积分
     */
    private Integer integral;
    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/19 16:00
     * @Info 做题正确率
     */
    private Integer correctNum;
    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/19 15:59
     * @Info 用户等级
     */
    private String grade;
}
