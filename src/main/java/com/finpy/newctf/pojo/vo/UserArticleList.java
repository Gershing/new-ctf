package com.finpy.newctf.pojo.vo;

import lombok.Data;

/**
 * &#064;Author 生姜の鱼丸
 * &#064;Time 2023/1/27 17:26
 * &#064;Info 用户文章列表
 */
@Data
public class UserArticleList {
    private Integer id;
    private String title;
}
