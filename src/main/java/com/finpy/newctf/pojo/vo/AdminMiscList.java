package com.finpy.newctf.pojo.vo;

import lombok.Data;

import java.util.List;

/**
 * &#064;Author 生姜の鱼丸
 * &#064;Time 2023/2/2 13:26
 * &#064;Info
 */
@Data
public class AdminMiscList {
    private Integer id;
    //题目名称
    private String miscName;
    //题目分数
    private Integer miscScores;
    //题目描述
    private String miscDescribe;
    //题目镜像
    private String miscFlag;
    //题目作者
    private String author;
    private long miscOk;
    // 知识点
    private List<String> miscKnowledge;
}
