package com.finpy.newctf.pojo.vo;

import lombok.Data;

/**
 * &#064;Author 生姜の鱼丸
 * &#064;Time 2023/3/18 11:00
 * &#064;Info
 */
@Data
public class CryptoListVo {
    private Integer id;

    private String cryName;

    private Integer cryScores;
}
