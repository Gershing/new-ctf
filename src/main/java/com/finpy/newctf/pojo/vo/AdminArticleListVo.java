package com.finpy.newctf.pojo.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * &#064;Author 生姜の鱼丸
 * &#064;Time 2023/1/23 17:26
 * &#064;Info
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AdminArticleListVo {
    private Integer id;
    //发布者id
    private String author;
    //文章标题
    private String title;
    //分类id
    private String categoryName;
    private LocalDateTime createTime;
}
