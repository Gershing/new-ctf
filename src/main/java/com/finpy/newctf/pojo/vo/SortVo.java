package com.finpy.newctf.pojo.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * &#064;Author 生姜の鱼丸
 * &#064;Time 2023/1/24 13:55
 * &#064;Info
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SortVo {
    private String nickName;
    private LocalDateTime time;
}
