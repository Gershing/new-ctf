package com.finpy.newctf.pojo.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * &#064;Author 生姜の鱼丸
 * &#064;Time 2023/1/24 12:48
 * &#064;Info 首页各个题目数量
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class IndexChallenge {
    private long web;
    private long misc;
    private long pwn;
    private long reverse;
    private long real;
    private long crypto;
    private long other;
    private long all;
}
