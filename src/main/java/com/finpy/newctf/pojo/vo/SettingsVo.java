package com.finpy.newctf.pojo.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * &#064;Author 生姜の鱼丸
 * &#064;Time 2023/1/16 16:21
 * &#064;Info
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SettingsVo {
    private String globalName;
    private Integer indexSortNum;
}
