package com.finpy.newctf.pojo.vo;

import lombok.Data;

import java.util.List;

/**
 * &#064;Author 生姜の鱼丸
 * &#064;Time 2023/3/19 17:50
 * &#064;Info
 */
@Data
public class AdminCryptoList {
    private Integer id;
    //题目名称
    private String cryName;
    //题目分数
    private Integer cryScores;
    //题目描述
    private String cryDescribe;
    //题目镜像
    private String cryFlag;
    //题目作者
    private String author;
    private long cryOk;
    // 知识点
    private List<String> cryKnowledge;
}
