package com.finpy.newctf.pojo.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;


/**
 * &#064;Author 生姜の鱼丸
 * &#064;Time 2023/1/16 20:13
 * &#064;Info
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PageVo {
    private List<?> data;
    private Long total;
}
