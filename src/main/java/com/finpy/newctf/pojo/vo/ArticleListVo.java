package com.finpy.newctf.pojo.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ArticleListVo {
    private Integer id;
    private Integer userId;
    private String title;
    private String categoryName;
    private String img;
    private LocalDateTime updateTime;

}
