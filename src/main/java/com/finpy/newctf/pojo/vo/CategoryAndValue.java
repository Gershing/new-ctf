package com.finpy.newctf.pojo.vo;

import lombok.Data;

/**
 * &#064;Author 生姜の鱼丸
 * &#064;Time 2023/1/27 15:01
 * &#064;Info 存放某个用户在每种题目类型下所作题目的个数
 */
@Data
public class CategoryAndValue {
    private String name;
    private Long value;
}
