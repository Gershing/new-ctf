package com.finpy.newctf.pojo.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * &#064;Author 生姜の鱼丸
 * &#064;Time 2023/1/19 12:36
 * &#064;Info 后台页面wp详情
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AdminWpVo {
    private Integer id;
    //Wp标题
    private String title;
    //Wp作者
    private String author;
    //题目类型
    private String categoryName;
    //修改时间
    private LocalDateTime updateTime;
}
