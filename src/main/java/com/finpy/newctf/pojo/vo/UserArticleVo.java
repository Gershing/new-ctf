package com.finpy.newctf.pojo.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserArticleVo {
    private String nickName;
    private String userSign;
    private String avatar;
    private Long articleNum;
    private Long OkNum;
    private Long wpNum;
}
