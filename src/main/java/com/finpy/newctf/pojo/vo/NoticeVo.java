package com.finpy.newctf.pojo.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * &#064;Author 生姜の鱼丸
 * &#064;Time 2023/1/19 12:13
 * &#064;Info
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class NoticeVo {
    private Integer id;
    /**
     * 标题
     */
    private String title;
    /**
     * 公告内容
     */
    private String content;
    private LocalDateTime updateTime;
}
