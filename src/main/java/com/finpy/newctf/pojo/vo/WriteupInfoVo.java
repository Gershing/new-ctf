package com.finpy.newctf.pojo.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class WriteupInfoVo {
    //Wp标题
    private String title;
    private String challenge;
    //Wp内容
    private String content;
    //Wp作者
    private String nickName;
    //题目类型
    private String categoryName;
    //发布时间
    private LocalDateTime updateTime;
}
