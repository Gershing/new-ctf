package com.finpy.newctf.pojo.vo;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class WebVo {
    private Integer id;
    private String webName;
    private Integer webScores;
}
