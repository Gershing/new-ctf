package com.finpy.newctf.pojo.vo;

import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * &#064;Author 生姜の鱼丸
 * &#064;Time 2023/3/18 11:04
 * &#064;Info
 */
@Data
public class CryptoInfoVo {

    private String cryName;

    private Integer cryScores;

    private String cryDescribe;

    private List<String> cryKnowledge;

    private Integer cryFile;

    private String fileName;

    private String cryAuthor;

    private Date updateTime;
}
