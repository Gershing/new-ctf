package com.finpy.newctf.pojo.vo;

import lombok.Data;

import java.util.List;

/**
 * &#064;Author 生姜の鱼丸
 * &#064;Time 2023/1/30 12:15
 * &#064;Info
 */
@Data
public class AdminWebList {
    private Integer id;
    //题目名称
    private String webName;
    //题目分数
    private Integer webScores;
    //题目描述
    private String webDescribe;
    //题目镜像
    private String webImage;
    //题目作者
    private String author;
    private long webOk;
    private long containerNum;
    // 知识点
    private List<String> webKnowledge;
}
