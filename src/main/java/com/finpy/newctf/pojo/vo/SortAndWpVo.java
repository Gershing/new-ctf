package com.finpy.newctf.pojo.vo;


import lombok.Data;

import java.util.List;

/**
 * &#064;Author 生姜の鱼丸
 * &#064;Time 2023/1/25 13:24
 * &#064;Info
 */
@Data
public class SortAndWpVo {
    private List<WpSortVo> writeup;
    private List<SortVo> ok;
}
