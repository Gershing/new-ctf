package com.finpy.newctf.pojo.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserSortVo {
    // 用户昵称
    private String nickName;
    //用户个性签名
    private String userSign;
    //用户头像
    private String avatar;
    // 用户积分
    private Integer integral;
    // 用户id
    private Integer id;
    // 解题量
    private long okNum;
    // 用户身份
    private String role;
    // 用户等级
    private String grade;
}
