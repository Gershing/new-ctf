package com.finpy.newctf.pojo.vo;

import lombok.Data;

/**
 * &#064;Author 生姜の鱼丸
 * &#064;Time 2023/3/12 14:33
 * &#064;Info 用于页面展示Reverse题目列表
 */
@Data
public class ReverseListVo {
    private Integer id;

    private String resName;

    private Integer resScores;
}
