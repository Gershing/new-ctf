package com.finpy.newctf.pojo.vo;

import lombok.Data;

import java.util.List;

/**
 * &#064;Author 生姜の鱼丸
 * &#064;Time 2023/3/19 17:59
 * &#064;Info
 */
@Data
public class AdminReverseList {
    private Integer id;
    //题目名称
    private String resName;
    //题目分数
    private Integer resScores;
    //题目描述
    private String resDescribe;
    //题目镜像
    private String resFlag;
    //题目作者
    private String author;
    private long resOk;
    // 知识点
    private List<String> resKnowledge;
}
