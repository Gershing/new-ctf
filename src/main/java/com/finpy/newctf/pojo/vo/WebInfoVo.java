package com.finpy.newctf.pojo.vo;

import lombok.Data;

import java.util.List;

/**
 * &#064;Author 生姜の鱼丸
 * &#064;Time 2023/1/24 15:34
 * &#064;Info
 */
@Data
public class WebInfoVo {
    //题目名称
    private String webName;
    //题目分数
    private Integer webScores;
    //题目描述
    private String webDescribe;
    //题目作者
    private String webAuthor;
    private List<String> webKnowledge;
    // 题目环境连接
    private String http;
    // 题目结束时间
    private long overTime;
    // 题目开始时间
    private long startTime;
}
