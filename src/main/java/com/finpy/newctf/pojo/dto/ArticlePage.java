package com.finpy.newctf.pojo.dto;

import lombok.Data;

/**
 * &#064;Author 生姜の鱼丸
 * &#064;Time 2023/1/27 16:43
 * &#064;Info
 */
@Data
public class ArticlePage {
    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/19 12:20
     * @Info 分类id
     */
    private Integer categoryId;
    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/19 12:20
     * @Info 第几页
     */
    private Integer page;
    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/19 12:20
     * @Info 一页几条数据
     */
    private Integer pageSize;
}
