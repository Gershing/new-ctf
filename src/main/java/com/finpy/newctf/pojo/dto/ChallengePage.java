package com.finpy.newctf.pojo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * &#064;Author 生姜の鱼丸
 * &#064;Time 2023/1/24 14:43
 * &#064;Info
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ChallengePage {
    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/19 12:20
     * @Info 分类id
     */
    private Integer categoryId;
    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/19 12:20
     * @Info 第几页
     */
    private Integer page;
    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/19 12:20
     * @Info 一页几条数据
     */
    private Integer pageSize;
}
