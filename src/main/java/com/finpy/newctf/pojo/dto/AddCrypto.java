package com.finpy.newctf.pojo.dto;

import lombok.Data;

import java.util.List;

/**
 * &#064;Author 生姜の鱼丸
 * &#064;Time 2023/3/14 20:04
 * &#064;Info 添加crypto题目
 */
@Data
public class AddCrypto {

    private String cryName;

    private Integer cryScores;

    private String cryDescribe;

    private List<String> cryKnowledge;

    private Integer cryFile;
    private Integer categoryId;

    private String cryFlag;
}
