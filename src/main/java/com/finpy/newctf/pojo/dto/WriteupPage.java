package com.finpy.newctf.pojo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * &#064;Author 生姜の鱼丸
 * &#064;Time 2023/1/16 19:48
 * &#064;Info wp分页
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class WriteupPage {
    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/19 12:20
     * @Info 分类id
     */
    private Integer categoryId;
    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/19 12:20
     * @Info 第几页
     */
    private Integer page;
    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/19 12:20
     * @Info 一页几条数据
     */
    private Integer pageSize;
}
