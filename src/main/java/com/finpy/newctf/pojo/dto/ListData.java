package com.finpy.newctf.pojo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * &#064;Author 生姜の鱼丸
 * &#064;Time 2023/1/19 17:08
 * &#064;Info 接受数组类型的数据
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ListData {
    private List<Integer> ids;
}
