package com.finpy.newctf.pojo.dto;

import lombok.Data;

import java.util.List;

/**
 * &#064;Author 生姜の鱼丸
 * &#064;Time 2023/3/12 15:31
 * &#064;Info 添加Reverse题目
 */
@Data
public class AddReverse {
    private String resName;
    private String resFlag;
    private String resDescribe;
    private List<String> resKnowledge;
    private Integer resFile;
    private Integer categoryId;
    private Integer resScores;
}
