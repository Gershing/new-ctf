package com.finpy.newctf.pojo.dto;

import lombok.Data;

import java.util.List;

/**
 * &#064;Author 生姜の鱼丸
 * &#064;Time 2023/1/24 16:32
 * &#064;Info
 */
@Data
public class AddWeb {
    //题目名称
    private String webName;
    //题目分数
    private Integer webScores;
    //题目描述
    private String webDescribe;
    //题目镜像
    private String webImage;
    // 知识点
    private List<String> webKnowledge;
    // 分类id
    private Integer categoryId;
}
