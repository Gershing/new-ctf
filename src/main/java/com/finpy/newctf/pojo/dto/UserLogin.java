package com.finpy.newctf.pojo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * &#064;Author 生姜の鱼丸
 * &#064;Time 2023/1/19 14:31
 * &#064;Info 用户登录
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserLogin {
    private String userName;
    private String passWord;
    private String captcha;
    private String captchaId;
}
