package com.finpy.newctf.pojo.dto;

import lombok.Data;

/**
 * &#064;Author 生姜の鱼丸
 * &#064;Time 2023/1/24 16:47
 * &#064;Info
 */
@Data
public class PageDto {
    private Integer page;
    private Integer pageSize;
}
