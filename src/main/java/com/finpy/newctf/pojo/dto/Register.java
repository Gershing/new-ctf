package com.finpy.newctf.pojo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * &#064;Author 生姜の鱼丸
 * &#064;Time 2023/1/24 13:34
 * &#064;Info
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Register {
    private String nickName;
    private String userName;
    private String passWord;
    private String captcha;
    private String captchaId;
}
