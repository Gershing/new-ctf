package com.finpy.newctf.pojo.dto;

import lombok.Data;

/**
 * &#064;Author 生姜の鱼丸
 * &#064;Time 2023/1/24 15:30
 * &#064;Info
 */
@Data
public class ChallengeInfo {
    // 题目id
    private Integer id;
    // 分类id
    private Integer categoryId;
}
