package com.finpy.newctf.pojo.dto;

import lombok.Data;

import java.util.List;

/**
 * &#064;Author 生姜の鱼丸
 * &#064;Time 2023/1/24 16:11
 * &#064;Info
 */
@Data
public class AddMisc {
    // 题目分类id
    private Integer categoryId;
    // 题目名称
    private String miscName;
    // 题目分数
    private Integer miscScores;
    // 题目描述
    private String miscDescribe;
    // 题目知识点
    private List<String> miscKnowledge;
    // flag
    private String miscFlag;
    // 文件id
    private Integer miscFile;
}
