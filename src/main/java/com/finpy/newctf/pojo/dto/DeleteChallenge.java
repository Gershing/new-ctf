package com.finpy.newctf.pojo.dto;

import lombok.Data;

/**
 * &#064;Author 生姜の鱼丸
 * &#064;Time 2023/2/3 14:27
 * &#064;Info
 */
@Data
public class DeleteChallenge {
    // 题目id
    private Integer id;
    // 分类id
    private Integer categoryId;
}
