package com.finpy.newctf.pojo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * &#064;Author 生姜の鱼丸
 * &#064;Time 2023/1/25 13:47
 * &#064;Info
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UpdateChallenge {
    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/25 13:50
     * @Info 题目id
     */
    private Integer id;
    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/25 13:50
     * @Info 题目名称
     */
    private String name;
    /**
     * @Author 生姜の鱼丸
     * @Date 2023/2/2 13:06
     * @Info 作者
     */
    private String author;
    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/25 13:50
     * @Info 题目分数
     */
    private Integer scores;
    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/25 13:50
     * @Info 题目flag
     */
    private String flag;
    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/25 13:50
     * @Info 题目描述
     */
    private String describe;
    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/25 13:51
     * @Info 题目文件id
     */
    private Integer fileId;
    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/25 13:51
     * @Info 题目分类
     */
    private Integer category;
    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/25 13:51
     * @Info 题目知识点
     */
    private List<String> knowledge;
    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/25 13:51
     * @Info web题目镜像
     */
    private String image;
}
