package com.finpy.newctf.pojo.dto;

import lombok.Data;

/**
 * &#064;Author 生姜の鱼丸
 * &#064;Time 2023/1/24 17:03
 * &#064;Info
 */
@Data
public class SubmitFlag {
    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/24 17:04
     * @Info 题目id
     */
    private Integer id;
    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/24 17:04
     * @Info flag
     */
    private String flag;
}
