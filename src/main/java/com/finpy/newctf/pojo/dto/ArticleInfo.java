package com.finpy.newctf.pojo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * &#064;Author 生姜の鱼丸
 * &#064;Time 2023/1/19 18:29
 * &#064;Info
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ArticleInfo {
    //文章标题
    private String title;
    //文章内容
    private String content;
    //分类id
    private String categoryName;
}
