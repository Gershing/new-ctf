package com.finpy.newctf.pojo.dto;

import lombok.Data;

import java.util.List;

/**
 * &#064;Author 生姜の鱼丸
 * &#064;Time 2023/3/28 21:24
 * &#064;Info
 */
@Data
public class AddPwn {
    private Integer categoryId;

    private String pwnName;

    private Integer pwnScores;

    private String pwnDescribe;

    private List<String> pwnKnowledge;

    private String pwnFlag;

    private Integer pwnFile;
}
