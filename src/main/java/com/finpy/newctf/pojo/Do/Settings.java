package com.finpy.newctf.pojo.Do;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * &#064;Author 生姜の鱼丸
 * &#064;Time 2023/1/16 16:03
 * &#064;Info 全局配置 - 实体类
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("zb_settings")
public class Settings {
    @TableId
    private Integer id;
    private String globalName;
    private Integer indexSortNum;
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

}
