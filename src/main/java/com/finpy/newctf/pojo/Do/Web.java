package com.finpy.newctf.pojo.Do;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * web题目表(Web)表实体类
 *
 * @author 生姜の鱼丸
 * @since 2023-01-19 14:08:30
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("zb_web")
public class Web {
    @TableId
    private Integer id;
    //题目名称        
    private String webName;
    //题目分数        
    private Integer webScores;
    //题目描述        
    private String webDescribe;
    //题目镜像        
    private String webImage;
    //容器占用cpu        
    private Object webCpu;

    private Integer webFlow;
    //题目作者        
    private Integer webAuthor;
    // 知识点
    private String webKnowledge;
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

    private Integer isDelete;

}

