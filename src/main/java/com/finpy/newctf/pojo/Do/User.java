package com.finpy.newctf.pojo.Do;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * 用户表(User)表实体类
 *
 * @author 生姜の鱼丸
 * @since 2023-01-19 12:53:24
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("zb_user")
public class User {
    @TableId
    private Integer id;

    private String userName;
    //用户昵称
    private String nickName;
    //用户个性签名
    private String userSign;
    //用户头像
    private String avatar;

    private String userPassword;
    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/19 12:54
     * @Info 0->管理员
     * 1->小组成员
     * 2->管理员&小组成员
     * 3->普通用户
     */
    private Integer userRole;
    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/19 12:54
     * @Info 0->可用
     * 1->禁用
     */
    private Integer userStatus;
    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/19 12:55
     * @Info 用户积分
     */
    private Integer integral;
    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/19 12:55
     * @Info 用户密码加密盐
     */
    private String salt;

    private Integer isDelete;
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

}

