package com.finpy.newctf.pojo.Do;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("zb_crypto")
public class Crypto {
    @TableId
    private Integer id;

    private String cryName;

    private Integer cryScores;

    private String cryDescribe;

    private String cryKnowledge;

    private Integer cryFile;

    private Integer cryAuthor;

    private String cryFlag;
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

    private Integer isDelete;

}

