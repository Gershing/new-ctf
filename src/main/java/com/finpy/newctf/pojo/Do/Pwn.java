package com.finpy.newctf.pojo.Do;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("zb_pwn")
public class Pwn {
    @TableId
    private Integer id;

    private String pwnName;

    private Integer pwnScores;

    private String pwnDescribe;

    private String pwnKnowledge;

    private Integer pwnAuthor;

    private String pwnFlag;

    private Integer pwnFile;
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

    private Integer isDelete;

}

