package com.finpy.newctf.pojo.Do;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 学习文章分类(Class)表实体类
 *
 * @author 生姜の鱼丸
 * @since 2023-01-19 12:23:20
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("zb_category")
public class Category {
    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/19 12:25
     * @Info 主键
     */
    private Integer id;
    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/19 12:25
     * @Info 分类名字
     */
    private String name;

}

