package com.finpy.newctf.pojo.Do;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * &#064;Author 生姜の鱼丸
 * &#064;Time 2023/1/23 19:40
 * &#064;Info 文件实体类
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("zb_file")
public class FileInfo {
    private Integer id;
    private String fileName;
    private String filePath;
    private Long size;
}
