package com.finpy.newctf.pojo.Do;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("zb_misc")
public class Misc {
    @TableId
    private Integer id;
    //题目名称        
    private String miscName;
    //题目分数        
    private Integer miscScores;
    //出题人        
    private Integer miscAuthor;
    //题目描述        
    private String miscDescribe;
    //flag        
    private String miscFlag;
    //题目文件
    private Integer miscFile;
    // 题目知识点
    private String miscKnowledge;

    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

    private Integer isDelete;

}

