package com.finpy.newctf.pojo.Do;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("web_container")
public class WebContainer {
    //主键
    @TableId
    private Integer id;
    //容器名称
    private String containerName;
    //题目id
    private Integer webId;
    //用户名
    private Integer userId;
    //容器过期时间
    private LocalDateTime containerDate;
    //容器端口
    private Integer webPort;
    //容器地址
    private String http;
    //flag
    private String webFlag;
    //容器id
    private String containerId;
    //0->可用；1->禁用
    private Integer isDelete;

    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

}

