package com.finpy.newctf.pojo.Do;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * 题目Wp(Writeup)表实体类
 *
 * @author 生姜の鱼丸
 * @since 2023-01-16 18:44:26
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("zb_writeup")
public class Writeup {
    @TableId
    private Integer id;
    //Wp标题
    private String title;
    //Wp所属的题目id
    private Integer cId;
    //Wp内容
    private String content;
    //Wp作者
    private Integer userId;
    //题目类型
    private Integer categoryId;
    //wp封面
    private String img;
    //修改时间
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    private Integer isDelete;

}

