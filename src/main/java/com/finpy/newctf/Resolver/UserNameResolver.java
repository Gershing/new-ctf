package com.finpy.newctf.Resolver;

import com.finpy.newctf.MyException.MyException;
import com.finpy.newctf.Utils.JwtUtil;
import com.finpy.newctf.Utils.ResCode;
import com.finpy.newctf.Utils.StringUtil;
import org.springframework.core.MethodParameter;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

/**
 * &#064;Author 生姜の鱼丸
 * &#064;Time 2023/1/15 13:07
 * &#064;Info
 */
@Component
public class UserNameResolver implements HandlerMethodArgumentResolver {
    // 用于表明哪些参数支持该解析器
    @Override
    public boolean supportsParameter(MethodParameter parameter) {
        // 判断当前参数是否加上UserId注解
        return parameter.hasParameterAnnotation(UserName.class);
    }

    // 进行参数解析的方法
    @Override
    public Object resolveArgument(MethodParameter parameter, ModelAndViewContainer mavContainer, NativeWebRequest webRequest, WebDataBinderFactory binderFactory) {
        // 从请求头中获取token，并获取token中的uid
        String token = webRequest.getHeader("Authorization");
        if (StringUtil.isNotNull(token)) {
            String userName = JwtUtil.getUserName(token);
            if (userName == null) {
                throw new MyException(ResCode.SIGN_ERROR);
            }
            return userName;
        }
        throw new MyException(ResCode.IS_NOT_USER);
    }
}
