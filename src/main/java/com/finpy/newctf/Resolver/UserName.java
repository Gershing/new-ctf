package com.finpy.newctf.Resolver;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * &#064;Author 生姜の鱼丸
 * &#064;Time 2023/1/15 13:06
 * &#064;Info 获取userName的注解
 */
@Target(ElementType.PARAMETER)
@Retention(RetentionPolicy.RUNTIME)
public @interface UserName {
}
