package com.finpy.newctf.Config;

import com.alibaba.fastjson.serializer.SerializerFeature;
import com.alibaba.fastjson.support.config.FastJsonConfig;
import com.alibaba.fastjson.support.spring.FastJsonHttpMessageConverter;
import com.finpy.newctf.Handler.MyInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.ArrayList;
import java.util.List;

/**
 * &#064;Author 生姜の鱼丸
 * &#064;Time 2023/1/15 12:27
 * &#064;Info
 */
@Configuration
public class MyWebConfig implements WebMvcConfigurer {
    final MyInterceptor myInterceptor;

    public MyWebConfig(MyInterceptor myInterceptor) {
        this.myInterceptor = myInterceptor;
    }

    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/11 20:46
     * @Info 拦截器
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(myInterceptor)
                .addPathPatterns("/api/**") // 被拦截的路径
                .excludePathPatterns("/**/login")
                .excludePathPatterns("/**/register")
                .excludePathPatterns("/**/wp/**")
                .excludePathPatterns("/**/notice/list")
                .excludePathPatterns("/**/download/file")
                .excludePathPatterns("/**/getCaptchaImage")
                .excludePathPatterns("/**/test")
                .excludePathPatterns("/**/**/env")
                .excludePathPatterns("/**/index");
    }

    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/11 20:46
     * @Info 跨域
     */
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedOrigins("*")
                .allowedHeaders("*")
                .allowedMethods("*")
                .allowCredentials(false)
                .exposedHeaders("*");
    }

    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/11 20:46
     * @Info 配置fastJson
     */
    @Override
    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
        converters.removeIf(converter -> converter instanceof MappingJackson2HttpMessageConverter);
        FastJsonHttpMessageConverter fastJsonHttpMessageConverter = new FastJsonHttpMessageConverter();
        FastJsonConfig fastJsonConfig = new FastJsonConfig();
        fastJsonConfig.setSerializerFeatures(SerializerFeature.PrettyFormat, SerializerFeature.QuoteFieldNames,
                SerializerFeature.WriteNullStringAsEmpty, SerializerFeature.WriteDateUseDateFormat,
                SerializerFeature.DisableCircularReferenceDetect);
        fastJsonConfig.setDateFormat("yyyy-MM-dd HH:mm:ss");
        List<MediaType> fastMediaTypes = new ArrayList<>();
        fastMediaTypes.add(MediaType.APPLICATION_JSON);
        fastJsonHttpMessageConverter.setSupportedMediaTypes(fastMediaTypes);
        fastJsonHttpMessageConverter.setFastJsonConfig(fastJsonConfig);
        converters.add(fastJsonHttpMessageConverter);
        WebMvcConfigurer.super.configureMessageConverters(converters);
    }

    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/11 20:47
     * @Info 配置静态资源访问
     */
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
//        registry.addResourceHandler("/static/**").addResourceLocations("classpath:/static/");
        registry.addResourceHandler("/upload/**").addResourceLocations("classpath:/static/file/");
        WebMvcConfigurer.super.addResourceHandlers(registry);
    }
}
