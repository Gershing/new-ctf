package com.finpy.newctf.Config;


import com.finpy.newctf.pojo.DockerContainer;
import com.github.dockerjava.api.DockerClient;
import com.github.dockerjava.api.command.CreateContainerResponse;
import com.github.dockerjava.api.command.LoadImageCmd;
import com.github.dockerjava.api.command.PullImageCmd;
import com.github.dockerjava.api.model.ExposedPort;
import com.github.dockerjava.api.model.HostConfig;
import com.github.dockerjava.api.model.PortBinding;
import com.github.dockerjava.core.DefaultDockerClientConfig;
import com.github.dockerjava.core.DockerClientBuilder;
import com.github.dockerjava.core.DockerClientConfig;

import java.io.FileInputStream;
import java.io.FileNotFoundException;


public class DockerConfig {

    private static final DockerClient dockerClient = createClient();

    // 连接docker
    public static DockerClient connectDocker() {
        DockerClientConfig config = DefaultDockerClientConfig.createDefaultConfigBuilder()
                .withDockerHost(Constant.DOCKER_URL)
                .build();
        return DockerClientBuilder.getInstance(config).build();
    }

    private static DockerClient createClient() {
        DockerClientConfig config = DefaultDockerClientConfig.createDefaultConfigBuilder()
                .withDockerHost(Constant.DOCKER_URL)
                .withDockerTlsVerify(true)
                // 证书的本地位置
                .withDockerCertPath("/www/wwwroot/java/certs")
                // 私钥的本地位置
                .withDockerConfig("/www/wwwroot/java/certs")
                .withApiVersion("1.42")
                .withRegistryUrl("https://index.docker.io/v1/")
                .withRegistryUsername("754465599")
                .withRegistryPassword("754465599")
                .withRegistryEmail("754465599@qq.com")
                .build();
        return DockerClientBuilder.getInstance(config).build();
    }

    // 创建容器
    public static CreateContainerResponse createContainers(DockerContainer dockerContainer) {
        PortBinding portBinding = PortBinding.parse(dockerContainer.getDstPort() + ":" + dockerContainer.getSrcPort());
        HostConfig hostConfig = HostConfig.newHostConfig()
                .withMemory(dockerContainer.getMemLimit() * 1024 * 1024L)
                .withCpuCount((int) (dockerContainer.getCpuLimit() * 100) * 10000000L)
                .withPortBindings(portBinding);
        System.out.println("连接成功");
        return dockerClient.createContainerCmd(dockerContainer.getImageName())
                .withName(dockerContainer.getName())
                .withHostConfig(hostConfig)
                .withEnv((dockerContainer.getFlag() != null) ? String.format("%s=%s", "FLAG", dockerContainer.getFlag()) : "FLAG=null")
                .withExposedPorts(ExposedPort.parse(dockerContainer.getSrcPort() + "/tcp"))
                .exec();
    }

    public static void startContainer(String id) {
        dockerClient.startContainerCmd(id).exec();
    }

    /**
     * repository 镜像名称:tag名称
     **/
    public PullImageCmd pullImage(DockerClient client, String repository) {
        return client.pullImageCmd(repository);
    }

    // 加载镜像
    public LoadImageCmd loadImage(DockerClient client, String filePath) {
        LoadImageCmd loadImageCmd = null;
        try {
            loadImageCmd = client.loadImageCmd(new FileInputStream(filePath));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return loadImageCmd;
    }

    /**
     * 停止容器
     *
     * @param containerId
     */
    public void stopContainer(String containerId) {
        dockerClient.stopContainerCmd(containerId).exec();
    }

    /**
     * 删除容器
     */
    public void removeContainer(String containerId) {
        dockerClient.removeContainerCmd(containerId).withForce(true).exec();
    }

}
