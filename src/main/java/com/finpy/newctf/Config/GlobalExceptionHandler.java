package com.finpy.newctf.Config;

import com.alibaba.fastjson.JSONObject;
import com.finpy.newctf.MyException.MyException;
import com.finpy.newctf.Utils.ResCode;
import com.finpy.newctf.Utils.ResUtils;
import org.springframework.dao.TransientDataAccessResourceException;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @Author 生姜の鱼丸
 * @Date 2023/1/11 20:45
 * @Info 全局异常处理
 */

@ControllerAdvice
public class GlobalExceptionHandler {

    /**
     * &#064;auther 生姜の鱼丸
     * &#064;Info 没有请求参数
     */
    @ExceptionHandler(HttpMessageNotReadableException.class)
    public void HttpMessageNotException(HttpServletResponse response) throws IOException {
        publicException(response, ResCode.DATA_ERROR);
    }

    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/15 12:24
     * @Info 空指针异常
     */
    @ExceptionHandler(NullPointerException.class)
    public void handleNullPointerException(HttpServletResponse response) throws IOException {
        publicException(response, ResCode.PROGRAM_INSIDE_EXCEPTION);
    }

    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/15 12:25
     * @Info 自定义异常类
     */
    @ExceptionHandler(MyException.class)
    public void MyException(MyException myException, HttpServletResponse response) throws IOException {
        response.setContentType("application/json;charset=UTF-8");
        response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        Map<String, Object> res = new HashMap<>();
        res.put("code", myException.getCode());
        res.put("msg", myException.getMsg());
        response.getWriter().write(JSONObject.toJSONString(res));
    }

    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/15 12:40
     * @Info 请求方式错误
     */
    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    public void RequestMethod(HttpServletResponse response) throws IOException {
        publicException(response, ResCode.REQUEST_METHOD_ERROR);
    }

    @ExceptionHandler(TransientDataAccessResourceException.class)
    public void DateException(HttpServletResponse response) throws IOException {
        publicException(response, ResCode.PROGRAM_INSIDE_EXCEPTION);
    }

    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/15 12:48
     * @Info 系统异常封装
     */
    public void publicException(HttpServletResponse response, ResCode resCode) throws IOException {
        response.addHeader("Access-Control-Allow-Origin", "*");
        response.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        response.setHeader("Access-Control-Allow-Methods", "*");
        response.setHeader("Access-Control-Allow-Credentials", "true");
        response.setContentType("application/json;charset=UTF-8");
        response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        response.getWriter().write(JSONObject.toJSONString(ResUtils.fail(resCode)));
    }
}