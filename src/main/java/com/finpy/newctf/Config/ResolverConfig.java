package com.finpy.newctf.Config;


import com.finpy.newctf.Resolver.UidResolver;
import com.finpy.newctf.Resolver.UserNameResolver;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.List;

/**
 * &#064;Author 生姜の鱼丸
 * &#064;Time 2023/1/12 11:42
 * &#064;Info
 */
@Configuration
public class ResolverConfig implements WebMvcConfigurer {
    final UidResolver uidResolver;
    final UserNameResolver userNameResolver;

    public ResolverConfig(UidResolver uidResolver, UserNameResolver userNameResolver) {
        this.uidResolver = uidResolver;
        this.userNameResolver = userNameResolver;
    }

    @Override
    public void addArgumentResolvers(List<HandlerMethodArgumentResolver> resolvers) {
        resolvers.add(uidResolver);
        resolvers.add(userNameResolver);
    }
}
