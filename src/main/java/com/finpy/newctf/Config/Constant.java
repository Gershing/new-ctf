package com.finpy.newctf.Config;

public class Constant {
    // Docker链接
    public static final String DOCKER_URL = "tcp://47.113.178.227:2375";

    // Redis Key前缀（验证码模块）
    public static final String CAPTCHA_KEY = "CAPTCHA_KEY";

    // Docker容器链接
    public static final String DOCKER_IP = "http://47.113.178.227";

    // Token的过期时间 (三十分钟)
    public static final long TOKEN_OVER_TIME = 30 * 60 * 1000;

    // Token私钥
    public static final String TOKEN_SECRET = "finpywwgkjdklfjKJHKLJGBkjvdkfjvbliwjbgKJNDJFBjndfkjbnldKJLKJBNDKFJBN";

    // 用户密码加密盐的长度
    public static final int SALT_LENGTH = 10;

    // 图片链接前缀 - 七牛云 （随机图片获取模块）
    public static final String IMG_URL = "http://file.finpyx.cn/public/imgs/";

    // 图片上传路径 （文件上传模块）
    public static final String FILE_IMG = "CTF/images/";

    // 文件上传路径 （文件上传模块）
    public static final String FILE_FILE = "CTF/file/";

    /**
     * @Author 生姜の鱼丸
     * @Date 2023/1/19 18:44
     * @Info 随机图片
     */
    public static final String IMG = "http://file.finpyx.cn/public/imgs/";
    public static final String FLAG = "flag";
    public static final String CHALLENGE_SORT = "10";
    public static final String WRITEUP_SORT = "10";
    public static final int DOCKER_OVER_TIME = 30;
    public static final String ARTICLE_LIST_NUM = "5";
}
