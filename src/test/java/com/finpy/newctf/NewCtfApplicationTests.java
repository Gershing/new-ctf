package com.finpy.newctf;

import com.finpy.newctf.Utils.JwtUtil;
import com.finpy.newctf.pojo.Do.User;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDateTime;

@SpringBootTest
class NewCtfApplicationTests {

    @Test
    public void test() {
        System.out.println(LocalDateTime.now().getMinute());
    }

    @Test
    public void token() {
        User user = new User();
        user.setId(6);
        user.setUserName("admin");
        String sign = JwtUtil.sign(user);
        System.out.println(sign);
    }
}
