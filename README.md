# NewCTF

#### 介绍

springboot+vue开发的前后端分离项目，ctf刷题平台，支持动态flag，目前仅支持Web、Misc两种题目
其他类型题目将会陆续加入。

#### 软件架构

Springboot、Vue、Mybatis、Redis、Docker

#### 使用说明

项目配置文件在com.finpy.newctf.Config.Constant类中

目前项目代码已完成

#### 实机演示地址： http://ctf.finpyx.cn

#### 前端项目开源地址：https://gitee.com/Gershing/new_ctf.git